﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Flythrough::Start()
extern void Flythrough_Start_m82752FD2B23306BA215CA4C3A6EFC3041B9293B1 (void);
// 0x00000002 System.Void Flythrough::Update()
extern void Flythrough_Update_m5A2404D7510FFF8CDF5FA8250C51F2D862CBC1FA (void);
// 0x00000003 System.Void Flythrough::.ctor()
extern void Flythrough__ctor_mE1C83D767FDD4C26DCE6363121B167D96B6BC3B6 (void);
// 0x00000004 System.Void SmoothMouseLook::Update()
extern void SmoothMouseLook_Update_m942F000312ABCE8C1AE0E2D06E59013D22640FD5 (void);
// 0x00000005 System.Void SmoothMouseLook::Start()
extern void SmoothMouseLook_Start_m065E245F8A4C432D4AE75AAC209EF5933071A7E2 (void);
// 0x00000006 System.Single SmoothMouseLook::ClampAngle(System.Single,System.Single,System.Single)
extern void SmoothMouseLook_ClampAngle_mA548EE0544AFE855A0F6C1F7E069124E5EB60DC2 (void);
// 0x00000007 System.Void SmoothMouseLook::.ctor()
extern void SmoothMouseLook__ctor_m388B62BE3F96D1DDBD3570E4A89801707BF81864 (void);
// 0x00000008 System.Void WaterReflection::OnWillRenderObject()
extern void WaterReflection_OnWillRenderObject_m6406F11E93B81C0D209F92F26C5671CD66604F38 (void);
// 0x00000009 System.Void WaterReflection::OnDisable()
extern void WaterReflection_OnDisable_m25A3661620154BBC9D6984C5E13BCE1FC3A9C058 (void);
// 0x0000000A System.Void WaterReflection::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void WaterReflection_UpdateCameraModes_mC381BBB35303EAF9E52A031D83B5E31AE5CCB093 (void);
// 0x0000000B System.Void WaterReflection::CreateMirrorObjects(UnityEngine.Camera,UnityEngine.Camera&)
extern void WaterReflection_CreateMirrorObjects_m6BD5EC942B0A7CDF21AAF1ACE050C6ACB00C0A2A (void);
// 0x0000000C System.Single WaterReflection::sgn(System.Single)
extern void WaterReflection_sgn_m12142C292809AD1E83D600D0C780DFBD28C0660A (void);
// 0x0000000D UnityEngine.Vector4 WaterReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void WaterReflection_CameraSpacePlane_m7000CF7A513730DEFA658C0F0F39A734E9D87568 (void);
// 0x0000000E System.Void WaterReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void WaterReflection_CalculateObliqueMatrix_mFB2263A7FF75702EEE1842AACF2DB8CAD35AAD94 (void);
// 0x0000000F System.Void WaterReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void WaterReflection_CalculateReflectionMatrix_m0F610CCC02B71FE8288ED448D75A1E619BFDD910 (void);
// 0x00000010 System.Void WaterReflection::.ctor()
extern void WaterReflection__ctor_mB05CAAFECE3D78E4AA8BAA81DEC211928CC27614 (void);
// 0x00000011 System.Void WaterReflection::.cctor()
extern void WaterReflection__cctor_m479596F889067BA76E2BE4668594B9CB8BFBC922 (void);
// 0x00000012 System.Void ControlJugadorTwo::Update()
extern void ControlJugadorTwo_Update_m77B83D80F13BF2198DEBD115D9C5C25F427203B6 (void);
// 0x00000013 System.Void ControlJugadorTwo::.ctor()
extern void ControlJugadorTwo__ctor_mA378ECE6FC61759E5B3E574929381D826A977EB5 (void);
// 0x00000014 System.Void DieTwo::Start()
extern void DieTwo_Start_mEBF85EA5B93EF1A9744B17525AA82EE58A090C18 (void);
// 0x00000015 System.Void DieTwo::Update()
extern void DieTwo_Update_m778BBD5C0DCBF5ABE740D90EC70D1869AF52C58A (void);
// 0x00000016 System.Void DieTwo::OnCollisionEnter(UnityEngine.Collision)
extern void DieTwo_OnCollisionEnter_m79CE45EAA7D69537F9A41C07DC0DD5819D450243 (void);
// 0x00000017 System.Void DieTwo::RestaBarra(System.Int32)
extern void DieTwo_RestaBarra_mA245EBE98F7372D06936E74C6CE4075AAD2EBC70 (void);
// 0x00000018 System.Void DieTwo::SumarBarra(System.Int32)
extern void DieTwo_SumarBarra_m86B67EA4E96DE82E5EC005F12AAB8D4D74181796 (void);
// 0x00000019 System.Void DieTwo::Fin()
extern void DieTwo_Fin_m830C4672781083E2DD3E256BBC353CC64CF3336A (void);
// 0x0000001A System.Void DieTwo::.ctor()
extern void DieTwo__ctor_mA129B8FFC09C7429D85A3C16682DD847BCADEC08 (void);
// 0x0000001B System.Void DisparoLlama::Update()
extern void DisparoLlama_Update_m9BE7738DFA1C01D59B9BA5708CCD75AD42392BE2 (void);
// 0x0000001C System.Void DisparoLlama::Disparo()
extern void DisparoLlama_Disparo_m75058404AFBD701B96C8C03C935C1012A198A178 (void);
// 0x0000001D System.Void DisparoLlama::activarBala()
extern void DisparoLlama_activarBala_m468374C3D942DD8D81E7FF8BF0FA4D0F38C43FED (void);
// 0x0000001E System.Void DisparoLlama::.ctor()
extern void DisparoLlama__ctor_m13646602533E552A3EABDDD3751FCD133C2F6F61 (void);
// 0x0000001F System.Void DisparoTwo::Update()
extern void DisparoTwo_Update_m804DB0B4F43307A3BBC2E92A1565C8A04F914EFE (void);
// 0x00000020 System.Void DisparoTwo::.ctor()
extern void DisparoTwo__ctor_m540D423F19591540B0589FA11B6CA0101B01F421 (void);
// 0x00000021 System.Void EfectoExplodsion::OnCollisionEnter(UnityEngine.Collision)
extern void EfectoExplodsion_OnCollisionEnter_m56DC71FF43CC5D6328FD4CC4EABE665F3C24B083 (void);
// 0x00000022 System.Void EfectoExplodsion::ActivarExplosion()
extern void EfectoExplodsion_ActivarExplosion_mC2D467D867F2348C37CEED6A5EC552A217D0457C (void);
// 0x00000023 System.Void EfectoExplodsion::.ctor()
extern void EfectoExplodsion__ctor_m84F4A271D4933D4B899F404A0C8F76CCFC22C6EB (void);
// 0x00000024 System.Void ArmasDestruccion::OnTriggerEnter(UnityEngine.Collider)
extern void ArmasDestruccion_OnTriggerEnter_mDBCAE427D47E75BD889339379B153229666F72F6 (void);
// 0x00000025 System.Void ArmasDestruccion::.ctor()
extern void ArmasDestruccion__ctor_m372F5925EB1BA22D72C0B9A4C0E79729806AA934 (void);
// 0x00000026 System.Void AddScore::Start()
extern void AddScore_Start_mE9F4778CE26303D9F46E08AEB38D057737870AB1 (void);
// 0x00000027 System.Void AddScore::OnTriggerEnter(UnityEngine.Collider)
extern void AddScore_OnTriggerEnter_mFBEF35CE4DCB482159B6E487631E894B966B8DCC (void);
// 0x00000028 System.Void AddScore::Fin()
extern void AddScore_Fin_m0A79A3D17CE63EF1692D689D7ADEF97C6C994D4E (void);
// 0x00000029 System.Void AddScore::.ctor()
extern void AddScore__ctor_mBBC30EDEB19626466167EA38B8725A3502923DB5 (void);
// 0x0000002A System.Void Die::Start()
extern void Die_Start_m3C19E75EF15474563861D09559E101361ED62F2A (void);
// 0x0000002B System.Void Die::Update()
extern void Die_Update_m07D3E26A666D0B3FA889CAF29754CF368754D907 (void);
// 0x0000002C System.Void Die::OnCollisionEnter(UnityEngine.Collision)
extern void Die_OnCollisionEnter_mA88DCF76EFFD12CDDED4DC873EB04AAD50FF9DEB (void);
// 0x0000002D System.Void Die::RestaBarra(System.Int32)
extern void Die_RestaBarra_m5482CA17842841EBD6208D7F6ABD823B530C0701 (void);
// 0x0000002E System.Void Die::SumarBarra(System.Int32)
extern void Die_SumarBarra_m0AEDD947F6C40A2D36B9361AD8D209BD76C6AE5B (void);
// 0x0000002F System.Void Die::Fin()
extern void Die_Fin_mDB9F073F1D73AA86EEAF01E4A3367A069245146B (void);
// 0x00000030 System.Void Die::.ctor()
extern void Die__ctor_mA9AD27E991249B54D4130682E91DF36A5343AC8F (void);
// 0x00000031 System.Void HealtBar::SetMaxHealth(System.Int32)
extern void HealtBar_SetMaxHealth_mB7CBF6B1C98EC5CDAD2D5DD920E4589B2E7EA08D (void);
// 0x00000032 System.Void HealtBar::setHealth(System.Int32)
extern void HealtBar_setHealth_mA275E7208468F5FC2E6E0CCDE76B067E6BAD43DB (void);
// 0x00000033 System.Void HealtBar::.ctor()
extern void HealtBar__ctor_mDEF43F6AC111E2DC4384A01AE84BEC5F0C38C510 (void);
// 0x00000034 System.Void Life::Start()
extern void Life_Start_m17F61456909AB7667D8E144181BA6511A8F5D878 (void);
// 0x00000035 System.Void Life::Update()
extern void Life_Update_m00D2119CADE3A4DF93FE10C8998A4CB2421A41C0 (void);
// 0x00000036 System.Void Life::OnTriggerEnter(UnityEngine.Collision)
extern void Life_OnTriggerEnter_mE1F7FD2FFD2AA37E2ABC67D390F366DF7A6DCB18 (void);
// 0x00000037 System.Void Life::SumarBarra(System.Int32)
extern void Life_SumarBarra_mFC5D71B5D564840DB626F3D1B8A1FEAAA0D6BA32 (void);
// 0x00000038 System.Void Life::.ctor()
extern void Life__ctor_m2531D353B237BD7938F28843C3EE82249D9DE44B (void);
// 0x00000039 System.Void NpcDialogo::Start()
extern void NpcDialogo_Start_m115904B68B46C7072D5ECE0DE51E59BB88669346 (void);
// 0x0000003A System.Void NpcDialogo::Update()
extern void NpcDialogo_Update_mB0F372CEC790FDA24A18C1CA3504683194AFA795 (void);
// 0x0000003B System.Void NpcDialogo::OnTriggerEnter(UnityEngine.Collider)
extern void NpcDialogo_OnTriggerEnter_m9FB976602502F9914771169B72D8B5B5006A796B (void);
// 0x0000003C System.Void NpcDialogo::OnTriggerExit(UnityEngine.Collider)
extern void NpcDialogo_OnTriggerExit_m8F6EF3B7056C87150A0DF544D471C20B3D27B4D1 (void);
// 0x0000003D System.Void NpcDialogo::no()
extern void NpcDialogo_no_m5D8F3B62D615566CCE72879F80ACA3F593D12810 (void);
// 0x0000003E System.Void NpcDialogo::si()
extern void NpcDialogo_si_m8EDFF4D548AC2095288F710A0851A8D93F76109B (void);
// 0x0000003F System.Void NpcDialogo::nso()
extern void NpcDialogo_nso_mD21BA6CCECB62C69F52A52E7CE3BDA0B399893DE (void);
// 0x00000040 System.Void NpcDialogo::.ctor()
extern void NpcDialogo__ctor_m2ADB476FB82B090D1DE6E21E47A3C2BE0E84F8DE (void);
// 0x00000041 System.Void BombaFinal::Start()
extern void BombaFinal_Start_m41E189A15C680C71CF227799D9095CB1E143A25F (void);
// 0x00000042 System.Void BombaFinal::FixedUpdate()
extern void BombaFinal_FixedUpdate_mF82EA7AFBE80B2CB6AF7C5B7ED5A394204564642 (void);
// 0x00000043 System.Void BombaFinal::Detonate()
extern void BombaFinal_Detonate_m000BAF097C601711A3DB8B6EDAA8CDBBDF2C1204 (void);
// 0x00000044 System.Void BombaFinal::Update()
extern void BombaFinal_Update_m5E1263B233E8F21BDA1072CB2CD797446C6C7E91 (void);
// 0x00000045 System.Void BombaFinal::.ctor()
extern void BombaFinal__ctor_mAEBC7F0CB68F2DD68046BD2873240FD3C56CA58D (void);
// 0x00000046 System.Void CommonEnemy::Awake()
extern void CommonEnemy_Awake_mD0B9832CF819A2AE40CF01365A5C39CCB715EA79 (void);
// 0x00000047 System.Void CommonEnemy::Start()
extern void CommonEnemy_Start_m889A4AC8A0A0972B2AB1327C596C2F1FAD48369B (void);
// 0x00000048 System.Void CommonEnemy::Update()
extern void CommonEnemy_Update_mF19C6F84A46BFDE942D9A1AB576E4EE1130CF1D1 (void);
// 0x00000049 System.Void CommonEnemy::CalculateEnemyPos()
extern void CommonEnemy_CalculateEnemyPos_mE0065ED9B9929DD7A0D4D3A8E96355287E58D7F8 (void);
// 0x0000004A System.Void CommonEnemy::AutoLooker()
extern void CommonEnemy_AutoLooker_mD4C377906B3E7435E5ADCED12B2BEBEE640909E5 (void);
// 0x0000004B System.Void CommonEnemy::Attack()
extern void CommonEnemy_Attack_m4F1186F47FCB511C6E0B79DF7F99B399FD29ABBA (void);
// 0x0000004C System.Void CommonEnemy::DestroyEnemy()
extern void CommonEnemy_DestroyEnemy_m6288CC0DC7BD3A7F9E99B49B0F871FEE665B6F9B (void);
// 0x0000004D System.Void CommonEnemy::StopDeathAnim()
extern void CommonEnemy_StopDeathAnim_m8873A4E592BDAA5E61DB5F829483CD39887A4DBA (void);
// 0x0000004E System.Void CommonEnemy::OnDrawGizmos()
extern void CommonEnemy_OnDrawGizmos_mF08F0D62EC6F8CC6B8A3C65E3EDED3880FF07EBE (void);
// 0x0000004F System.Void CommonEnemy::OnCollisionEnter(UnityEngine.Collision)
extern void CommonEnemy_OnCollisionEnter_m91DC87D75F9DE095AFAB2A2E4E34EB068B7FE57B (void);
// 0x00000050 System.Void CommonEnemy::.ctor()
extern void CommonEnemy__ctor_mE71FC7E310EF0D5E68E2565D8C61012C3C22B835 (void);
// 0x00000051 System.Void Instanciador::Start()
extern void Instanciador_Start_mFA9856B4C8534C7406C8E0B4F88C995397F377B7 (void);
// 0x00000052 System.Void Instanciador::Creando()
extern void Instanciador_Creando_m8766C769B17AFF13561CFEDB84776665785D08C7 (void);
// 0x00000053 System.Void Instanciador::.ctor()
extern void Instanciador__ctor_m08B25BD813D0DA839FBE1275962205BBE9265779 (void);
// 0x00000054 System.Void Inventario::Start()
extern void Inventario_Start_mEE9ECEBD3EF5BF997E9C9ED7BCE8130916B9EA75 (void);
// 0x00000055 System.Void Inventario::Update()
extern void Inventario_Update_mD169FC60B57865A0D3DCB4C350052AAD60C7FFD7 (void);
// 0x00000056 System.Void Inventario::OnTriggerEnter(UnityEngine.Collider)
extern void Inventario_OnTriggerEnter_m3A27113EAA1EE79BA76DDA47B6BBA717F8144F4C (void);
// 0x00000057 System.Void Inventario::AddItem(UnityEngine.GameObject,System.Int32,System.String,System.String,UnityEngine.Sprite)
extern void Inventario_AddItem_mB8BF5A99C0ED9903D95FADE1C713601B71FAC4E8 (void);
// 0x00000058 System.Void Inventario::.ctor()
extern void Inventario__ctor_m88E70CBCC688206E3125E38543B7A3CF011822BF (void);
// 0x00000059 System.Void Items::Start()
extern void Items_Start_m3524E61A7AA5BCE3A949CAA8FAF959F658D103F3 (void);
// 0x0000005A System.Void Items::Update()
extern void Items_Update_m9DAD53A3712922D4443D16F8460FF6E40C8A3343 (void);
// 0x0000005B System.Void Items::itemsUsage()
extern void Items_itemsUsage_m0970B1FCE1B1263F908ACFD2A12CF8E276284030 (void);
// 0x0000005C System.Void Items::.ctor()
extern void Items__ctor_m0214104150353BC80C4A757CC60B221265C659F5 (void);
// 0x0000005D System.Void Slots::Start()
extern void Slots_Start_m4023E79568C3D93961B7B9E26260F078057BCAF7 (void);
// 0x0000005E System.Void Slots::UpdateSlot()
extern void Slots_UpdateSlot_mA26D2459E35B0A831D3A76EBE2F375EF289AE1CB (void);
// 0x0000005F System.Void Slots::UseItem()
extern void Slots_UseItem_m646ADDD5AA16A2CD55862F9F998486D0705EFDA9 (void);
// 0x00000060 System.Void Slots::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void Slots_OnPointerClick_mD34D6294563E4594DF7C045BAC22AFC83CE61071 (void);
// 0x00000061 System.Void Slots::.ctor()
extern void Slots__ctor_mED3A94C8025F274E4127A9E84DEE1F6F332D28EF (void);
// 0x00000062 System.Void LanzaLLamas::Update()
extern void LanzaLLamas_Update_mB650DC3B5733BE91B5A499167D9589ABF229AE4B (void);
// 0x00000063 System.Void LanzaLLamas::Disparo()
extern void LanzaLLamas_Disparo_mC4C29418800AF56C89C1FA45B27D2D70DFEE5C71 (void);
// 0x00000064 System.Void LanzaLLamas::activarBala()
extern void LanzaLLamas_activarBala_m312050E79E36A6057A2142BE89D6502A0A8D9660 (void);
// 0x00000065 System.Void LanzaLLamas::.ctor()
extern void LanzaLLamas__ctor_mF5867DB11307CB0EBA543F5C1E00B1BCDEC50E60 (void);
// 0x00000066 System.Void Menu::EscenaJuego()
extern void Menu_EscenaJuego_m7F1A874E228C024E8EC1CFA609A613BD5C760E80 (void);
// 0x00000067 System.Void Menu::CargarNivel(System.String)
extern void Menu_CargarNivel_m146259E5E4923BB0CCD808DCC6CE42BF39939591 (void);
// 0x00000068 System.Void Menu::.ctor()
extern void Menu__ctor_m87A18732D59A6382889C2771A372B622FD6CD58D (void);
// 0x00000069 System.Void Quit::Exit()
extern void Quit_Exit_m3E0A2DA0D53956E42206BD46695EEF1E786E7478 (void);
// 0x0000006A System.Void Quit::.ctor()
extern void Quit__ctor_m4C50BF52D3A6ADB583BE76FE1DEAFB1C889B3F6A (void);
// 0x0000006B System.Void Plataformas::Start()
extern void Plataformas_Start_m3668DA11CA2A142E458BCDF898115C48029F3DB5 (void);
// 0x0000006C System.Void Plataformas::Update()
extern void Plataformas_Update_m610D3CC8D66D2758F95A63F23393B8DC8F6B304D (void);
// 0x0000006D System.Void Plataformas::Creando()
extern void Plataformas_Creando_m988164AE31D37492A3F22B7A2D1A595AA235F4E6 (void);
// 0x0000006E System.Void Plataformas::Prendiendo()
extern void Plataformas_Prendiendo_mF7BE88B1CA9FA1D2340B7A7DD1F6D636636B0CFC (void);
// 0x0000006F System.Void Plataformas::.ctor()
extern void Plataformas__ctor_mFE4879DC787FA04CA2BCE34BA7A36AF220F8EDF2 (void);
// 0x00000070 System.Void Armas::Start()
extern void Armas_Start_m964F5855B67F68FB1C4F3C53067F013B49055E21 (void);
// 0x00000071 System.Void Armas::OnTriggerEnter(UnityEngine.Collider)
extern void Armas_OnTriggerEnter_m6EBA7EC19C1BC452DFFCBC7F77D86CCE22EE2979 (void);
// 0x00000072 System.Void Armas::ArmaGenerador()
extern void Armas_ArmaGenerador_mE3C3D906A047F63521F15C7354C6F6C419A79867 (void);
// 0x00000073 System.Void Armas::.ctor()
extern void Armas__ctor_m7D7234CB5BBECB22C0FC3EB6FDA757178DF709C6 (void);
// 0x00000074 System.Void ControlCamara::Start()
extern void ControlCamara_Start_m3E41AB41B48C305B801CE88E1088E30F6B48065C (void);
// 0x00000075 System.Void ControlCamara::LateUpdate()
extern void ControlCamara_LateUpdate_m01E53FBC184BAEAE7D3B2C0DC1059F7221B1AAF8 (void);
// 0x00000076 System.Void ControlCamara::.ctor()
extern void ControlCamara__ctor_m954699EF3390352A462FE175B54EEB6A85882E34 (void);
// 0x00000077 System.Void Disparo::Update()
extern void Disparo_Update_mEBE4F69B09F60EC8317B69CFF7A4020E273C3F25 (void);
// 0x00000078 System.Void Disparo::.ctor()
extern void Disparo__ctor_mDD5F227B13216300EC5BDEA53801E8517DA5B5A1 (void);
// 0x00000079 System.Void PlayerMove::Update()
extern void PlayerMove_Update_mDF42B2202BD4511D2B1C7075AEC64E69865E8DCF (void);
// 0x0000007A System.Void PlayerMove::.ctor()
extern void PlayerMove__ctor_m85CA761CC1DED13FBB3373A6BAC8CF81B73B77B0 (void);
// 0x0000007B System.Void Rotacion::Update()
extern void Rotacion_Update_mB9EC5458AE4CC7604FAC946B4508A7527F79841D (void);
// 0x0000007C System.Void Rotacion::.ctor()
extern void Rotacion__ctor_mDCFA94CF73551D88EDB0068FEB9619A0230FAE31 (void);
// 0x0000007D System.Void Timer::Update()
extern void Timer_Update_m7320C1508ACA29DE9F062B558142ED29476B6569 (void);
// 0x0000007E System.Void Timer::Explotar()
extern void Timer_Explotar_mEB54856F7E96640D7EFFBF5173BDE5852E3F387A (void);
// 0x0000007F System.Void Timer::.ctor()
extern void Timer__ctor_m970098BC1EE2B392F3744D1FC36524057EC6D98C (void);
// 0x00000080 System.Void BulletMovement::Start()
extern void BulletMovement_Start_m23FD2976922A882D9131970DF55FBCAD774C7315 (void);
// 0x00000081 System.Void BulletMovement::Update()
extern void BulletMovement_Update_m27FED4D8AA57FFB33554678E07CCEB9A18F1C4FA (void);
// 0x00000082 System.Void BulletMovement::.ctor()
extern void BulletMovement__ctor_mA3E6F314BBADDE1AE6FA3C469E0304476527EF48 (void);
// 0x00000083 System.Void DisparoTorreta::Start()
extern void DisparoTorreta_Start_m6CE03746997206BAFFA932750A0B83EDAF201B24 (void);
// 0x00000084 System.Void DisparoTorreta::Update()
extern void DisparoTorreta_Update_m1802EA6988DBA9D8FAB91ABC1DB00A2C75815BD8 (void);
// 0x00000085 System.Collections.IEnumerator DisparoTorreta::FireBullets_CR()
extern void DisparoTorreta_FireBullets_CR_mA9BC91E492005F30EC7CC2CB14754D1C17F8E10E (void);
// 0x00000086 System.Void DisparoTorreta::.ctor()
extern void DisparoTorreta__ctor_mD59A51A720550076E409AF000551A9467AB507E7 (void);
// 0x00000087 System.Void TorretaRotacion::Start()
extern void TorretaRotacion_Start_mACC24A844B090172034034D2ADECBF823077F1C1 (void);
// 0x00000088 System.Void TorretaRotacion::Update()
extern void TorretaRotacion_Update_mD2C3B80E3E489400A932A7E9894BB8A4CE965146 (void);
// 0x00000089 System.Void TorretaRotacion::.ctor()
extern void TorretaRotacion__ctor_mD733F6BE1C824539F0056DE1E893EC26BC124D70 (void);
// 0x0000008A System.Void ChangeSizeColor::Update()
extern void ChangeSizeColor_Update_m0901BB8A1B7D836A85CD137E47C2D2EF15867786 (void);
// 0x0000008B System.Void ChangeSizeColor::ChangeEffectColor(System.Single)
extern void ChangeSizeColor_ChangeEffectColor_m7C3B80778A56E4C44965B7F2D719E6437801954F (void);
// 0x0000008C System.Void ChangeSizeColor::CheckIsColorChange(System.Boolean)
extern void ChangeSizeColor_CheckIsColorChange_m61B3CD982E27E7D219F762F6CE6E3D6BD6D4E305 (void);
// 0x0000008D System.Void ChangeSizeColor::CheckColorState()
extern void ChangeSizeColor_CheckColorState_m984F694A17C11467C09D9092F32687804A611EFC (void);
// 0x0000008E System.Void ChangeSizeColor::GetIntensityFactor()
extern void ChangeSizeColor_GetIntensityFactor_mCC94B810EF38A20696E5C1AF581EAAF654EC5890 (void);
// 0x0000008F System.Void ChangeSizeColor::.ctor()
extern void ChangeSizeColor__ctor_m2CFB9C282816C5AA2D45DABE42BBC68FDF612F5E (void);
// 0x00000090 System.Void DelayActive::Start()
extern void DelayActive_Start_mFF905293823B3376CFBCB44C0621C959A5141CBE (void);
// 0x00000091 System.Void DelayActive::Update()
extern void DelayActive_Update_mFE7BA706A140EE1980E1E3BBCF718BC043D3C47B (void);
// 0x00000092 System.Void DelayActive::.ctor()
extern void DelayActive__ctor_m82F046FBDCE58164EB3BE835880B76A9D99D5B58 (void);
// 0x00000093 System.Void LookAtTarget::Update()
extern void LookAtTarget_Update_m63019289ED3D063B9ADEADD26F281C7FEE4C03F6 (void);
// 0x00000094 System.Void LookAtTarget::.ctor()
extern void LookAtTarget__ctor_mFA983B94A7327E3F43B57E14BDDFAC980F9CB222 (void);
// 0x00000095 System.Void MultipleObjectsMake::Start()
extern void MultipleObjectsMake_Start_mAB28B79703F2E67329851B9D791788302704D7AC (void);
// 0x00000096 System.Void MultipleObjectsMake::Update()
extern void MultipleObjectsMake_Update_m689CBF8EE560FA3C5850C3CE9603C7BB54CF82B4 (void);
// 0x00000097 System.Void MultipleObjectsMake::.ctor()
extern void MultipleObjectsMake__ctor_m35A8C6C64D70AB9F89D465DB2593BD97158F4CAB (void);
// 0x00000098 System.Void ObjectMove::Start()
extern void ObjectMove_Start_mBF8737576548780D2FF8BB82243A41BEB990426B (void);
// 0x00000099 System.Void ObjectMove::LateUpdate()
extern void ObjectMove_LateUpdate_m22492C39D9C6B9002612834CB98F63C2F946B2EC (void);
// 0x0000009A System.Void ObjectMove::HitObj(UnityEngine.RaycastHit)
extern void ObjectMove_HitObj_mB907DEC61F1557205F7CE73929F2E0E2681F2B3D (void);
// 0x0000009B System.Void ObjectMove::.ctor()
extern void ObjectMove__ctor_m52FC29CB90E7342FDBC98CE7EBA128D3ADB92A3B (void);
// 0x0000009C System.Void ObjectMoveDestroy::Start()
extern void ObjectMoveDestroy_Start_mDDC7D2E20D361CA1773339732AED6644B839A4A8 (void);
// 0x0000009D System.Void ObjectMoveDestroy::LateUpdate()
extern void ObjectMoveDestroy_LateUpdate_m74E45401C4D8AAD0E05C9DFB0B468ADACA75C718 (void);
// 0x0000009E System.Void ObjectMoveDestroy::MakeHitObject(UnityEngine.RaycastHit)
extern void ObjectMoveDestroy_MakeHitObject_mC18DF58BD37C6A6A25D8B6E12C9B8335001C46BD (void);
// 0x0000009F System.Void ObjectMoveDestroy::MakeHitObject(UnityEngine.Transform)
extern void ObjectMoveDestroy_MakeHitObject_mFD604A577E4604870300F6C70AF7F573FC71D8E4 (void);
// 0x000000A0 System.Void ObjectMoveDestroy::HitObj(UnityEngine.RaycastHit)
extern void ObjectMoveDestroy_HitObj_m4D3A52A89BAACD7CE3395E5F881DA44619AA24CE (void);
// 0x000000A1 System.Void ObjectMoveDestroy::.ctor()
extern void ObjectMoveDestroy__ctor_mAF50C4DBE05F2AF186DAE277C6FEC4B602E6CFCA (void);
// 0x000000A2 System.Void ShieldActivate::Start()
extern void ShieldActivate_Start_m6B80FA14330D48AB91F97C513F2781D3C28095BA (void);
// 0x000000A3 System.Void ShieldActivate::Update()
extern void ShieldActivate_Update_m24286007D25F4F5A2A25674B48A02180096DBE1A (void);
// 0x000000A4 System.Void ShieldActivate::AddHitObject(UnityEngine.Vector3)
extern void ShieldActivate_AddHitObject_mAD151869B78D2B16E9AB48A243BD240AC6929362 (void);
// 0x000000A5 System.Void ShieldActivate::AddEmpty()
extern void ShieldActivate_AddEmpty_mA6EF00D032666DD5F967A7EF062BA4DF83517653 (void);
// 0x000000A6 System.Void ShieldActivate::.ctor()
extern void ShieldActivate__ctor_mF706D22B19D913A86C48FDC557B37501EF24B811 (void);
// 0x000000A7 UnityEngine.Vector4 ShieldActivate::<Update>b__7_0(UnityEngine.Vector4)
extern void ShieldActivate_U3CUpdateU3Eb__7_0_mDBCA072176F90CB5BAF86F65738C24284E16185D (void);
// 0x000000A8 System.Single _ObjectsMakeBase::GetRandomValue(System.Single)
extern void _ObjectsMakeBase_GetRandomValue_m98D973ACA5E858C0F41BD634CCA95049D471B561 (void);
// 0x000000A9 System.Single _ObjectsMakeBase::GetRandomValue2(System.Single)
extern void _ObjectsMakeBase_GetRandomValue2_m4014821A75453A8DF8C3DD1FA99C315A3FE402BA (void);
// 0x000000AA UnityEngine.Vector3 _ObjectsMakeBase::GetRandomVector(UnityEngine.Vector3)
extern void _ObjectsMakeBase_GetRandomVector_m93FAD631EE4E7FE545C246A073F9C5F02F92E67A (void);
// 0x000000AB UnityEngine.Vector3 _ObjectsMakeBase::GetRandomVector2(UnityEngine.Vector3)
extern void _ObjectsMakeBase_GetRandomVector2_m09CDAE1E67A3DA1DF0E6EDAB9D44CD87435D5CB6 (void);
// 0x000000AC System.Void _ObjectsMakeBase::.ctor()
extern void _ObjectsMakeBase__ctor_m17FAA8E820F557A1BCA28440DF093E52F11FDA97 (void);
// 0x000000AD System.Void NewMaterialChange::Awake()
extern void NewMaterialChange_Awake_m9B4353F2A46071AF4AE40C0F8BF57088821AAB81 (void);
// 0x000000AE System.Void NewMaterialChange::LateUpdate()
extern void NewMaterialChange_LateUpdate_mCDD03615584DE297165D09C5B6BF735597797BA1 (void);
// 0x000000AF System.Void NewMaterialChange::.ctor()
extern void NewMaterialChange__ctor_m399E833138B242F309CA366B367518B1F22888FA (void);
// 0x000000B0 System.Void ScaleFactorApplyToMaterial::Awake()
extern void ScaleFactorApplyToMaterial_Awake_mE2778999EDDAAA330AA2975CE41801132D910F56 (void);
// 0x000000B1 System.Void ScaleFactorApplyToMaterial::Update()
extern void ScaleFactorApplyToMaterial_Update_m4E3F4F9B68CCCFCC675B9571E9E2142D96B0DBF0 (void);
// 0x000000B2 System.Void ScaleFactorApplyToMaterial::.ctor()
extern void ScaleFactorApplyToMaterial__ctor_m12843F1E978FA2EFCDE5C9854B48129384D3AC0D (void);
// 0x000000B3 System.Void VariousEffectsScene::Awake()
extern void VariousEffectsScene_Awake_m318B76C6956D16B155401A075D05EF35CBA10498 (void);
// 0x000000B4 System.Void VariousEffectsScene::Update()
extern void VariousEffectsScene_Update_mC974A0BB2B415EC9EF1D5EBDFC7946EA29D0CD73 (void);
// 0x000000B5 System.Void VariousEffectsScene::InputKey()
extern void VariousEffectsScene_InputKey_mE1C4392BDE0EABDEC09301C27500247E224C94B8 (void);
// 0x000000B6 System.Void VariousEffectsScene::MakeObject()
extern void VariousEffectsScene_MakeObject_m86DC07D7C18B90F16884AB56A44683993DD22083 (void);
// 0x000000B7 System.Void VariousEffectsScene::DestroyGameObject()
extern void VariousEffectsScene_DestroyGameObject_m372877CF191E21CE8351EA263F5B4787E6F70E9D (void);
// 0x000000B8 System.Void VariousEffectsScene::GetSizeFactor()
extern void VariousEffectsScene_GetSizeFactor_m737C9C3DBEC3E85D6BA730A3CAF11C8552EAE05D (void);
// 0x000000B9 System.Void VariousEffectsScene::.ctor()
extern void VariousEffectsScene__ctor_mD623B9C19F20F89BDE364CE6C6F598A7D20A9F74 (void);
// 0x000000BA System.Void VariousEffectsScene::.cctor()
extern void VariousEffectsScene__cctor_m85504A94406A210C80B8F8D01E3EB6131F7EBFAF (void);
// 0x000000BB System.Void VariousMouseOrbit::Start()
extern void VariousMouseOrbit_Start_m2D5B0F4C454BB46BBD7E9C2C6E7D7E462C2BF9A7 (void);
// 0x000000BC System.Void VariousMouseOrbit::LateUpdate()
extern void VariousMouseOrbit_LateUpdate_m1DBD26830404891D0E5228BF9B03ECDA50D101FD (void);
// 0x000000BD System.Single VariousMouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern void VariousMouseOrbit_ClampAngle_m99DC88A57D9A93DF33D1DB1680F6115AA064D6EC (void);
// 0x000000BE System.Void VariousMouseOrbit::.ctor()
extern void VariousMouseOrbit__ctor_m3DEC0502D5B32AC2EC856D3513FFD0B252CA599D (void);
// 0x000000BF System.Void VariousRotateObject::Awake()
extern void VariousRotateObject_Awake_mBE29D4FCF2DD907029A62C6CC748ED24804BC8A9 (void);
// 0x000000C0 System.Void VariousRotateObject::Update()
extern void VariousRotateObject_Update_m821CDE9BED198A1FA3737E596FFA73D0B2B3D222 (void);
// 0x000000C1 System.Void VariousRotateObject::.ctor()
extern void VariousRotateObject__ctor_m0DA7EF4B9DA6811B6D63329E09F62182802298A0 (void);
// 0x000000C2 System.Void VariousTranslateMove::Start()
extern void VariousTranslateMove_Start_m44B70B62B1F4050DDC89770849AC30E3CBB7CA20 (void);
// 0x000000C3 System.Void VariousTranslateMove::Update()
extern void VariousTranslateMove_Update_mED87581A9680380C50660AA2797544280BD1A213 (void);
// 0x000000C4 System.Void VariousTranslateMove::.ctor()
extern void VariousTranslateMove__ctor_mBD6FA24AE7CA3B238B7DEDE78527D2F53F77644A (void);
// 0x000000C5 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mD13A02B63932BDA275E1A788FD72D86D69B9E440 (void);
// 0x000000C6 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mD31D1ED1B2C3C82986BFBD18FA584D45167431F3 (void);
// 0x000000C7 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m01CC3E959ACECC222DFD8541231EC1E00C024194 (void);
// 0x000000C8 System.Void ChatController::.ctor()
extern void ChatController__ctor_mF4343BA56301C6825EB0A71EBF9600525B437BCD (void);
// 0x000000C9 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m31A59FA78B78A6457BB20EC10C08C82E4C669140 (void);
// 0x000000CA System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_mA06B4508E4574A97B9B0A20B0273557F5AD068D7 (void);
// 0x000000CB System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mAB3C67FA11192EFB31545F42D3D8AAB2A662FEB2 (void);
// 0x000000CC System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_m21098EFD0904DFD43A3CB2FF536E83F1593C2412 (void);
// 0x000000CD System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mBA9215F94AB29FBA4D3335AEA6FAB50567509E74 (void);
// 0x000000CE System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_mD139A23C440A026E47FA8E3AD01AD6FEF7713C3D (void);
// 0x000000CF System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_mF6477F5EB75EC15CD6B81ACD85271F854BABC5D6 (void);
// 0x000000D0 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mCA5EA200223A9F224F2F4DBD306DAE038C71A35F (void);
// 0x000000D1 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mBB38130850945A40631821275F07C19720E0C55E (void);
// 0x000000D2 TMPro.TMP_TextEventHandler_CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF (void);
// 0x000000D3 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler_CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m237C99FE66E4E16518DAE68FF9CBF1A52E816AD2 (void);
// 0x000000D4 TMPro.TMP_TextEventHandler_SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4 (void);
// 0x000000D5 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler_SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_mAAE4B440E34EE5736D43D6A8A7D3A7CEE0503D69 (void);
// 0x000000D6 TMPro.TMP_TextEventHandler_WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E (void);
// 0x000000D7 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler_WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m6062C0AF2FDD8752DC4A75663EE8E5C128504698 (void);
// 0x000000D8 TMPro.TMP_TextEventHandler_LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B (void);
// 0x000000D9 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler_LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m1A8E37D2069EF684EF930D4F1ABE764AE17D9A62 (void);
// 0x000000DA TMPro.TMP_TextEventHandler_LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D (void);
// 0x000000DB System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler_LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m1376CC9B70177B0C25ACEDF91D5B94BC4B8CF71D (void);
// 0x000000DC System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m9A353CC9705A9E824A60C3D2D026A7FD96B41D74 (void);
// 0x000000DD System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_m2F3241223A91F9C50E11B27F67BA2B6D19328B72 (void);
// 0x000000DE System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m1827A9D3F08839023DE71352202FE5F744E150EF (void);
// 0x000000DF System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_m788B93D2C3B54BCF09475675B274BCB047D449FB (void);
// 0x000000E0 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_mBC44C107A6FB8C43F7C6629D4A15CA85471A28B2 (void);
// 0x000000E1 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_mEF24BCE06B0CE4450B6AE9561EC4B5052DAF00F6 (void);
// 0x000000E2 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m7C4D266070EE2ADC66BCCFD50EB74FEB4923B77E (void);
// 0x000000E3 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mAAF4AF44929D0C9FD73C89E5266028908074AEB1 (void);
// 0x000000E4 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m082D12F7D044456D8514E4D31944C6900F8262C0 (void);
// 0x000000E5 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_mEA56AE9489B50CF5E5FC682AA18D1CE9AF8E1F8B (void);
// 0x000000E6 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mC0055F208B85783F0B3DB942137439C897552571 (void);
// 0x000000E7 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m2FA501D2C572C46A61635E0A3E2FF45EC3A3749C (void);
// 0x000000E8 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m976ED5172DEAFC628DE7C4C51DF25B1373C7846A (void);
// 0x000000E9 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mD92CA5A254960EB149966C2A3243514596C96EAD (void);
// 0x000000EA System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m2D028BFC6EFB4C84C1A7A98B87A509B27E75BA06 (void);
// 0x000000EB System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m7CA524C53D9E88510EE7987E680F49E8353E4B64 (void);
// 0x000000EC System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_m8D1A987C39FD4756642011D01F35BDC3B1F99403 (void);
// 0x000000ED System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m73F65BA012D86A6BE17E82012AE8E2339CA5D550 (void);
// 0x000000EE System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m9A5E67EA64AAC56D56C7D269CC9685E78276360A (void);
// 0x000000EF System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_m22D98FCFC356D5CD7F401DE7EDCDAF7AE0219402 (void);
// 0x000000F0 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_mDAC3E3BE80C9236562EFB6E74DEBE67D8713101D (void);
// 0x000000F1 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m581B79998DB6946746CBF7380AFCC7F2B75D99F7 (void);
// 0x000000F2 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_mA9D72DB0BB6E4F72192DA91BC9F8918A9C61B676 (void);
// 0x000000F3 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_mDC862C8119AB0B4807245CC3482F027842EAB425 (void);
// 0x000000F4 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m198C209AC84EF7A2437ADB2B67F6B78D12AB9216 (void);
// 0x000000F5 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m2108A608ABD8EA7FD2B47EE40C07F4117BB7607E (void);
// 0x000000F6 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mDA26D26457D277CC2D9042F3BD623D48849440C4 (void);
// 0x000000F7 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mC50AAC1AF75B07CD6753EA3224C369E43001791B (void);
// 0x000000F8 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m2832B9D713355ECF861642D115F86AA64A6F119E (void);
// 0x000000F9 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_m8E01638EBFE80CC0B9E4A97AB809B91E3C6956BE (void);
// 0x000000FA System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_m24F4FADC328B0C76264DE24663CFA914EA94D1FD (void);
// 0x000000FB System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mC45F318132D23804CBF73EA2445EF7589C2333E9 (void);
// 0x000000FC System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mC3894CE97A12F50FB225CA8F6F05A7B3CA4B0623 (void);
// 0x000000FD System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m22A3AE8E48128DF849EE2957F4EF881A433CA8CB (void);
// 0x000000FE System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_m742F828A2245E8CC29BC045A999C5E527931DFF1 (void);
// 0x000000FF System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mA2284F621031B4D494AC06B687AF43D2D1D89BD7 (void);
// 0x00000100 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m51C217E0CB26C2E627BA01599147F69B893EF189 (void);
// 0x00000101 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m6A9CEFA12DB252E297E41E256698DD4E90809F6A (void);
// 0x00000102 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B (void);
// 0x00000103 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m4A69C47EA665D49482B930F924E49C8E70FAC225 (void);
// 0x00000104 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m11DC90EB1A059F4201457E33C4422A7BDA90F099 (void);
// 0x00000105 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m9CE8A9F929B99B2318A6F8598EE20E1D4E842ECD (void);
// 0x00000106 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m8F48CBCC48D4CD26F731BA82ECBAC9DC0392AE0D (void);
// 0x00000107 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m9F5CE74EDA110F7539B4081CF3EE6B9FCF40D4A7 (void);
// 0x00000108 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m906CC32CE5FE551DF29928581FFF7DE589C501F2 (void);
// 0x00000109 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_m614FA9DE53ECB1CF4C6AF6BBC58CE35CA904EB32 (void);
// 0x0000010A System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m16AB65EF6AB38F237F5A6D2D412AB7E5BF7B1349 (void);
// 0x0000010B System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m537A709F25C3AA752437A025BEE741BD2F71320E (void);
// 0x0000010C System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mD86AC3A8D918D14200BF80A354E0E43DC5A565A2 (void);
// 0x0000010D System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_m22D9B03F3E1269B8B104E76DA083ED105029258A (void);
// 0x0000010E System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m42813B343A1FDD155C6BFBFCB514E084FB528DA0 (void);
// 0x0000010F System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mC6992B7B1B6A441DEC5315185E3CE022BB567D61 (void);
// 0x00000110 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mEC541297C2228C26AB54F825705F0476D45F877A (void);
// 0x00000111 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_mA1170F805C77CC89B818D8FBEE533846AF66509C (void);
// 0x00000112 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mB871339347DCB016E019F509A00BDE9A58105822 (void);
// 0x00000113 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m44A79DDBDF03F254BAFB97BE3E42845B769136C5 (void);
// 0x00000114 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA67343988C9E9B71C981A9FFAD620C4A9A6AA267 (void);
// 0x00000115 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m1EA6A5E31F88A1C7E20167A3BCCE427E9E828116 (void);
// 0x00000116 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m82972EF3AF67EAAFD94A5EE3EA852CE15BE37FC1 (void);
// 0x00000117 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m40594D716F53E6E5BC0ECD2FFE8ECA44FAA5C8E4 (void);
// 0x00000118 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m8462C2DC4F71BDE295BE446B213B73F78442E264 (void);
// 0x00000119 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m3AC7467ECE689A58590DA325F8B300B08C1E1B5D (void);
// 0x0000011A System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m081D44F31AA16E345F914869A07BD47D118707DF (void);
// 0x0000011B System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m217B6E2FC4029A304908EE9DC1E4AA2885CBF8A3 (void);
// 0x0000011C System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m24A7CCA0D93F17AC1A12A340277C706B5C2F9BAB (void);
// 0x0000011D System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m6088452529A70A6684BD8936872B71451779A2F4 (void);
// 0x0000011E System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m79EEE4DF7792F553F5DEDCF0094DAC6F2A58137A (void);
// 0x0000011F System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m745577078D86EF6C23B914BD03EA1A1D169B9B7B (void);
// 0x00000120 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mF6C09A2C64F5D2619014ADD50039358FAD24DB3E (void);
// 0x00000121 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_mB0AAA8D034FC575EB3BCF7B0D4514BD110178AD3 (void);
// 0x00000122 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m13B20506F762769F099DE10B3CCA2DF194192B42 (void);
// 0x00000123 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mD53FD60E0C5930231FB16BDEA37165FF46D85F6E (void);
// 0x00000124 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m1D03D0E14D6054D292334C19030256B666ACDA0E (void);
// 0x00000125 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_m494C501CF565B1ED7C8CB2951EB6FB4F8505637F (void);
// 0x00000126 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m255A7821E5BA4A7A75B9276E07BC9EA7331B5AA6 (void);
// 0x00000127 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_mA4A02EB5C853A44F251F43F0AD5967AE914E2B0F (void);
// 0x00000128 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m04555F8DF147C553FA2D59E33E744901D811B615 (void);
// 0x00000129 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter_FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDF0FDFBCD11955E0E1D1C9E961B6AD0690C669ED (void);
// 0x0000012A System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m99ACA1D2410917C5837321FC5AC84EAED676D4CC (void);
// 0x0000012B System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m639E300B56757BDB94766447365E1C94B5B83ACD (void);
// 0x0000012C System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay_FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m74B8F0AE15DA6C9968C482981EBCF5CC9DB1F43D (void);
// 0x0000012D System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m570C4B4CB3126622D6DFF71158336313C45C717A (void);
// 0x0000012E System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m5F758974DA88ED8187E71A5100D2D9E47985E359 (void);
// 0x0000012F System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_mC32B726B6202883E12E1A62A52E50092E7E9D9F0 (void);
// 0x00000130 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m6CDFDC88D47FE66021C133974C8CB0E16B08A00E (void);
// 0x00000131 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_m4F61F06DFE11CFAF9B064CCA5B2D6423D5CFC302 (void);
// 0x00000132 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m572903C9070A8AD276D2CB14DF7659AE551C75B3 (void);
// 0x00000133 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m1A36B043E4EDD945C93DFC49F7FAFB7034728593 (void);
// 0x00000134 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m6F9BE1975CB15EE559D3B617E8972C8812B41325 (void);
// 0x00000135 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m73C6B3DAA27778B666B9B3B75C9D4641FC1BEC8A (void);
// 0x00000136 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mC24F1D67B99F0AFE7535143BB60C530C8E8735F0 (void);
// 0x00000137 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m68CAA9BDC1DF3454ECB7C5496A5A2020F84027B8 (void);
// 0x00000138 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4DB9B9E3836D192AA7F42B7EBDC31883E39610E9 (void);
// 0x00000139 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_mD99CC6A70945373DA699327F5A0D0C4516A7D02A (void);
// 0x0000013A System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m3286BB7639F6042AD4437D41BF4633326C4290BE (void);
// 0x0000013B System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m61ABFBAA95ED83A248FBCC3F5823907824434D34 (void);
// 0x0000013C System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m65A61DF0BA640F2787DD87E93B8FD9DEC14AACB4 (void);
// 0x0000013D System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m11DBC535BA8001B48A06F61515C0787C3A86611F (void);
// 0x0000013E System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mB3C405B4856E9B437E13E4BD85DAE73FFF1F6561 (void);
// 0x0000013F System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m436D4CD2A82C0F95B8D942DF4CCFCE09792EDF0F (void);
// 0x00000140 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_mDB693DABF1C3BA92B7A3A4E1460F28E3FAFB444D (void);
// 0x00000141 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_mAEEAA831C084B447DFD0C91A5FA606CB26D3E22A (void);
// 0x00000142 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mF01B64B3E5FE5B648DE2EED031962D9183C2D238 (void);
// 0x00000143 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m9CDB87631C324FB89FA7D52F5BC910146F3DDEB7 (void);
// 0x00000144 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m9B68D69B87E07DC0154344E5276EFC5B11205718 (void);
// 0x00000145 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9699A62E72D3A262EDFDF7AC63ABD33E53F179B6 (void);
// 0x00000146 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m1B592E7AC81C7F17D0A59325EBDE71E067178E8A (void);
// 0x00000147 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_m5567B541D1602AD54B0F437D4710BCD1951FE2C5 (void);
// 0x00000148 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mB698E212B8C3B7C66C71C88F4761A4F33FCE4683 (void);
// 0x00000149 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_m09CFD4E872042E7377BEC8B95D34F22F62ABC45B (void);
// 0x0000014A System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m1CCF60CA14B2FF530950D366FF078281ADC48FF4 (void);
// 0x0000014B System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_mC19C148659C8C97357DB56F36E14914133DA93CF (void);
// 0x0000014C System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_mD2ABEB338822E0DBCFDEC1DB46EC20BB2C44A8C2 (void);
// 0x0000014D System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m60947EACA10408B6EAD2EC7AC77B4E54E2666DD8 (void);
// 0x0000014E System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_mF5AF9069E523C0DF610EF3BE2017E73A4609E3CC (void);
// 0x0000014F System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m96D17C13A279F9B442588F6448672BCF07E4A409 (void);
// 0x00000150 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_mE761EE13D2F9573841EFA5DEE82E545545280BFA (void);
// 0x00000151 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC97FED540BDE59254AB30C5E6BCF1F53B766945F (void);
// 0x00000152 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_mD84B9A0167705D5D3C8CA024D479DC7B5362E67B (void);
// 0x00000153 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mA59F3CA197B3A474F4D795E2B3F182179FF633CF (void);
// 0x00000154 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m8D4DD5CA81E5B0C02937D43C1782533D54F34B3F (void);
// 0x00000155 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m4473873008D4A843A26F0D2353FC36ABE74CEED2 (void);
// 0x00000156 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m24BEF670ABD1CCC864FAFE04750FEB83D916D0DF (void);
// 0x00000157 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mCC864EE9569AF68F53F6EAEB2CE8077CFEAF9E53 (void);
// 0x00000158 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mAF3B23F4DD98AC3E641700B994B2994C14F0E12C (void);
// 0x00000159 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_mDCAEC737A20F161914DD12A2FCBAB6AB7C64FEF2 (void);
// 0x0000015A System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m5F98434E568929859E250743BA214C0782D17F2B (void);
// 0x0000015B System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m01E35B4259BA0B13EC5DA18A11535C2EC6344123 (void);
// 0x0000015C System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_mACD450919605863EC4C36DA360898BAEBBF3DDDB (void);
// 0x0000015D System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m6910FF4AC88466E3B7DB867AD553429F1745320D (void);
// 0x0000015E System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mBFB58FE53145750AD747B38D9D1E7E00F8DBF9A0 (void);
// 0x0000015F System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m328088EC0F893B3E60BB072F77ADF939B8566E4D (void);
// 0x00000160 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_m67833553C2739616BF059C661F92A032885510B2 (void);
// 0x00000161 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m9643904751E2DF0A7DF536847AEA1A2B0774DD20 (void);
// 0x00000162 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_mDF931C271901519BF21FD356F706FA8CDE236406 (void);
// 0x00000163 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038 (void);
// 0x00000164 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mAAAB1687869E0121C858C65BDB2D294D55CFB67A (void);
// 0x00000165 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mC3EAA1AE81FB3DA4B25F20E557EC6627A06DCB31 (void);
// 0x00000166 System.Void Polyart.FoliageInteractor::Update()
extern void FoliageInteractor_Update_mF6E7F7F992819E4615446327CD697C28A2D6E3B2 (void);
// 0x00000167 System.Void Polyart.FoliageInteractor::.ctor()
extern void FoliageInteractor__ctor_m24C8183DB53E344FB64BB71039E38B13F5B58735 (void);
// 0x00000168 System.Void DisparoTorreta_<FireBullets_CR>d__6::.ctor(System.Int32)
extern void U3CFireBullets_CRU3Ed__6__ctor_m3EC7F4118AE65B212E78016BFA1E3293B17377B6 (void);
// 0x00000169 System.Void DisparoTorreta_<FireBullets_CR>d__6::System.IDisposable.Dispose()
extern void U3CFireBullets_CRU3Ed__6_System_IDisposable_Dispose_m9DE3CBE6806B7047A9EC62519D0207E5212B762F (void);
// 0x0000016A System.Boolean DisparoTorreta_<FireBullets_CR>d__6::MoveNext()
extern void U3CFireBullets_CRU3Ed__6_MoveNext_mCA115BF9D601591260A784C8CDE0D9B54ACC7DA6 (void);
// 0x0000016B System.Object DisparoTorreta_<FireBullets_CR>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireBullets_CRU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51859FA611B3CBD28C1FE935DED541EBCDD3B53B (void);
// 0x0000016C System.Void DisparoTorreta_<FireBullets_CR>d__6::System.Collections.IEnumerator.Reset()
extern void U3CFireBullets_CRU3Ed__6_System_Collections_IEnumerator_Reset_mEB5C75A303835CF75F22E1A90B9ECE2501062EF8 (void);
// 0x0000016D System.Object DisparoTorreta_<FireBullets_CR>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CFireBullets_CRU3Ed__6_System_Collections_IEnumerator_get_Current_m7E7F110997CE8F7751C6BA699DA36FCE192D1B08 (void);
// 0x0000016E System.Void ShieldActivate_<>c::.cctor()
extern void U3CU3Ec__cctor_m988B3726B871813020E7C2EDDAB8CEB2E67DEEEA (void);
// 0x0000016F System.Void ShieldActivate_<>c::.ctor()
extern void U3CU3Ec__ctor_m3653D140BE00AE006E397893DEF7E9E03DDB1EC7 (void);
// 0x00000170 System.Boolean ShieldActivate_<>c::<Update>b__7_1(UnityEngine.Vector4)
extern void U3CU3Ec_U3CUpdateU3Eb__7_1_mEB85FA29526D4B119D5BF22ED0177A534728049A (void);
// 0x00000171 System.Void EnvMapAnimator_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m004DF17C34E6A9C76325BD4C65E0F328AD4B37E0 (void);
// 0x00000172 System.Void EnvMapAnimator_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mE7E20C5789828A8FDE263BD5ACC2D02982334B46 (void);
// 0x00000173 System.Boolean EnvMapAnimator_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mB3F55ABCABBA6717942BDC85935FDE439D09E226 (void);
// 0x00000174 System.Object EnvMapAnimator_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF976D094846BDC403335C76F41CAC26D53C8F1D2 (void);
// 0x00000175 System.Void EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9368B3951732B0D18993DA1B889346A775F252CE (void);
// 0x00000176 System.Object EnvMapAnimator_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m495592283D84E6B446F7B9B7405F250D9687DEFB (void);
// 0x00000177 System.Void TMPro.TMP_TextEventHandler_CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m036DA7F340B0839696EB50045AB186BD1046BE85 (void);
// 0x00000178 System.Void TMPro.TMP_TextEventHandler_SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0BC042938C4EBBB77FFAD68C1ACD74FC1C3C1052 (void);
// 0x00000179 System.Void TMPro.TMP_TextEventHandler_WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m1C01733FD9860337084DFE63607ECE0EF8A450EA (void);
// 0x0000017A System.Void TMPro.TMP_TextEventHandler_LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m1C3A0C84C5C0FEA6C33FA9ED99756A85363C9EF2 (void);
// 0x0000017B System.Void TMPro.TMP_TextEventHandler_LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_mC7034F51586C51D1DE381F6222816DC1542AFF3A (void);
// 0x0000017C System.Void TMPro.Examples.Benchmark01_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m139DF863E59AD287A6C14228BB59D56E7FD2E578 (void);
// 0x0000017D System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mBBFAE2F68813477259A0B665B4E81833C03C746B (void);
// 0x0000017E System.Boolean TMPro.Examples.Benchmark01_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m8D27A150B4BC045DD5A1ACB2FABBB7F7F318A015 (void);
// 0x0000017F System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C55687F407BA372889D6A533FB817E1EA81C165 (void);
// 0x00000180 System.Void TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mE725DFFE744FAEDABF70579D04BBEB17A6CFF692 (void);
// 0x00000181 System.Object TMPro.Examples.Benchmark01_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD8EEDE3D2E1E9838472D20AE93DD750D1EE39AF8 (void);
// 0x00000182 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mECD4DB9695B4D04CEF08DF193DAFA21412DA40EF (void);
// 0x00000183 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m6773C67C5C6E7E52F8C8545181173A58A6B7D939 (void);
// 0x00000184 System.Boolean TMPro.Examples.Benchmark01_UGUI_<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mB823F98B1B0907B6959EAEE4D1EBE0AA35795EA3 (void);
// 0x00000185 System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF100CFC13DED969B3BBE2007B3F863DCE919D978 (void);
// 0x00000186 System.Void TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mDE1D3A30ECA00E3CA2218A582F2C87EEE082E2DB (void);
// 0x00000187 System.Object TMPro.Examples.Benchmark01_UGUI_<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m13710692FCBB7BB736B1F6446778124064802C83 (void);
// 0x00000188 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mB4DA3EEEFC5ECB8376EF29EAC034162B575961B8 (void);
// 0x00000189 System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mC6A68A27A902E234429E3706D6DB432BEA04A384 (void);
// 0x0000018A System.Boolean TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_mDE142F6B3AAB5916834A6820A5E7B13A7B71E822 (void);
// 0x0000018B System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F4D59DDC372B977CD7297AA5F0C83D4FFBBA830 (void);
// 0x0000018C System.Void TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mAC1E960F7FFCF032EE668635835954377D1469F6 (void);
// 0x0000018D System.Object TMPro.Examples.ShaderPropAnimator_<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m96B6B8A0D715268DB789F3D4C60FC6DEC9E1F6E6 (void);
// 0x0000018E System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m07871FDF578BC130082658B43FB4322C15F0909E (void);
// 0x0000018F System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m3C8BC1501397E256464ADD27A32486CDE63C2BE2 (void);
// 0x00000190 System.Boolean TMPro.Examples.SkewTextExample_<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m11D4701B069FCFC106319CBDCA56244EFA4C795F (void);
// 0x00000191 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m995FCE4A3B9B270605168D01EF7439A437864C06 (void);
// 0x00000192 System.Void TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m9970CA113E1A293472987C004B42771993CAA05C (void);
// 0x00000193 System.Object TMPro.Examples.SkewTextExample_<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m398E9D0D09F26D5C3268EB41936ED92240474910 (void);
// 0x00000194 System.Void TMPro.Examples.TeleType_<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mFB2D6F55665AAA83D38C58F58FA9DD0F5CE51351 (void);
// 0x00000195 System.Void TMPro.Examples.TeleType_<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m72D91E3700B8E1F2053A7620793F97E01C1A2E3A (void);
// 0x00000196 System.Boolean TMPro.Examples.TeleType_<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2839A135AFB4518430283897981AF4C91ABDBC6A (void);
// 0x00000197 System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E80ED07333F946FC221C16A77997DF51A80F87 (void);
// 0x00000198 System.Void TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mAC8E620F4FCE24A409840017FB003AA1048497EF (void);
// 0x00000199 System.Object TMPro.Examples.TeleType_<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mD6DFD0886CCA250CB95B2828DEEEEE5EA6DC303A (void);
// 0x0000019A System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mAF579198F26F3FD002CB7F4919CCB513E2B770E1 (void);
// 0x0000019B System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m2B3AB12689498DE28F90CF5DA3D40AA6C31B928E (void);
// 0x0000019C System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_m16C2594A51B9D102B30646C47111F3476FFBF311 (void);
// 0x0000019D System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B5325041D4E4A3F3D5575373F25A93752D9E514 (void);
// 0x0000019E System.Void TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m321E1A1FD7DE7840F8433BF739D3E889FB3E9C7C (void);
// 0x0000019F System.Object TMPro.Examples.TextConsoleSimulator_<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9CF6271A74900FFA5BD4027E84D63C164C1650BC (void);
// 0x000001A0 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m5D2D48675C51D6CBD649C1AAD44A80CCA291F310 (void);
// 0x000001A1 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m9F97B9D63E30AF41B31E44C66F31FE6B22DDFD4C (void);
// 0x000001A2 System.Boolean TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m19EB0E15617A1893EEF1916629334CED1D8E9EA1 (void);
// 0x000001A3 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E45A0AFB76FA46A5DA4E92E3FE114E310D8643 (void);
// 0x000001A4 System.Void TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m09CF6739322B1DB072CB8CFE591DBBC046008E63 (void);
// 0x000001A5 System.Object TMPro.Examples.TextConsoleSimulator_<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m2083C1001867012CC47AFA8158F00ED15527C603 (void);
// 0x000001A6 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m7E5E121E510EFDCCF0D565EBBF607F80836EBB66 (void);
// 0x000001A7 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_mD0C99DF97552E843D1E86CF689372B3759134BB7 (void);
// 0x000001A8 System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mA0FD89A2C5D0867977DAB0896DAB041EDFA64200 (void);
// 0x000001A9 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m735F43BD7983BCC8E417573886ADC91CDED8F29B (void);
// 0x000001AA System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mBE24EA2EDAFC5FD22D633595174B122D5F84BB02 (void);
// 0x000001AB System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshProFloatingText>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m4199EEF69BC21DC6DDC2A0D0323AB70508CB1194 (void);
// 0x000001AC System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m664F9327A457FB4D53F20172479BB74A4B50675A (void);
// 0x000001AD System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_mA301D3B10770F41C50E25B8785CE78B373BBCE7C (void);
// 0x000001AE System.Boolean TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m96E4CEDBAD5AA7AC7C96A481502B002BC711725F (void);
// 0x000001AF System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7609D75AFDAE758DAAE60202465AB3733475352B (void);
// 0x000001B0 System.Void TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m6F0C933685FE89DF2DECC34EDD2FE5AD10542579 (void);
// 0x000001B1 System.Object TMPro.Examples.TextMeshProFloatingText_<DisplayTextMeshFloatingText>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m29F3BB8ADAADB8FD750636C535D2B50292E2DE3B (void);
// 0x000001B2 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_mD05EA47C6D3F9DC7BE5B4F687A62244E48BC3808 (void);
// 0x000001B3 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5E0C78AE94BC2C9BD7818EF0DD43D46ABF8C0172 (void);
// 0x000001B4 System.Boolean TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m8ABBF64D39EB5C5AC87AE0D833B6CBE570444347 (void);
// 0x000001B5 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m328E88E5192D85CDD2157E26CD15CC0B21149AB6 (void);
// 0x000001B6 System.Void TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_mB49B7C6E42E5B1F488ECA85B1B79AC1D6BBC3022 (void);
// 0x000001B7 System.Object TMPro.Examples.VertexColorCycler_<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m6966F60B8B8F4540B175D0C145D8A73C89CE5429 (void);
// 0x000001B8 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m14F85BFAE5EFFAF0BBD6519F6A65B1EE36FC5F0F (void);
// 0x000001B9 System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m36A05E300F1E06AE13E2BFF19C18A59F3E73424A (void);
// 0x000001BA System.Boolean TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m95629D41B32EBFFE935AA88081C00EB958D17F53 (void);
// 0x000001BB System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m994AE653E10219CEB601B88CC22E332FF8AD1F36 (void);
// 0x000001BC System.Void TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mE8D9B5795F095798688B43CF40C04C3FE3F01841 (void);
// 0x000001BD System.Object TMPro.Examples.VertexJitter_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m12F2FCDBABCD9F41374064CAD53515D2D039EFD0 (void);
// 0x000001BE System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m93C153A3293F3E7F9AB47C712F5D8BC9CB0B179D (void);
// 0x000001BF System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m6E2E964131F208551826E353B282FBB9477CB002 (void);
// 0x000001C0 System.Boolean TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mD5F89478A4FDEA52E1261CCCF30CC88B0D693407 (void);
// 0x000001C1 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6A7BC75B026CFA96FA1C123E6F7E5A5AAFC46E9 (void);
// 0x000001C2 System.Void TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m28679C8DDFAAD809DD115DB68829543159F47FBD (void);
// 0x000001C3 System.Object TMPro.Examples.VertexShakeA_<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m85E44906BDBB53959D8A47AE74B74420179AD3EC (void);
// 0x000001C4 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m9B592C7897687114428DF0983D5CE52A21051818 (void);
// 0x000001C5 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m24A98FB24DAE578F96B90B3A6D61D18400A731B1 (void);
// 0x000001C6 System.Boolean TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m52F01BB7305705B9DE4309E1A05E2725BA06672E (void);
// 0x000001C7 System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB05E3BDEFD461F6516F45D404DBDEC452C646D37 (void);
// 0x000001C8 System.Void TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m72086FED04B8AC40B92836B894D6807AFB51BB38 (void);
// 0x000001C9 System.Object TMPro.Examples.VertexShakeB_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA1E44018FA1464D953241F5FA414715C9ADE98CF (void);
// 0x000001CA System.Void TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mF154C9990B4AF8DA353F6A8C115C96FB7CB76410 (void);
// 0x000001CB System.Int32 TMPro.Examples.VertexZoom_<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mDEE1E28BD53D02CB2E40D0C263BBA65C0B5AC66C (void);
// 0x000001CC System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m92C755B17AC00199A759541B62E354765068E7F1 (void);
// 0x000001CD System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mBA95580B1808E4EB047CD7F082595E1C5EE930C2 (void);
// 0x000001CE System.Boolean TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mA307F1943028D7555032189B48F6289E09D2E220 (void);
// 0x000001CF System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288CCD0BF91027A0CE3AE12D301F3E5189F0DCE (void);
// 0x000001D0 System.Void TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m7C214643A52D954B2755D3477C3D0BFC85DAFF8E (void);
// 0x000001D1 System.Object TMPro.Examples.VertexZoom_<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m666CA82A5C974B5AC09F277AFC97A82D1D1C365E (void);
// 0x000001D2 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m63F411BEA2E513D84AAA701A1EDF0D0322FDE9C4 (void);
// 0x000001D3 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m3D9631AF92186316351AFF095E5176423B84E25F (void);
// 0x000001D4 System.Boolean TMPro.Examples.WarpTextExample_<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m24ED53830B643858F6068C6908E7C0E044C671A4 (void);
// 0x000001D5 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07B1ABC445F7EADD1DB5F513857F1607C97AAC12 (void);
// 0x000001D6 System.Void TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4C7C46CC80C502CC1AD61D2F9DAF34F09226AF06 (void);
// 0x000001D7 System.Object TMPro.Examples.WarpTextExample_<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m86693688EEF5E754D44B448E82D87FAB40D6C8B8 (void);
static Il2CppMethodPointer s_methodPointers[471] = 
{
	Flythrough_Start_m82752FD2B23306BA215CA4C3A6EFC3041B9293B1,
	Flythrough_Update_m5A2404D7510FFF8CDF5FA8250C51F2D862CBC1FA,
	Flythrough__ctor_mE1C83D767FDD4C26DCE6363121B167D96B6BC3B6,
	SmoothMouseLook_Update_m942F000312ABCE8C1AE0E2D06E59013D22640FD5,
	SmoothMouseLook_Start_m065E245F8A4C432D4AE75AAC209EF5933071A7E2,
	SmoothMouseLook_ClampAngle_mA548EE0544AFE855A0F6C1F7E069124E5EB60DC2,
	SmoothMouseLook__ctor_m388B62BE3F96D1DDBD3570E4A89801707BF81864,
	WaterReflection_OnWillRenderObject_m6406F11E93B81C0D209F92F26C5671CD66604F38,
	WaterReflection_OnDisable_m25A3661620154BBC9D6984C5E13BCE1FC3A9C058,
	WaterReflection_UpdateCameraModes_mC381BBB35303EAF9E52A031D83B5E31AE5CCB093,
	WaterReflection_CreateMirrorObjects_m6BD5EC942B0A7CDF21AAF1ACE050C6ACB00C0A2A,
	WaterReflection_sgn_m12142C292809AD1E83D600D0C780DFBD28C0660A,
	WaterReflection_CameraSpacePlane_m7000CF7A513730DEFA658C0F0F39A734E9D87568,
	WaterReflection_CalculateObliqueMatrix_mFB2263A7FF75702EEE1842AACF2DB8CAD35AAD94,
	WaterReflection_CalculateReflectionMatrix_m0F610CCC02B71FE8288ED448D75A1E619BFDD910,
	WaterReflection__ctor_mB05CAAFECE3D78E4AA8BAA81DEC211928CC27614,
	WaterReflection__cctor_m479596F889067BA76E2BE4668594B9CB8BFBC922,
	ControlJugadorTwo_Update_m77B83D80F13BF2198DEBD115D9C5C25F427203B6,
	ControlJugadorTwo__ctor_mA378ECE6FC61759E5B3E574929381D826A977EB5,
	DieTwo_Start_mEBF85EA5B93EF1A9744B17525AA82EE58A090C18,
	DieTwo_Update_m778BBD5C0DCBF5ABE740D90EC70D1869AF52C58A,
	DieTwo_OnCollisionEnter_m79CE45EAA7D69537F9A41C07DC0DD5819D450243,
	DieTwo_RestaBarra_mA245EBE98F7372D06936E74C6CE4075AAD2EBC70,
	DieTwo_SumarBarra_m86B67EA4E96DE82E5EC005F12AAB8D4D74181796,
	DieTwo_Fin_m830C4672781083E2DD3E256BBC353CC64CF3336A,
	DieTwo__ctor_mA129B8FFC09C7429D85A3C16682DD847BCADEC08,
	DisparoLlama_Update_m9BE7738DFA1C01D59B9BA5708CCD75AD42392BE2,
	DisparoLlama_Disparo_m75058404AFBD701B96C8C03C935C1012A198A178,
	DisparoLlama_activarBala_m468374C3D942DD8D81E7FF8BF0FA4D0F38C43FED,
	DisparoLlama__ctor_m13646602533E552A3EABDDD3751FCD133C2F6F61,
	DisparoTwo_Update_m804DB0B4F43307A3BBC2E92A1565C8A04F914EFE,
	DisparoTwo__ctor_m540D423F19591540B0589FA11B6CA0101B01F421,
	EfectoExplodsion_OnCollisionEnter_m56DC71FF43CC5D6328FD4CC4EABE665F3C24B083,
	EfectoExplodsion_ActivarExplosion_mC2D467D867F2348C37CEED6A5EC552A217D0457C,
	EfectoExplodsion__ctor_m84F4A271D4933D4B899F404A0C8F76CCFC22C6EB,
	ArmasDestruccion_OnTriggerEnter_mDBCAE427D47E75BD889339379B153229666F72F6,
	ArmasDestruccion__ctor_m372F5925EB1BA22D72C0B9A4C0E79729806AA934,
	AddScore_Start_mE9F4778CE26303D9F46E08AEB38D057737870AB1,
	AddScore_OnTriggerEnter_mFBEF35CE4DCB482159B6E487631E894B966B8DCC,
	AddScore_Fin_m0A79A3D17CE63EF1692D689D7ADEF97C6C994D4E,
	AddScore__ctor_mBBC30EDEB19626466167EA38B8725A3502923DB5,
	Die_Start_m3C19E75EF15474563861D09559E101361ED62F2A,
	Die_Update_m07D3E26A666D0B3FA889CAF29754CF368754D907,
	Die_OnCollisionEnter_mA88DCF76EFFD12CDDED4DC873EB04AAD50FF9DEB,
	Die_RestaBarra_m5482CA17842841EBD6208D7F6ABD823B530C0701,
	Die_SumarBarra_m0AEDD947F6C40A2D36B9361AD8D209BD76C6AE5B,
	Die_Fin_mDB9F073F1D73AA86EEAF01E4A3367A069245146B,
	Die__ctor_mA9AD27E991249B54D4130682E91DF36A5343AC8F,
	HealtBar_SetMaxHealth_mB7CBF6B1C98EC5CDAD2D5DD920E4589B2E7EA08D,
	HealtBar_setHealth_mA275E7208468F5FC2E6E0CCDE76B067E6BAD43DB,
	HealtBar__ctor_mDEF43F6AC111E2DC4384A01AE84BEC5F0C38C510,
	Life_Start_m17F61456909AB7667D8E144181BA6511A8F5D878,
	Life_Update_m00D2119CADE3A4DF93FE10C8998A4CB2421A41C0,
	Life_OnTriggerEnter_mE1F7FD2FFD2AA37E2ABC67D390F366DF7A6DCB18,
	Life_SumarBarra_mFC5D71B5D564840DB626F3D1B8A1FEAAA0D6BA32,
	Life__ctor_m2531D353B237BD7938F28843C3EE82249D9DE44B,
	NpcDialogo_Start_m115904B68B46C7072D5ECE0DE51E59BB88669346,
	NpcDialogo_Update_mB0F372CEC790FDA24A18C1CA3504683194AFA795,
	NpcDialogo_OnTriggerEnter_m9FB976602502F9914771169B72D8B5B5006A796B,
	NpcDialogo_OnTriggerExit_m8F6EF3B7056C87150A0DF544D471C20B3D27B4D1,
	NpcDialogo_no_m5D8F3B62D615566CCE72879F80ACA3F593D12810,
	NpcDialogo_si_m8EDFF4D548AC2095288F710A0851A8D93F76109B,
	NpcDialogo_nso_mD21BA6CCECB62C69F52A52E7CE3BDA0B399893DE,
	NpcDialogo__ctor_m2ADB476FB82B090D1DE6E21E47A3C2BE0E84F8DE,
	BombaFinal_Start_m41E189A15C680C71CF227799D9095CB1E143A25F,
	BombaFinal_FixedUpdate_mF82EA7AFBE80B2CB6AF7C5B7ED5A394204564642,
	BombaFinal_Detonate_m000BAF097C601711A3DB8B6EDAA8CDBBDF2C1204,
	BombaFinal_Update_m5E1263B233E8F21BDA1072CB2CD797446C6C7E91,
	BombaFinal__ctor_mAEBC7F0CB68F2DD68046BD2873240FD3C56CA58D,
	CommonEnemy_Awake_mD0B9832CF819A2AE40CF01365A5C39CCB715EA79,
	CommonEnemy_Start_m889A4AC8A0A0972B2AB1327C596C2F1FAD48369B,
	CommonEnemy_Update_mF19C6F84A46BFDE942D9A1AB576E4EE1130CF1D1,
	CommonEnemy_CalculateEnemyPos_mE0065ED9B9929DD7A0D4D3A8E96355287E58D7F8,
	CommonEnemy_AutoLooker_mD4C377906B3E7435E5ADCED12B2BEBEE640909E5,
	CommonEnemy_Attack_m4F1186F47FCB511C6E0B79DF7F99B399FD29ABBA,
	CommonEnemy_DestroyEnemy_m6288CC0DC7BD3A7F9E99B49B0F871FEE665B6F9B,
	CommonEnemy_StopDeathAnim_m8873A4E592BDAA5E61DB5F829483CD39887A4DBA,
	CommonEnemy_OnDrawGizmos_mF08F0D62EC6F8CC6B8A3C65E3EDED3880FF07EBE,
	CommonEnemy_OnCollisionEnter_m91DC87D75F9DE095AFAB2A2E4E34EB068B7FE57B,
	CommonEnemy__ctor_mE71FC7E310EF0D5E68E2565D8C61012C3C22B835,
	Instanciador_Start_mFA9856B4C8534C7406C8E0B4F88C995397F377B7,
	Instanciador_Creando_m8766C769B17AFF13561CFEDB84776665785D08C7,
	Instanciador__ctor_m08B25BD813D0DA839FBE1275962205BBE9265779,
	Inventario_Start_mEE9ECEBD3EF5BF997E9C9ED7BCE8130916B9EA75,
	Inventario_Update_mD169FC60B57865A0D3DCB4C350052AAD60C7FFD7,
	Inventario_OnTriggerEnter_m3A27113EAA1EE79BA76DDA47B6BBA717F8144F4C,
	Inventario_AddItem_mB8BF5A99C0ED9903D95FADE1C713601B71FAC4E8,
	Inventario__ctor_m88E70CBCC688206E3125E38543B7A3CF011822BF,
	Items_Start_m3524E61A7AA5BCE3A949CAA8FAF959F658D103F3,
	Items_Update_m9DAD53A3712922D4443D16F8460FF6E40C8A3343,
	Items_itemsUsage_m0970B1FCE1B1263F908ACFD2A12CF8E276284030,
	Items__ctor_m0214104150353BC80C4A757CC60B221265C659F5,
	Slots_Start_m4023E79568C3D93961B7B9E26260F078057BCAF7,
	Slots_UpdateSlot_mA26D2459E35B0A831D3A76EBE2F375EF289AE1CB,
	Slots_UseItem_m646ADDD5AA16A2CD55862F9F998486D0705EFDA9,
	Slots_OnPointerClick_mD34D6294563E4594DF7C045BAC22AFC83CE61071,
	Slots__ctor_mED3A94C8025F274E4127A9E84DEE1F6F332D28EF,
	LanzaLLamas_Update_mB650DC3B5733BE91B5A499167D9589ABF229AE4B,
	LanzaLLamas_Disparo_mC4C29418800AF56C89C1FA45B27D2D70DFEE5C71,
	LanzaLLamas_activarBala_m312050E79E36A6057A2142BE89D6502A0A8D9660,
	LanzaLLamas__ctor_mF5867DB11307CB0EBA543F5C1E00B1BCDEC50E60,
	Menu_EscenaJuego_m7F1A874E228C024E8EC1CFA609A613BD5C760E80,
	Menu_CargarNivel_m146259E5E4923BB0CCD808DCC6CE42BF39939591,
	Menu__ctor_m87A18732D59A6382889C2771A372B622FD6CD58D,
	Quit_Exit_m3E0A2DA0D53956E42206BD46695EEF1E786E7478,
	Quit__ctor_m4C50BF52D3A6ADB583BE76FE1DEAFB1C889B3F6A,
	Plataformas_Start_m3668DA11CA2A142E458BCDF898115C48029F3DB5,
	Plataformas_Update_m610D3CC8D66D2758F95A63F23393B8DC8F6B304D,
	Plataformas_Creando_m988164AE31D37492A3F22B7A2D1A595AA235F4E6,
	Plataformas_Prendiendo_mF7BE88B1CA9FA1D2340B7A7DD1F6D636636B0CFC,
	Plataformas__ctor_mFE4879DC787FA04CA2BCE34BA7A36AF220F8EDF2,
	Armas_Start_m964F5855B67F68FB1C4F3C53067F013B49055E21,
	Armas_OnTriggerEnter_m6EBA7EC19C1BC452DFFCBC7F77D86CCE22EE2979,
	Armas_ArmaGenerador_mE3C3D906A047F63521F15C7354C6F6C419A79867,
	Armas__ctor_m7D7234CB5BBECB22C0FC3EB6FDA757178DF709C6,
	ControlCamara_Start_m3E41AB41B48C305B801CE88E1088E30F6B48065C,
	ControlCamara_LateUpdate_m01E53FBC184BAEAE7D3B2C0DC1059F7221B1AAF8,
	ControlCamara__ctor_m954699EF3390352A462FE175B54EEB6A85882E34,
	Disparo_Update_mEBE4F69B09F60EC8317B69CFF7A4020E273C3F25,
	Disparo__ctor_mDD5F227B13216300EC5BDEA53801E8517DA5B5A1,
	PlayerMove_Update_mDF42B2202BD4511D2B1C7075AEC64E69865E8DCF,
	PlayerMove__ctor_m85CA761CC1DED13FBB3373A6BAC8CF81B73B77B0,
	Rotacion_Update_mB9EC5458AE4CC7604FAC946B4508A7527F79841D,
	Rotacion__ctor_mDCFA94CF73551D88EDB0068FEB9619A0230FAE31,
	Timer_Update_m7320C1508ACA29DE9F062B558142ED29476B6569,
	Timer_Explotar_mEB54856F7E96640D7EFFBF5173BDE5852E3F387A,
	Timer__ctor_m970098BC1EE2B392F3744D1FC36524057EC6D98C,
	BulletMovement_Start_m23FD2976922A882D9131970DF55FBCAD774C7315,
	BulletMovement_Update_m27FED4D8AA57FFB33554678E07CCEB9A18F1C4FA,
	BulletMovement__ctor_mA3E6F314BBADDE1AE6FA3C469E0304476527EF48,
	DisparoTorreta_Start_m6CE03746997206BAFFA932750A0B83EDAF201B24,
	DisparoTorreta_Update_m1802EA6988DBA9D8FAB91ABC1DB00A2C75815BD8,
	DisparoTorreta_FireBullets_CR_mA9BC91E492005F30EC7CC2CB14754D1C17F8E10E,
	DisparoTorreta__ctor_mD59A51A720550076E409AF000551A9467AB507E7,
	TorretaRotacion_Start_mACC24A844B090172034034D2ADECBF823077F1C1,
	TorretaRotacion_Update_mD2C3B80E3E489400A932A7E9894BB8A4CE965146,
	TorretaRotacion__ctor_mD733F6BE1C824539F0056DE1E893EC26BC124D70,
	ChangeSizeColor_Update_m0901BB8A1B7D836A85CD137E47C2D2EF15867786,
	ChangeSizeColor_ChangeEffectColor_m7C3B80778A56E4C44965B7F2D719E6437801954F,
	ChangeSizeColor_CheckIsColorChange_m61B3CD982E27E7D219F762F6CE6E3D6BD6D4E305,
	ChangeSizeColor_CheckColorState_m984F694A17C11467C09D9092F32687804A611EFC,
	ChangeSizeColor_GetIntensityFactor_mCC94B810EF38A20696E5C1AF581EAAF654EC5890,
	ChangeSizeColor__ctor_m2CFB9C282816C5AA2D45DABE42BBC68FDF612F5E,
	DelayActive_Start_mFF905293823B3376CFBCB44C0621C959A5141CBE,
	DelayActive_Update_mFE7BA706A140EE1980E1E3BBCF718BC043D3C47B,
	DelayActive__ctor_m82F046FBDCE58164EB3BE835880B76A9D99D5B58,
	LookAtTarget_Update_m63019289ED3D063B9ADEADD26F281C7FEE4C03F6,
	LookAtTarget__ctor_mFA983B94A7327E3F43B57E14BDDFAC980F9CB222,
	MultipleObjectsMake_Start_mAB28B79703F2E67329851B9D791788302704D7AC,
	MultipleObjectsMake_Update_m689CBF8EE560FA3C5850C3CE9603C7BB54CF82B4,
	MultipleObjectsMake__ctor_m35A8C6C64D70AB9F89D465DB2593BD97158F4CAB,
	ObjectMove_Start_mBF8737576548780D2FF8BB82243A41BEB990426B,
	ObjectMove_LateUpdate_m22492C39D9C6B9002612834CB98F63C2F946B2EC,
	ObjectMove_HitObj_mB907DEC61F1557205F7CE73929F2E0E2681F2B3D,
	ObjectMove__ctor_m52FC29CB90E7342FDBC98CE7EBA128D3ADB92A3B,
	ObjectMoveDestroy_Start_mDDC7D2E20D361CA1773339732AED6644B839A4A8,
	ObjectMoveDestroy_LateUpdate_m74E45401C4D8AAD0E05C9DFB0B468ADACA75C718,
	ObjectMoveDestroy_MakeHitObject_mC18DF58BD37C6A6A25D8B6E12C9B8335001C46BD,
	ObjectMoveDestroy_MakeHitObject_mFD604A577E4604870300F6C70AF7F573FC71D8E4,
	ObjectMoveDestroy_HitObj_m4D3A52A89BAACD7CE3395E5F881DA44619AA24CE,
	ObjectMoveDestroy__ctor_mAF50C4DBE05F2AF186DAE277C6FEC4B602E6CFCA,
	ShieldActivate_Start_m6B80FA14330D48AB91F97C513F2781D3C28095BA,
	ShieldActivate_Update_m24286007D25F4F5A2A25674B48A02180096DBE1A,
	ShieldActivate_AddHitObject_mAD151869B78D2B16E9AB48A243BD240AC6929362,
	ShieldActivate_AddEmpty_mA6EF00D032666DD5F967A7EF062BA4DF83517653,
	ShieldActivate__ctor_mF706D22B19D913A86C48FDC557B37501EF24B811,
	ShieldActivate_U3CUpdateU3Eb__7_0_mDBCA072176F90CB5BAF86F65738C24284E16185D,
	_ObjectsMakeBase_GetRandomValue_m98D973ACA5E858C0F41BD634CCA95049D471B561,
	_ObjectsMakeBase_GetRandomValue2_m4014821A75453A8DF8C3DD1FA99C315A3FE402BA,
	_ObjectsMakeBase_GetRandomVector_m93FAD631EE4E7FE545C246A073F9C5F02F92E67A,
	_ObjectsMakeBase_GetRandomVector2_m09CDAE1E67A3DA1DF0E6EDAB9D44CD87435D5CB6,
	_ObjectsMakeBase__ctor_m17FAA8E820F557A1BCA28440DF093E52F11FDA97,
	NewMaterialChange_Awake_m9B4353F2A46071AF4AE40C0F8BF57088821AAB81,
	NewMaterialChange_LateUpdate_mCDD03615584DE297165D09C5B6BF735597797BA1,
	NewMaterialChange__ctor_m399E833138B242F309CA366B367518B1F22888FA,
	ScaleFactorApplyToMaterial_Awake_mE2778999EDDAAA330AA2975CE41801132D910F56,
	ScaleFactorApplyToMaterial_Update_m4E3F4F9B68CCCFCC675B9571E9E2142D96B0DBF0,
	ScaleFactorApplyToMaterial__ctor_m12843F1E978FA2EFCDE5C9854B48129384D3AC0D,
	VariousEffectsScene_Awake_m318B76C6956D16B155401A075D05EF35CBA10498,
	VariousEffectsScene_Update_mC974A0BB2B415EC9EF1D5EBDFC7946EA29D0CD73,
	VariousEffectsScene_InputKey_mE1C4392BDE0EABDEC09301C27500247E224C94B8,
	VariousEffectsScene_MakeObject_m86DC07D7C18B90F16884AB56A44683993DD22083,
	VariousEffectsScene_DestroyGameObject_m372877CF191E21CE8351EA263F5B4787E6F70E9D,
	VariousEffectsScene_GetSizeFactor_m737C9C3DBEC3E85D6BA730A3CAF11C8552EAE05D,
	VariousEffectsScene__ctor_mD623B9C19F20F89BDE364CE6C6F598A7D20A9F74,
	VariousEffectsScene__cctor_m85504A94406A210C80B8F8D01E3EB6131F7EBFAF,
	VariousMouseOrbit_Start_m2D5B0F4C454BB46BBD7E9C2C6E7D7E462C2BF9A7,
	VariousMouseOrbit_LateUpdate_m1DBD26830404891D0E5228BF9B03ECDA50D101FD,
	VariousMouseOrbit_ClampAngle_m99DC88A57D9A93DF33D1DB1680F6115AA064D6EC,
	VariousMouseOrbit__ctor_m3DEC0502D5B32AC2EC856D3513FFD0B252CA599D,
	VariousRotateObject_Awake_mBE29D4FCF2DD907029A62C6CC748ED24804BC8A9,
	VariousRotateObject_Update_m821CDE9BED198A1FA3737E596FFA73D0B2B3D222,
	VariousRotateObject__ctor_m0DA7EF4B9DA6811B6D63329E09F62182802298A0,
	VariousTranslateMove_Start_m44B70B62B1F4050DDC89770849AC30E3CBB7CA20,
	VariousTranslateMove_Update_mED87581A9680380C50660AA2797544280BD1A213,
	VariousTranslateMove__ctor_mBD6FA24AE7CA3B238B7DEDE78527D2F53F77644A,
	ChatController_OnEnable_mD13A02B63932BDA275E1A788FD72D86D69B9E440,
	ChatController_OnDisable_mD31D1ED1B2C3C82986BFBD18FA584D45167431F3,
	ChatController_AddToChatOutput_m01CC3E959ACECC222DFD8541231EC1E00C024194,
	ChatController__ctor_mF4343BA56301C6825EB0A71EBF9600525B437BCD,
	DropdownSample_OnButtonClick_m31A59FA78B78A6457BB20EC10C08C82E4C669140,
	DropdownSample__ctor_mA06B4508E4574A97B9B0A20B0273557F5AD068D7,
	EnvMapAnimator_Awake_mAB3C67FA11192EFB31545F42D3D8AAB2A662FEB2,
	EnvMapAnimator_Start_m21098EFD0904DFD43A3CB2FF536E83F1593C2412,
	EnvMapAnimator__ctor_mBA9215F94AB29FBA4D3335AEA6FAB50567509E74,
	TMP_DigitValidator_Validate_mD139A23C440A026E47FA8E3AD01AD6FEF7713C3D,
	TMP_DigitValidator__ctor_mF6477F5EB75EC15CD6B81ACD85271F854BABC5D6,
	TMP_PhoneNumberValidator_Validate_mCA5EA200223A9F224F2F4DBD306DAE038C71A35F,
	TMP_PhoneNumberValidator__ctor_mBB38130850945A40631821275F07C19720E0C55E,
	TMP_TextEventHandler_get_onCharacterSelection_mF70DBE3FF43B3D6E64053D37A2FADF802533E1FF,
	TMP_TextEventHandler_set_onCharacterSelection_m237C99FE66E4E16518DAE68FF9CBF1A52E816AD2,
	TMP_TextEventHandler_get_onSpriteSelection_m395603314F8CD073897DCAB5513270C6ADD94BF4,
	TMP_TextEventHandler_set_onSpriteSelection_mAAE4B440E34EE5736D43D6A8A7D3A7CEE0503D69,
	TMP_TextEventHandler_get_onWordSelection_m415F4479934B1739658356B47DF4C2E90496AE2E,
	TMP_TextEventHandler_set_onWordSelection_m6062C0AF2FDD8752DC4A75663EE8E5C128504698,
	TMP_TextEventHandler_get_onLineSelection_m8E724700CC5DF1197B103F87156576A52F62AB2B,
	TMP_TextEventHandler_set_onLineSelection_m1A8E37D2069EF684EF930D4F1ABE764AE17D9A62,
	TMP_TextEventHandler_get_onLinkSelection_m221527467F0606DD3561E0FB0D7678AA8329AD5D,
	TMP_TextEventHandler_set_onLinkSelection_m1376CC9B70177B0C25ACEDF91D5B94BC4B8CF71D,
	TMP_TextEventHandler_Awake_m9A353CC9705A9E824A60C3D2D026A7FD96B41D74,
	TMP_TextEventHandler_LateUpdate_m2F3241223A91F9C50E11B27F67BA2B6D19328B72,
	TMP_TextEventHandler_OnPointerEnter_m1827A9D3F08839023DE71352202FE5F744E150EF,
	TMP_TextEventHandler_OnPointerExit_m788B93D2C3B54BCF09475675B274BCB047D449FB,
	TMP_TextEventHandler_SendOnCharacterSelection_mBC44C107A6FB8C43F7C6629D4A15CA85471A28B2,
	TMP_TextEventHandler_SendOnSpriteSelection_mEF24BCE06B0CE4450B6AE9561EC4B5052DAF00F6,
	TMP_TextEventHandler_SendOnWordSelection_m7C4D266070EE2ADC66BCCFD50EB74FEB4923B77E,
	TMP_TextEventHandler_SendOnLineSelection_mAAF4AF44929D0C9FD73C89E5266028908074AEB1,
	TMP_TextEventHandler_SendOnLinkSelection_m082D12F7D044456D8514E4D31944C6900F8262C0,
	TMP_TextEventHandler__ctor_mEA56AE9489B50CF5E5FC682AA18D1CE9AF8E1F8B,
	Benchmark01_Start_mC0055F208B85783F0B3DB942137439C897552571,
	Benchmark01__ctor_m2FA501D2C572C46A61635E0A3E2FF45EC3A3749C,
	Benchmark01_UGUI_Start_m976ED5172DEAFC628DE7C4C51DF25B1373C7846A,
	Benchmark01_UGUI__ctor_mD92CA5A254960EB149966C2A3243514596C96EAD,
	Benchmark02_Start_m2D028BFC6EFB4C84C1A7A98B87A509B27E75BA06,
	Benchmark02__ctor_m7CA524C53D9E88510EE7987E680F49E8353E4B64,
	Benchmark03_Awake_m8D1A987C39FD4756642011D01F35BDC3B1F99403,
	Benchmark03_Start_m73F65BA012D86A6BE17E82012AE8E2339CA5D550,
	Benchmark03__ctor_m9A5E67EA64AAC56D56C7D269CC9685E78276360A,
	Benchmark04_Start_m22D98FCFC356D5CD7F401DE7EDCDAF7AE0219402,
	Benchmark04__ctor_mDAC3E3BE80C9236562EFB6E74DEBE67D8713101D,
	CameraController_Awake_m581B79998DB6946746CBF7380AFCC7F2B75D99F7,
	CameraController_Start_mA9D72DB0BB6E4F72192DA91BC9F8918A9C61B676,
	CameraController_LateUpdate_mDC862C8119AB0B4807245CC3482F027842EAB425,
	CameraController_GetPlayerInput_m198C209AC84EF7A2437ADB2B67F6B78D12AB9216,
	CameraController__ctor_m2108A608ABD8EA7FD2B47EE40C07F4117BB7607E,
	ObjectSpin_Awake_mDA26D26457D277CC2D9042F3BD623D48849440C4,
	ObjectSpin_Update_mC50AAC1AF75B07CD6753EA3224C369E43001791B,
	ObjectSpin__ctor_m2832B9D713355ECF861642D115F86AA64A6F119E,
	ShaderPropAnimator_Awake_m8E01638EBFE80CC0B9E4A97AB809B91E3C6956BE,
	ShaderPropAnimator_Start_m24F4FADC328B0C76264DE24663CFA914EA94D1FD,
	ShaderPropAnimator_AnimateProperties_mC45F318132D23804CBF73EA2445EF7589C2333E9,
	ShaderPropAnimator__ctor_mC3894CE97A12F50FB225CA8F6F05A7B3CA4B0623,
	SimpleScript_Start_m22A3AE8E48128DF849EE2957F4EF881A433CA8CB,
	SimpleScript_Update_m742F828A2245E8CC29BC045A999C5E527931DFF1,
	SimpleScript__ctor_mA2284F621031B4D494AC06B687AF43D2D1D89BD7,
	SkewTextExample_Awake_m51C217E0CB26C2E627BA01599147F69B893EF189,
	SkewTextExample_Start_m6A9CEFA12DB252E297E41E256698DD4E90809F6A,
	SkewTextExample_CopyAnimationCurve_m3CE7B666BEF4CFFE9EB110C8D57D9A5F6385720B,
	SkewTextExample_WarpText_m4A69C47EA665D49482B930F924E49C8E70FAC225,
	SkewTextExample__ctor_m11DC90EB1A059F4201457E33C4422A7BDA90F099,
	TMP_ExampleScript_01_Awake_m9CE8A9F929B99B2318A6F8598EE20E1D4E842ECD,
	TMP_ExampleScript_01_Update_m8F48CBCC48D4CD26F731BA82ECBAC9DC0392AE0D,
	TMP_ExampleScript_01__ctor_m9F5CE74EDA110F7539B4081CF3EE6B9FCF40D4A7,
	TMP_FrameRateCounter_Awake_m906CC32CE5FE551DF29928581FFF7DE589C501F2,
	TMP_FrameRateCounter_Start_m614FA9DE53ECB1CF4C6AF6BBC58CE35CA904EB32,
	TMP_FrameRateCounter_Update_m16AB65EF6AB38F237F5A6D2D412AB7E5BF7B1349,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m537A709F25C3AA752437A025BEE741BD2F71320E,
	TMP_FrameRateCounter__ctor_mD86AC3A8D918D14200BF80A354E0E43DC5A565A2,
	TMP_TextEventCheck_OnEnable_m22D9B03F3E1269B8B104E76DA083ED105029258A,
	TMP_TextEventCheck_OnDisable_m42813B343A1FDD155C6BFBFCB514E084FB528DA0,
	TMP_TextEventCheck_OnCharacterSelection_mC6992B7B1B6A441DEC5315185E3CE022BB567D61,
	TMP_TextEventCheck_OnSpriteSelection_mEC541297C2228C26AB54F825705F0476D45F877A,
	TMP_TextEventCheck_OnWordSelection_mA1170F805C77CC89B818D8FBEE533846AF66509C,
	TMP_TextEventCheck_OnLineSelection_mB871339347DCB016E019F509A00BDE9A58105822,
	TMP_TextEventCheck_OnLinkSelection_m44A79DDBDF03F254BAFB97BE3E42845B769136C5,
	TMP_TextEventCheck__ctor_mA67343988C9E9B71C981A9FFAD620C4A9A6AA267,
	TMP_TextInfoDebugTool__ctor_m1EA6A5E31F88A1C7E20167A3BCCE427E9E828116,
	TMP_TextSelector_A_Awake_m82972EF3AF67EAAFD94A5EE3EA852CE15BE37FC1,
	TMP_TextSelector_A_LateUpdate_m40594D716F53E6E5BC0ECD2FFE8ECA44FAA5C8E4,
	TMP_TextSelector_A_OnPointerEnter_m8462C2DC4F71BDE295BE446B213B73F78442E264,
	TMP_TextSelector_A_OnPointerExit_m3AC7467ECE689A58590DA325F8B300B08C1E1B5D,
	TMP_TextSelector_A__ctor_m081D44F31AA16E345F914869A07BD47D118707DF,
	TMP_TextSelector_B_Awake_m217B6E2FC4029A304908EE9DC1E4AA2885CBF8A3,
	TMP_TextSelector_B_OnEnable_m24A7CCA0D93F17AC1A12A340277C706B5C2F9BAB,
	TMP_TextSelector_B_OnDisable_m6088452529A70A6684BD8936872B71451779A2F4,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m79EEE4DF7792F553F5DEDCF0094DAC6F2A58137A,
	TMP_TextSelector_B_LateUpdate_m745577078D86EF6C23B914BD03EA1A1D169B9B7B,
	TMP_TextSelector_B_OnPointerEnter_mF6C09A2C64F5D2619014ADD50039358FAD24DB3E,
	TMP_TextSelector_B_OnPointerExit_mB0AAA8D034FC575EB3BCF7B0D4514BD110178AD3,
	TMP_TextSelector_B_OnPointerClick_m13B20506F762769F099DE10B3CCA2DF194192B42,
	TMP_TextSelector_B_OnPointerUp_mD53FD60E0C5930231FB16BDEA37165FF46D85F6E,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m1D03D0E14D6054D292334C19030256B666ACDA0E,
	TMP_TextSelector_B__ctor_m494C501CF565B1ED7C8CB2951EB6FB4F8505637F,
	TMP_UiFrameRateCounter_Awake_m255A7821E5BA4A7A75B9276E07BC9EA7331B5AA6,
	TMP_UiFrameRateCounter_Start_mA4A02EB5C853A44F251F43F0AD5967AE914E2B0F,
	TMP_UiFrameRateCounter_Update_m04555F8DF147C553FA2D59E33E744901D811B615,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_mDF0FDFBCD11955E0E1D1C9E961B6AD0690C669ED,
	TMP_UiFrameRateCounter__ctor_m99ACA1D2410917C5837321FC5AC84EAED676D4CC,
	TMPro_InstructionOverlay_Awake_m639E300B56757BDB94766447365E1C94B5B83ACD,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m74B8F0AE15DA6C9968C482981EBCF5CC9DB1F43D,
	TMPro_InstructionOverlay__ctor_m570C4B4CB3126622D6DFF71158336313C45C717A,
	TeleType_Awake_m5F758974DA88ED8187E71A5100D2D9E47985E359,
	TeleType_Start_mC32B726B6202883E12E1A62A52E50092E7E9D9F0,
	TeleType__ctor_m6CDFDC88D47FE66021C133974C8CB0E16B08A00E,
	TextConsoleSimulator_Awake_m4F61F06DFE11CFAF9B064CCA5B2D6423D5CFC302,
	TextConsoleSimulator_Start_m572903C9070A8AD276D2CB14DF7659AE551C75B3,
	TextConsoleSimulator_OnEnable_m1A36B043E4EDD945C93DFC49F7FAFB7034728593,
	TextConsoleSimulator_OnDisable_m6F9BE1975CB15EE559D3B617E8972C8812B41325,
	TextConsoleSimulator_ON_TEXT_CHANGED_m73C6B3DAA27778B666B9B3B75C9D4641FC1BEC8A,
	TextConsoleSimulator_RevealCharacters_mC24F1D67B99F0AFE7535143BB60C530C8E8735F0,
	TextConsoleSimulator_RevealWords_m68CAA9BDC1DF3454ECB7C5496A5A2020F84027B8,
	TextConsoleSimulator__ctor_m4DB9B9E3836D192AA7F42B7EBDC31883E39610E9,
	TextMeshProFloatingText_Awake_mD99CC6A70945373DA699327F5A0D0C4516A7D02A,
	TextMeshProFloatingText_Start_m3286BB7639F6042AD4437D41BF4633326C4290BE,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m61ABFBAA95ED83A248FBCC3F5823907824434D34,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m65A61DF0BA640F2787DD87E93B8FD9DEC14AACB4,
	TextMeshProFloatingText__ctor_m11DBC535BA8001B48A06F61515C0787C3A86611F,
	TextMeshSpawner_Awake_mB3C405B4856E9B437E13E4BD85DAE73FFF1F6561,
	TextMeshSpawner_Start_m436D4CD2A82C0F95B8D942DF4CCFCE09792EDF0F,
	TextMeshSpawner__ctor_mDB693DABF1C3BA92B7A3A4E1460F28E3FAFB444D,
	VertexColorCycler_Awake_mAEEAA831C084B447DFD0C91A5FA606CB26D3E22A,
	VertexColorCycler_Start_mF01B64B3E5FE5B648DE2EED031962D9183C2D238,
	VertexColorCycler_AnimateVertexColors_m9CDB87631C324FB89FA7D52F5BC910146F3DDEB7,
	VertexColorCycler__ctor_m9B68D69B87E07DC0154344E5276EFC5B11205718,
	VertexJitter_Awake_m9699A62E72D3A262EDFDF7AC63ABD33E53F179B6,
	VertexJitter_OnEnable_m1B592E7AC81C7F17D0A59325EBDE71E067178E8A,
	VertexJitter_OnDisable_m5567B541D1602AD54B0F437D4710BCD1951FE2C5,
	VertexJitter_Start_mB698E212B8C3B7C66C71C88F4761A4F33FCE4683,
	VertexJitter_ON_TEXT_CHANGED_m09CFD4E872042E7377BEC8B95D34F22F62ABC45B,
	VertexJitter_AnimateVertexColors_m1CCF60CA14B2FF530950D366FF078281ADC48FF4,
	VertexJitter__ctor_mC19C148659C8C97357DB56F36E14914133DA93CF,
	VertexShakeA_Awake_mD2ABEB338822E0DBCFDEC1DB46EC20BB2C44A8C2,
	VertexShakeA_OnEnable_m60947EACA10408B6EAD2EC7AC77B4E54E2666DD8,
	VertexShakeA_OnDisable_mF5AF9069E523C0DF610EF3BE2017E73A4609E3CC,
	VertexShakeA_Start_m96D17C13A279F9B442588F6448672BCF07E4A409,
	VertexShakeA_ON_TEXT_CHANGED_mE761EE13D2F9573841EFA5DEE82E545545280BFA,
	VertexShakeA_AnimateVertexColors_mC97FED540BDE59254AB30C5E6BCF1F53B766945F,
	VertexShakeA__ctor_mD84B9A0167705D5D3C8CA024D479DC7B5362E67B,
	VertexShakeB_Awake_mA59F3CA197B3A474F4D795E2B3F182179FF633CF,
	VertexShakeB_OnEnable_m8D4DD5CA81E5B0C02937D43C1782533D54F34B3F,
	VertexShakeB_OnDisable_m4473873008D4A843A26F0D2353FC36ABE74CEED2,
	VertexShakeB_Start_m24BEF670ABD1CCC864FAFE04750FEB83D916D0DF,
	VertexShakeB_ON_TEXT_CHANGED_mCC864EE9569AF68F53F6EAEB2CE8077CFEAF9E53,
	VertexShakeB_AnimateVertexColors_mAF3B23F4DD98AC3E641700B994B2994C14F0E12C,
	VertexShakeB__ctor_mDCAEC737A20F161914DD12A2FCBAB6AB7C64FEF2,
	VertexZoom_Awake_m5F98434E568929859E250743BA214C0782D17F2B,
	VertexZoom_OnEnable_m01E35B4259BA0B13EC5DA18A11535C2EC6344123,
	VertexZoom_OnDisable_mACD450919605863EC4C36DA360898BAEBBF3DDDB,
	VertexZoom_Start_m6910FF4AC88466E3B7DB867AD553429F1745320D,
	VertexZoom_ON_TEXT_CHANGED_mBFB58FE53145750AD747B38D9D1E7E00F8DBF9A0,
	VertexZoom_AnimateVertexColors_m328088EC0F893B3E60BB072F77ADF939B8566E4D,
	VertexZoom__ctor_m67833553C2739616BF059C661F92A032885510B2,
	WarpTextExample_Awake_m9643904751E2DF0A7DF536847AEA1A2B0774DD20,
	WarpTextExample_Start_mDF931C271901519BF21FD356F706FA8CDE236406,
	WarpTextExample_CopyAnimationCurve_m2C738EA265E2B35868110EE1D8FCBD4F1D61C038,
	WarpTextExample_WarpText_mAAAB1687869E0121C858C65BDB2D294D55CFB67A,
	WarpTextExample__ctor_mC3EAA1AE81FB3DA4B25F20E557EC6627A06DCB31,
	FoliageInteractor_Update_mF6E7F7F992819E4615446327CD697C28A2D6E3B2,
	FoliageInteractor__ctor_m24C8183DB53E344FB64BB71039E38B13F5B58735,
	U3CFireBullets_CRU3Ed__6__ctor_m3EC7F4118AE65B212E78016BFA1E3293B17377B6,
	U3CFireBullets_CRU3Ed__6_System_IDisposable_Dispose_m9DE3CBE6806B7047A9EC62519D0207E5212B762F,
	U3CFireBullets_CRU3Ed__6_MoveNext_mCA115BF9D601591260A784C8CDE0D9B54ACC7DA6,
	U3CFireBullets_CRU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m51859FA611B3CBD28C1FE935DED541EBCDD3B53B,
	U3CFireBullets_CRU3Ed__6_System_Collections_IEnumerator_Reset_mEB5C75A303835CF75F22E1A90B9ECE2501062EF8,
	U3CFireBullets_CRU3Ed__6_System_Collections_IEnumerator_get_Current_m7E7F110997CE8F7751C6BA699DA36FCE192D1B08,
	U3CU3Ec__cctor_m988B3726B871813020E7C2EDDAB8CEB2E67DEEEA,
	U3CU3Ec__ctor_m3653D140BE00AE006E397893DEF7E9E03DDB1EC7,
	U3CU3Ec_U3CUpdateU3Eb__7_1_mEB85FA29526D4B119D5BF22ED0177A534728049A,
	U3CStartU3Ed__4__ctor_m004DF17C34E6A9C76325BD4C65E0F328AD4B37E0,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mE7E20C5789828A8FDE263BD5ACC2D02982334B46,
	U3CStartU3Ed__4_MoveNext_mB3F55ABCABBA6717942BDC85935FDE439D09E226,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF976D094846BDC403335C76F41CAC26D53C8F1D2,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m9368B3951732B0D18993DA1B889346A775F252CE,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m495592283D84E6B446F7B9B7405F250D9687DEFB,
	CharacterSelectionEvent__ctor_m036DA7F340B0839696EB50045AB186BD1046BE85,
	SpriteSelectionEvent__ctor_m0BC042938C4EBBB77FFAD68C1ACD74FC1C3C1052,
	WordSelectionEvent__ctor_m1C01733FD9860337084DFE63607ECE0EF8A450EA,
	LineSelectionEvent__ctor_m1C3A0C84C5C0FEA6C33FA9ED99756A85363C9EF2,
	LinkSelectionEvent__ctor_mC7034F51586C51D1DE381F6222816DC1542AFF3A,
	U3CStartU3Ed__10__ctor_m139DF863E59AD287A6C14228BB59D56E7FD2E578,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mBBFAE2F68813477259A0B665B4E81833C03C746B,
	U3CStartU3Ed__10_MoveNext_m8D27A150B4BC045DD5A1ACB2FABBB7F7F318A015,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C55687F407BA372889D6A533FB817E1EA81C165,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mE725DFFE744FAEDABF70579D04BBEB17A6CFF692,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD8EEDE3D2E1E9838472D20AE93DD750D1EE39AF8,
	U3CStartU3Ed__10__ctor_mECD4DB9695B4D04CEF08DF193DAFA21412DA40EF,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m6773C67C5C6E7E52F8C8545181173A58A6B7D939,
	U3CStartU3Ed__10_MoveNext_mB823F98B1B0907B6959EAEE4D1EBE0AA35795EA3,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF100CFC13DED969B3BBE2007B3F863DCE919D978,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_mDE1D3A30ECA00E3CA2218A582F2C87EEE082E2DB,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m13710692FCBB7BB736B1F6446778124064802C83,
	U3CAnimatePropertiesU3Ed__6__ctor_mB4DA3EEEFC5ECB8376EF29EAC034162B575961B8,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_mC6A68A27A902E234429E3706D6DB432BEA04A384,
	U3CAnimatePropertiesU3Ed__6_MoveNext_mDE142F6B3AAB5916834A6820A5E7B13A7B71E822,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F4D59DDC372B977CD7297AA5F0C83D4FFBBA830,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mAC1E960F7FFCF032EE668635835954377D1469F6,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m96B6B8A0D715268DB789F3D4C60FC6DEC9E1F6E6,
	U3CWarpTextU3Ed__7__ctor_m07871FDF578BC130082658B43FB4322C15F0909E,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m3C8BC1501397E256464ADD27A32486CDE63C2BE2,
	U3CWarpTextU3Ed__7_MoveNext_m11D4701B069FCFC106319CBDCA56244EFA4C795F,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m995FCE4A3B9B270605168D01EF7439A437864C06,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m9970CA113E1A293472987C004B42771993CAA05C,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m398E9D0D09F26D5C3268EB41936ED92240474910,
	U3CStartU3Ed__4__ctor_mFB2D6F55665AAA83D38C58F58FA9DD0F5CE51351,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m72D91E3700B8E1F2053A7620793F97E01C1A2E3A,
	U3CStartU3Ed__4_MoveNext_m2839A135AFB4518430283897981AF4C91ABDBC6A,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB3E80ED07333F946FC221C16A77997DF51A80F87,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mAC8E620F4FCE24A409840017FB003AA1048497EF,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mD6DFD0886CCA250CB95B2828DEEEEE5EA6DC303A,
	U3CRevealCharactersU3Ed__7__ctor_mAF579198F26F3FD002CB7F4919CCB513E2B770E1,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m2B3AB12689498DE28F90CF5DA3D40AA6C31B928E,
	U3CRevealCharactersU3Ed__7_MoveNext_m16C2594A51B9D102B30646C47111F3476FFBF311,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8B5325041D4E4A3F3D5575373F25A93752D9E514,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m321E1A1FD7DE7840F8433BF739D3E889FB3E9C7C,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m9CF6271A74900FFA5BD4027E84D63C164C1650BC,
	U3CRevealWordsU3Ed__8__ctor_m5D2D48675C51D6CBD649C1AAD44A80CCA291F310,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m9F97B9D63E30AF41B31E44C66F31FE6B22DDFD4C,
	U3CRevealWordsU3Ed__8_MoveNext_m19EB0E15617A1893EEF1916629334CED1D8E9EA1,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m98E45A0AFB76FA46A5DA4E92E3FE114E310D8643,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m09CF6739322B1DB072CB8CFE591DBBC046008E63,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_m2083C1001867012CC47AFA8158F00ED15527C603,
	U3CDisplayTextMeshProFloatingTextU3Ed__12__ctor_m7E5E121E510EFDCCF0D565EBBF607F80836EBB66,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_IDisposable_Dispose_mD0C99DF97552E843D1E86CF689372B3759134BB7,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_MoveNext_mA0FD89A2C5D0867977DAB0896DAB041EDFA64200,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m735F43BD7983BCC8E417573886ADC91CDED8F29B,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_Reset_mBE24EA2EDAFC5FD22D633595174B122D5F84BB02,
	U3CDisplayTextMeshProFloatingTextU3Ed__12_System_Collections_IEnumerator_get_Current_m4199EEF69BC21DC6DDC2A0D0323AB70508CB1194,
	U3CDisplayTextMeshFloatingTextU3Ed__13__ctor_m664F9327A457FB4D53F20172479BB74A4B50675A,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_IDisposable_Dispose_mA301D3B10770F41C50E25B8785CE78B373BBCE7C,
	U3CDisplayTextMeshFloatingTextU3Ed__13_MoveNext_m96E4CEDBAD5AA7AC7C96A481502B002BC711725F,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7609D75AFDAE758DAAE60202465AB3733475352B,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_Reset_m6F0C933685FE89DF2DECC34EDD2FE5AD10542579,
	U3CDisplayTextMeshFloatingTextU3Ed__13_System_Collections_IEnumerator_get_Current_m29F3BB8ADAADB8FD750636C535D2B50292E2DE3B,
	U3CAnimateVertexColorsU3Ed__3__ctor_mD05EA47C6D3F9DC7BE5B4F687A62244E48BC3808,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5E0C78AE94BC2C9BD7818EF0DD43D46ABF8C0172,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m8ABBF64D39EB5C5AC87AE0D833B6CBE570444347,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m328E88E5192D85CDD2157E26CD15CC0B21149AB6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_mB49B7C6E42E5B1F488ECA85B1B79AC1D6BBC3022,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m6966F60B8B8F4540B175D0C145D8A73C89CE5429,
	U3CAnimateVertexColorsU3Ed__11__ctor_m14F85BFAE5EFFAF0BBD6519F6A65B1EE36FC5F0F,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m36A05E300F1E06AE13E2BFF19C18A59F3E73424A,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m95629D41B32EBFFE935AA88081C00EB958D17F53,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m994AE653E10219CEB601B88CC22E332FF8AD1F36,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mE8D9B5795F095798688B43CF40C04C3FE3F01841,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m12F2FCDBABCD9F41374064CAD53515D2D039EFD0,
	U3CAnimateVertexColorsU3Ed__11__ctor_m93C153A3293F3E7F9AB47C712F5D8BC9CB0B179D,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m6E2E964131F208551826E353B282FBB9477CB002,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mD5F89478A4FDEA52E1261CCCF30CC88B0D693407,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6A7BC75B026CFA96FA1C123E6F7E5A5AAFC46E9,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m28679C8DDFAAD809DD115DB68829543159F47FBD,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m85E44906BDBB53959D8A47AE74B74420179AD3EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m9B592C7897687114428DF0983D5CE52A21051818,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m24A98FB24DAE578F96B90B3A6D61D18400A731B1,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m52F01BB7305705B9DE4309E1A05E2725BA06672E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB05E3BDEFD461F6516F45D404DBDEC452C646D37,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m72086FED04B8AC40B92836B894D6807AFB51BB38,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mA1E44018FA1464D953241F5FA414715C9ADE98CF,
	U3CU3Ec__DisplayClass10_0__ctor_mF154C9990B4AF8DA353F6A8C115C96FB7CB76410,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mDEE1E28BD53D02CB2E40D0C263BBA65C0B5AC66C,
	U3CAnimateVertexColorsU3Ed__10__ctor_m92C755B17AC00199A759541B62E354765068E7F1,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mBA95580B1808E4EB047CD7F082595E1C5EE930C2,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mA307F1943028D7555032189B48F6289E09D2E220,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5288CCD0BF91027A0CE3AE12D301F3E5189F0DCE,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m7C214643A52D954B2755D3477C3D0BFC85DAFF8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m666CA82A5C974B5AC09F277AFC97A82D1D1C365E,
	U3CWarpTextU3Ed__8__ctor_m63F411BEA2E513D84AAA701A1EDF0D0322FDE9C4,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m3D9631AF92186316351AFF095E5176423B84E25F,
	U3CWarpTextU3Ed__8_MoveNext_m24ED53830B643858F6068C6908E7C0E044C671A4,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07B1ABC445F7EADD1DB5F513857F1607C97AAC12,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4C7C46CC80C502CC1AD61D2F9DAF34F09226AF06,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m86693688EEF5E754D44B448E82D87FAB40D6C8B8,
};
static const int32_t s_InvokerIndices[471] = 
{
	23,
	23,
	23,
	23,
	23,
	1282,
	23,
	23,
	23,
	27,
	590,
	393,
	2258,
	2259,
	2259,
	23,
	3,
	23,
	23,
	23,
	23,
	26,
	32,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	32,
	32,
	23,
	23,
	32,
	32,
	23,
	23,
	23,
	26,
	32,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	2260,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	296,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2261,
	23,
	23,
	23,
	2261,
	26,
	2261,
	23,
	23,
	23,
	1136,
	23,
	23,
	2262,
	1104,
	1104,
	1119,
	1119,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	2263,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	14,
	23,
	1989,
	23,
	1989,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	26,
	455,
	455,
	35,
	35,
	608,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	23,
	455,
	455,
	35,
	35,
	608,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	23,
	23,
	23,
	32,
	23,
	23,
	32,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	28,
	28,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	14,
	23,
	23,
	23,
	28,
	14,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	3,
	23,
	1301,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	56,
	32,
	23,
	114,
	14,
	23,
	14,
	32,
	23,
	114,
	14,
	23,
	14,
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	471,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
