﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// CodeMonkey.HealthSystemCM.Demo
struct Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6;
// CodeMonkey.HealthSystemCM.Demo_Bullet
struct Demo_Bullet_t0CC46797722BD7E2889978D6B16C4B47FB3686F8;
// CodeMonkey.HealthSystemCM.Demo_Cannon
struct Demo_Cannon_t456090E165C54F555F95058559544CD25714162A;
// CodeMonkey.HealthSystemCM.Demo_DamageButton
struct Demo_DamageButton_tA3803267184A803054306D09C2293BFC17FD719E;
// CodeMonkey.HealthSystemCM.Demo_DamageButton/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6;
// CodeMonkey.HealthSystemCM.Demo_DefenseGame
struct Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B;
// CodeMonkey.HealthSystemCM.Demo_HealButton
struct Demo_HealButton_tFA151641791C07388AA4AAEAC99DAA88D127D779;
// CodeMonkey.HealthSystemCM.Demo_HealButton/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D;
// CodeMonkey.HealthSystemCM.Demo_Unit
struct Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02;
// CodeMonkey.HealthSystemCM.Demo_UnitCSharp
struct Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C;
// CodeMonkey.HealthSystemCM.Demo_Zombie
struct Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91;
// CodeMonkey.HealthSystemCM.HealthBarUI
struct HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4;
// CodeMonkey.HealthSystemCM.HealthSystem
struct HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3;
// CodeMonkey.HealthSystemCM.HealthSystemComponent
struct HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6;
// CodeMonkey.HealthSystemCM.IGetHealthSystem
struct IGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1;
// CodeMonkey.HealthSystemCM.LookAtCamera
struct LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0;
// CodeMonkey.HealthSystemCM.Readme_HealthSystem
struct Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C;
// CodeMonkey.HealthSystemCM.Readme_HealthSystem/Section
struct Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89;
// CodeMonkey.HealthSystemCM.Readme_HealthSystem/Section[]
struct SectionU5BU5D_tF83A823B01B1C60F29C7AE56EDC08A5C0BFCD715;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventArgs
struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E;
// System.EventHandler
struct EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72;
// UnityEngine.Collider2D
struct Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F;
// UnityEngine.Events.UnityAction
struct UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Mesh
struct Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE;
// UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734;
// UnityEngine.Sprite
struct Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198;
// UnityEngine.TextMesh
struct TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.Graphic
struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B;
// UnityEngine.UI.Selectable
struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;

IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0CF9912C87CE662DB805471FE51F9E3902E24B05;
IL2CPP_EXTERN_C String_t* _stringLiteral0EF80FE05747C3B8066F46572FA67164D400EB4C;
IL2CPP_EXTERN_C String_t* _stringLiteral39BFCE4FC3E16D17E8BAECB76A0542032AE6460A;
IL2CPP_EXTERN_C String_t* _stringLiteral4293DE8BAA84BEFF979E1C0FF1D9F6E939AF64F3;
IL2CPP_EXTERN_C String_t* _stringLiteral494C9D7BE9DFA92675F2FBB6C4B38C7F70C38F62;
IL2CPP_EXTERN_C String_t* _stringLiteral4BD0DA8D8466F0DDA99BD8C7274F1A00E6D9E572;
IL2CPP_EXTERN_C String_t* _stringLiteral829807EF52C7B8846BD67A635F78561AF68316A9;
IL2CPP_EXTERN_C String_t* _stringLiteralA1D28A508954C4E8248BBBDF385E3FB855AB6A7A;
IL2CPP_EXTERN_C String_t* _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC;
IL2CPP_EXTERN_C String_t* _stringLiteralCF4F1FE5D0BA6E121C00FC814B0C56AE954A65BA;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralEDCEFD93A3A438FA1701DBC9E39E86745E138B68;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m95916328C175634DC742DD13D032F149F1FBF58C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisHealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6_m6926C7925E795638A562C47471C5FF35D8DDED23_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_TryGetComponent_TisDemo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91_mA657A59968DC92CF336739AB31064F190688A393_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_UnitCSharp_HealthSystem_OnDamaged_mA11ED92EC84BF522A87597E476EAB03CE414250D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_UnitCSharp_HealthSystem_OnDead_mB8FCC204EB6B40020CC7478703A6FD8020558958_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_UnitCSharp_HealthSystem_OnHealed_mC11861945ACBBF6535571F527070A432DB84AC12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_Unit_HealthSystem_OnDamaged_mDFE694CC2C14414A79608E6455C85604F87DE4AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_Unit_HealthSystem_OnDead_mCAF5A68EF417E026DF0CE10006661A22AE35CF27_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_Unit_HealthSystem_OnHealed_mB19EBDE076B2174AE17EEE736D2B271E7A1EA67A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_TryGetComponent_TisIGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1_mF2CEAD5C764E76D26ECC9C4AC14F9B60C269E79A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Insert_mBF5D461C74C96AC635EAF8ADDF7E18AE64E815A1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_RemoveAt_mD17877118EA5CCF90E0D36CF420287270492A0F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m3B3616F62C011EB9A03B855AF972712288A6CEF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m731FBE36F3F0B2464BB7BDBC2584990068765100_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Awake_m2884174F2B16323136857B6BBC9DB1C342610763_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Bullet_OnTriggerEnter2D_mB2A859E2D7299AC5A27C99BB607562B42E47996D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Bullet_Start_m45FDDAFBE031D873A015A127BEC073D35835C7EF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Cannon_Awake_mE9E6BBDE4A029D331FC9456B7ADAE31E75D0E3EB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Cannon_Update_mB0F52105ADCFE487F8E4EC304298B38F124A90A1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_DamageButton_Start_m47D53331A12DCF11CB75D0FDBCAE47D3F7B7A6E3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_HealButton_Start_m728E0182CC8C7207A890A5568F24E583C8171DCF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Start_m3989B457771469E3BBCC14F6551F69808E0E2341_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_UnitCSharp_Awake_m38AEE7D1F5B4E9BC4678A54C0DFAF9C68F8F7546_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Unit_Start_m2AA94BF459E5ED88B019B66C3AA97EE9940E9E2C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Zombie_Awake_m948FA4A81F17E21AAC1C1AD509BD3A089671A03F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo_Zombie_Update_m382273970FD8253126D44E8A35EFE0CA2D5E4D7E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Demo__ctor_m8F4D851855681644B749EE73051E1BE06DEBAA1A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthBarUI_OnDestroy_m780AFFA696943D26015DE5582FC0E17E192DDBE9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystemComponent_Awake_m70FC15C7F1FB8E777D58EBE37E3E6C688E8BA0FB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_HealComplete_m98E89B04DD881539AA4EEBB44399A7F06AE58958_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_SetHealthMax_m34E7B7BC73D4367C0055A61AD1B21BA56FF5565B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_remove_OnDamaged_m3B7CC3942A6A596520B226EB032F3841737A7674_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_remove_OnDead_m28D623D97CEF97A8F418ECB1EE8C67032553D7B7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_remove_OnHealed_m5FC7901576A801F1C61EA47C70778D1993DC24D5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t HealthSystem_remove_OnHealthMaxChanged_mBF597E0E3EF51715FCD35D117B18E9895C473C89_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tEB64645ACABF28152F8E00076FED8C32E07BD967 
{
public:

public:
};


// System.Object


// CodeMonkey.HealthSystemCM.Demo_DamageButton_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6  : public RuntimeObject
{
public:
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_DamageButton_<>c__DisplayClass1_0::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_0;

public:
	inline static int32_t get_offset_of_healthSystem_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6, ___healthSystem_0)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_0() const { return ___healthSystem_0; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_0() { return &___healthSystem_0; }
	inline void set_healthSystem_0(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_0), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_HealButton_<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D  : public RuntimeObject
{
public:
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_HealButton_<>c__DisplayClass1_0::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_0;

public:
	inline static int32_t get_offset_of_healthSystem_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D, ___healthSystem_0)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_0() const { return ___healthSystem_0; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_0() { return &___healthSystem_0; }
	inline void set_healthSystem_0(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_0), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.HealthSystem
struct  HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3  : public RuntimeObject
{
public:
	// System.EventHandler CodeMonkey.HealthSystemCM.HealthSystem::OnHealthChanged
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___OnHealthChanged_0;
	// System.EventHandler CodeMonkey.HealthSystemCM.HealthSystem::OnHealthMaxChanged
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___OnHealthMaxChanged_1;
	// System.EventHandler CodeMonkey.HealthSystemCM.HealthSystem::OnDamaged
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___OnDamaged_2;
	// System.EventHandler CodeMonkey.HealthSystemCM.HealthSystem::OnHealed
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___OnHealed_3;
	// System.EventHandler CodeMonkey.HealthSystemCM.HealthSystem::OnDead
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___OnDead_4;
	// System.Single CodeMonkey.HealthSystemCM.HealthSystem::healthMax
	float ___healthMax_5;
	// System.Single CodeMonkey.HealthSystemCM.HealthSystem::health
	float ___health_6;

public:
	inline static int32_t get_offset_of_OnHealthChanged_0() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___OnHealthChanged_0)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_OnHealthChanged_0() const { return ___OnHealthChanged_0; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_OnHealthChanged_0() { return &___OnHealthChanged_0; }
	inline void set_OnHealthChanged_0(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___OnHealthChanged_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHealthChanged_0), (void*)value);
	}

	inline static int32_t get_offset_of_OnHealthMaxChanged_1() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___OnHealthMaxChanged_1)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_OnHealthMaxChanged_1() const { return ___OnHealthMaxChanged_1; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_OnHealthMaxChanged_1() { return &___OnHealthMaxChanged_1; }
	inline void set_OnHealthMaxChanged_1(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___OnHealthMaxChanged_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHealthMaxChanged_1), (void*)value);
	}

	inline static int32_t get_offset_of_OnDamaged_2() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___OnDamaged_2)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_OnDamaged_2() const { return ___OnDamaged_2; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_OnDamaged_2() { return &___OnDamaged_2; }
	inline void set_OnDamaged_2(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___OnDamaged_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDamaged_2), (void*)value);
	}

	inline static int32_t get_offset_of_OnHealed_3() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___OnHealed_3)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_OnHealed_3() const { return ___OnHealed_3; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_OnHealed_3() { return &___OnHealed_3; }
	inline void set_OnHealed_3(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___OnHealed_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHealed_3), (void*)value);
	}

	inline static int32_t get_offset_of_OnDead_4() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___OnDead_4)); }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * get_OnDead_4() const { return ___OnDead_4; }
	inline EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** get_address_of_OnDead_4() { return &___OnDead_4; }
	inline void set_OnDead_4(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * value)
	{
		___OnDead_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDead_4), (void*)value);
	}

	inline static int32_t get_offset_of_healthMax_5() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___healthMax_5)); }
	inline float get_healthMax_5() const { return ___healthMax_5; }
	inline float* get_address_of_healthMax_5() { return &___healthMax_5; }
	inline void set_healthMax_5(float value)
	{
		___healthMax_5 = value;
	}

	inline static int32_t get_offset_of_health_6() { return static_cast<int32_t>(offsetof(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3, ___health_6)); }
	inline float get_health_6() const { return ___health_6; }
	inline float* get_address_of_health_6() { return &___health_6; }
	inline void set_health_6(float value)
	{
		___health_6 = value;
	}
};


// CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section
struct  Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89  : public RuntimeObject
{
public:
	// System.String CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section::heading
	String_t* ___heading_0;
	// System.String CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section::linkText
	String_t* ___linkText_1;
	// System.String CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section::url
	String_t* ___url_2;
	// System.String[] CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section::textLines
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___textLines_3;

public:
	inline static int32_t get_offset_of_heading_0() { return static_cast<int32_t>(offsetof(Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89, ___heading_0)); }
	inline String_t* get_heading_0() const { return ___heading_0; }
	inline String_t** get_address_of_heading_0() { return &___heading_0; }
	inline void set_heading_0(String_t* value)
	{
		___heading_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___heading_0), (void*)value);
	}

	inline static int32_t get_offset_of_linkText_1() { return static_cast<int32_t>(offsetof(Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89, ___linkText_1)); }
	inline String_t* get_linkText_1() const { return ___linkText_1; }
	inline String_t** get_address_of_linkText_1() { return &___linkText_1; }
	inline void set_linkText_1(String_t* value)
	{
		___linkText_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___linkText_1), (void*)value);
	}

	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89, ___url_2)); }
	inline String_t* get_url_2() const { return ___url_2; }
	inline String_t** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(String_t* value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___url_2), (void*)value);
	}

	inline static int32_t get_offset_of_textLines_3() { return static_cast<int32_t>(offsetof(Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89, ___textLines_3)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_textLines_3() const { return ___textLines_3; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_textLines_3() { return &___textLines_3; }
	inline void set_textLines_3(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___textLines_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textLines_3), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.String>
struct  List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____items_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_0), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_Calls_0)); }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t18AA4F473C7B295216B7D4B9723B4F3DFCCC9A3F * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t6E5DF2EBDA42794B5FE0C6DAA97DF65F0BFF571F * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___list_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_list_0() const { return ___list_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.String>
struct  Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	String_t* ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___list_0)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_list_0() const { return ___list_0; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813, ___current_3)); }
	inline String_t* get_current_3() const { return ___current_3; }
	inline String_t** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(String_t* value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct  UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F  : public UnityEventBase_t6E0F7823762EE94BB8489B5AE41C7802A266D3D5
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_HighlightedSprite_0)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_PressedSprite_1)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_SelectedSprite_2)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A, ___m_DisabledSprite_3)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_pinvoke
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A_marshaled_com
{
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_HighlightedSprite_0;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_PressedSprite_1;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_SelectedSprite_2;
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.Button_ButtonClickedEvent
struct  ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB  : public UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F
{
public:

public:
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_NormalColor_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_HighlightedColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_PressedColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_SelectedColor_3)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_DisabledColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};


// UnityEngine.UI.Image_FillMethod
struct  FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5 
{
public:
	// System.Int32 UnityEngine.UI.Image_FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_t0DB7332683118B7C7D2748BE74CFBF19CD19F8C5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image_Type
struct  Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A 
{
public:
	// System.Int32 UnityEngine.UI.Image_Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t96B8A259B84ADA5E7D3B1F13AEAE22175937F38A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation_Mode
struct  Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26 
{
public:
	// System.Int32 UnityEngine.UI.Navigation_Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t93F92BD50B147AE38D82BA33FA77FD247A59FE26, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable_Transition
struct  Transition_tA9261C608B54C52324084A0B080E7A3E0548A181 
{
public:
	// System.Int32 UnityEngine.UI.Selectable_Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_tA9261C608B54C52324084A0B080E7A3E0548A181, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};

// UnityEngine.UI.Navigation
struct  Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 
{
public:
	// UnityEngine.UI.Navigation_Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_1() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnUp_1)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnUp_1() const { return ___m_SelectOnUp_1; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnUp_1() { return &___m_SelectOnUp_1; }
	inline void set_m_SelectOnUp_1(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnUp_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_2() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnDown_2)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnDown_2() const { return ___m_SelectOnDown_2; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnDown_2() { return &___m_SelectOnDown_2; }
	inline void set_m_SelectOnDown_2(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnDown_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_3() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnLeft_3)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnLeft_3() const { return ___m_SelectOnLeft_3; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnLeft_3() { return &___m_SelectOnLeft_3; }
	inline void set_m_SelectOnLeft_3(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnLeft_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_4() { return static_cast<int32_t>(offsetof(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07, ___m_SelectOnRight_4)); }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * get_m_SelectOnRight_4() const { return ___m_SelectOnRight_4; }
	inline Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A ** get_address_of_m_SelectOnRight_4() { return &___m_SelectOnRight_4; }
	inline void set_m_SelectOnRight_4(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * value)
	{
		___m_SelectOnRight_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07_marshaled_com
{
	int32_t ___m_Mode_0;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnUp_1;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnDown_2;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnLeft_3;
	Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A * ___m_SelectOnRight_4;
};

// CodeMonkey.HealthSystemCM.Readme_HealthSystem
struct  Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// UnityEngine.Texture2D CodeMonkey.HealthSystemCM.Readme_HealthSystem::codeMonkeyHeader
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___codeMonkeyHeader_4;
	// System.String CodeMonkey.HealthSystemCM.Readme_HealthSystem::title
	String_t* ___title_5;
	// System.String CodeMonkey.HealthSystemCM.Readme_HealthSystem::titlesub
	String_t* ___titlesub_6;
	// System.Boolean CodeMonkey.HealthSystemCM.Readme_HealthSystem::loadedLayout
	bool ___loadedLayout_7;
	// CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section[] CodeMonkey.HealthSystemCM.Readme_HealthSystem::sections
	SectionU5BU5D_tF83A823B01B1C60F29C7AE56EDC08A5C0BFCD715* ___sections_8;

public:
	inline static int32_t get_offset_of_codeMonkeyHeader_4() { return static_cast<int32_t>(offsetof(Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C, ___codeMonkeyHeader_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_codeMonkeyHeader_4() const { return ___codeMonkeyHeader_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_codeMonkeyHeader_4() { return &___codeMonkeyHeader_4; }
	inline void set_codeMonkeyHeader_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___codeMonkeyHeader_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___codeMonkeyHeader_4), (void*)value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C, ___title_5)); }
	inline String_t* get_title_5() const { return ___title_5; }
	inline String_t** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(String_t* value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___title_5), (void*)value);
	}

	inline static int32_t get_offset_of_titlesub_6() { return static_cast<int32_t>(offsetof(Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C, ___titlesub_6)); }
	inline String_t* get_titlesub_6() const { return ___titlesub_6; }
	inline String_t** get_address_of_titlesub_6() { return &___titlesub_6; }
	inline void set_titlesub_6(String_t* value)
	{
		___titlesub_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___titlesub_6), (void*)value);
	}

	inline static int32_t get_offset_of_loadedLayout_7() { return static_cast<int32_t>(offsetof(Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C, ___loadedLayout_7)); }
	inline bool get_loadedLayout_7() const { return ___loadedLayout_7; }
	inline bool* get_address_of_loadedLayout_7() { return &___loadedLayout_7; }
	inline void set_loadedLayout_7(bool value)
	{
		___loadedLayout_7 = value;
	}

	inline static int32_t get_offset_of_sections_8() { return static_cast<int32_t>(offsetof(Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C, ___sections_8)); }
	inline SectionU5BU5D_tF83A823B01B1C60F29C7AE56EDC08A5C0BFCD715* get_sections_8() const { return ___sections_8; }
	inline SectionU5BU5D_tF83A823B01B1C60F29C7AE56EDC08A5C0BFCD715** get_address_of_sections_8() { return &___sections_8; }
	inline void set_sections_8(SectionU5BU5D_tF83A823B01B1C60F29C7AE56EDC08A5C0BFCD715* value)
	{
		___sections_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sections_8), (void*)value);
	}
};


// System.EventHandler
struct  EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct  UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.ParticleSystem
struct  ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.TextMesh
struct  TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera_CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Collider2D
struct  Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// CodeMonkey.HealthSystemCM.Demo
struct  Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CodeMonkey.HealthSystemCM.Demo::getHealthSystemGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___getHealthSystemGameObject_4;
	// UnityEngine.TextMesh CodeMonkey.HealthSystemCM.Demo::textMesh
	TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * ___textMesh_5;
	// System.Collections.Generic.List`1<System.String> CodeMonkey.HealthSystemCM.Demo::logList
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___logList_6;

public:
	inline static int32_t get_offset_of_getHealthSystemGameObject_4() { return static_cast<int32_t>(offsetof(Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6, ___getHealthSystemGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_getHealthSystemGameObject_4() const { return ___getHealthSystemGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_getHealthSystemGameObject_4() { return &___getHealthSystemGameObject_4; }
	inline void set_getHealthSystemGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___getHealthSystemGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getHealthSystemGameObject_4), (void*)value);
	}

	inline static int32_t get_offset_of_textMesh_5() { return static_cast<int32_t>(offsetof(Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6, ___textMesh_5)); }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * get_textMesh_5() const { return ___textMesh_5; }
	inline TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A ** get_address_of_textMesh_5() { return &___textMesh_5; }
	inline void set_textMesh_5(TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * value)
	{
		___textMesh_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textMesh_5), (void*)value);
	}

	inline static int32_t get_offset_of_logList_6() { return static_cast<int32_t>(offsetof(Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6, ___logList_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_logList_6() const { return ___logList_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_logList_6() { return &___logList_6; }
	inline void set_logList_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___logList_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___logList_6), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_Bullet
struct  Demo_Bullet_t0CC46797722BD7E2889978D6B16C4B47FB3686F8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// CodeMonkey.HealthSystemCM.Demo_Cannon
struct  Demo_Cannon_t456090E165C54F555F95058559544CD25714162A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform CodeMonkey.HealthSystemCM.Demo_Cannon::pfBullet
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pfBullet_4;
	// UnityEngine.Transform CodeMonkey.HealthSystemCM.Demo_Cannon::aimTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___aimTransform_5;

public:
	inline static int32_t get_offset_of_pfBullet_4() { return static_cast<int32_t>(offsetof(Demo_Cannon_t456090E165C54F555F95058559544CD25714162A, ___pfBullet_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pfBullet_4() const { return ___pfBullet_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pfBullet_4() { return &___pfBullet_4; }
	inline void set_pfBullet_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pfBullet_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pfBullet_4), (void*)value);
	}

	inline static int32_t get_offset_of_aimTransform_5() { return static_cast<int32_t>(offsetof(Demo_Cannon_t456090E165C54F555F95058559544CD25714162A, ___aimTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_aimTransform_5() const { return ___aimTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_aimTransform_5() { return &___aimTransform_5; }
	inline void set_aimTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___aimTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aimTransform_5), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_DamageButton
struct  Demo_DamageButton_tA3803267184A803054306D09C2293BFC17FD719E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CodeMonkey.HealthSystemCM.Demo_DamageButton::getHealthSystemGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___getHealthSystemGameObject_4;

public:
	inline static int32_t get_offset_of_getHealthSystemGameObject_4() { return static_cast<int32_t>(offsetof(Demo_DamageButton_tA3803267184A803054306D09C2293BFC17FD719E, ___getHealthSystemGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_getHealthSystemGameObject_4() const { return ___getHealthSystemGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_getHealthSystemGameObject_4() { return &___getHealthSystemGameObject_4; }
	inline void set_getHealthSystemGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___getHealthSystemGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getHealthSystemGameObject_4), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_DefenseGame
struct  Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform CodeMonkey.HealthSystemCM.Demo_DefenseGame::pfZombie
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___pfZombie_4;
	// System.Single CodeMonkey.HealthSystemCM.Demo_DefenseGame::spawnTimer
	float ___spawnTimer_5;
	// System.Single CodeMonkey.HealthSystemCM.Demo_DefenseGame::spawnTimerMax
	float ___spawnTimerMax_6;

public:
	inline static int32_t get_offset_of_pfZombie_4() { return static_cast<int32_t>(offsetof(Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B, ___pfZombie_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_pfZombie_4() const { return ___pfZombie_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_pfZombie_4() { return &___pfZombie_4; }
	inline void set_pfZombie_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___pfZombie_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pfZombie_4), (void*)value);
	}

	inline static int32_t get_offset_of_spawnTimer_5() { return static_cast<int32_t>(offsetof(Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B, ___spawnTimer_5)); }
	inline float get_spawnTimer_5() const { return ___spawnTimer_5; }
	inline float* get_address_of_spawnTimer_5() { return &___spawnTimer_5; }
	inline void set_spawnTimer_5(float value)
	{
		___spawnTimer_5 = value;
	}

	inline static int32_t get_offset_of_spawnTimerMax_6() { return static_cast<int32_t>(offsetof(Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B, ___spawnTimerMax_6)); }
	inline float get_spawnTimerMax_6() const { return ___spawnTimerMax_6; }
	inline float* get_address_of_spawnTimerMax_6() { return &___spawnTimerMax_6; }
	inline void set_spawnTimerMax_6(float value)
	{
		___spawnTimerMax_6 = value;
	}
};


// CodeMonkey.HealthSystemCM.Demo_HealButton
struct  Demo_HealButton_tFA151641791C07388AA4AAEAC99DAA88D127D779  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CodeMonkey.HealthSystemCM.Demo_HealButton::getHealthSystemGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___getHealthSystemGameObject_4;

public:
	inline static int32_t get_offset_of_getHealthSystemGameObject_4() { return static_cast<int32_t>(offsetof(Demo_HealButton_tFA151641791C07388AA4AAEAC99DAA88D127D779, ___getHealthSystemGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_getHealthSystemGameObject_4() const { return ___getHealthSystemGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_getHealthSystemGameObject_4() { return &___getHealthSystemGameObject_4; }
	inline void set_getHealthSystemGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___getHealthSystemGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getHealthSystemGameObject_4), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_Unit
struct  Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem CodeMonkey.HealthSystemCM.Demo_Unit::damageParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___damageParticleSystem_4;
	// UnityEngine.ParticleSystem CodeMonkey.HealthSystemCM.Demo_Unit::healParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___healParticleSystem_5;
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_Unit::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_6;

public:
	inline static int32_t get_offset_of_damageParticleSystem_4() { return static_cast<int32_t>(offsetof(Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02, ___damageParticleSystem_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_damageParticleSystem_4() const { return ___damageParticleSystem_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_damageParticleSystem_4() { return &___damageParticleSystem_4; }
	inline void set_damageParticleSystem_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___damageParticleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___damageParticleSystem_4), (void*)value);
	}

	inline static int32_t get_offset_of_healParticleSystem_5() { return static_cast<int32_t>(offsetof(Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02, ___healParticleSystem_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_healParticleSystem_5() const { return ___healParticleSystem_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_healParticleSystem_5() { return &___healParticleSystem_5; }
	inline void set_healParticleSystem_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___healParticleSystem_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healParticleSystem_5), (void*)value);
	}

	inline static int32_t get_offset_of_healthSystem_6() { return static_cast<int32_t>(offsetof(Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02, ___healthSystem_6)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_6() const { return ___healthSystem_6; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_6() { return &___healthSystem_6; }
	inline void set_healthSystem_6(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_6), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_UnitCSharp
struct  Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem CodeMonkey.HealthSystemCM.Demo_UnitCSharp::damageParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___damageParticleSystem_4;
	// UnityEngine.ParticleSystem CodeMonkey.HealthSystemCM.Demo_UnitCSharp::healParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___healParticleSystem_5;
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_UnitCSharp::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_6;

public:
	inline static int32_t get_offset_of_damageParticleSystem_4() { return static_cast<int32_t>(offsetof(Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C, ___damageParticleSystem_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_damageParticleSystem_4() const { return ___damageParticleSystem_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_damageParticleSystem_4() { return &___damageParticleSystem_4; }
	inline void set_damageParticleSystem_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___damageParticleSystem_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___damageParticleSystem_4), (void*)value);
	}

	inline static int32_t get_offset_of_healParticleSystem_5() { return static_cast<int32_t>(offsetof(Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C, ___healParticleSystem_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_healParticleSystem_5() const { return ___healParticleSystem_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_healParticleSystem_5() { return &___healParticleSystem_5; }
	inline void set_healParticleSystem_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___healParticleSystem_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healParticleSystem_5), (void*)value);
	}

	inline static int32_t get_offset_of_healthSystem_6() { return static_cast<int32_t>(offsetof(Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C, ___healthSystem_6)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_6() const { return ___healthSystem_6; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_6() { return &___healthSystem_6; }
	inline void set_healthSystem_6(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_6), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.Demo_Zombie
struct  Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_Zombie::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_4;

public:
	inline static int32_t get_offset_of_healthSystem_4() { return static_cast<int32_t>(offsetof(Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91, ___healthSystem_4)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_4() const { return ___healthSystem_4; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_4() { return &___healthSystem_4; }
	inline void set_healthSystem_4(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_4), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.HealthBarUI
struct  HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject CodeMonkey.HealthSystemCM.HealthBarUI::getHealthSystemGameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___getHealthSystemGameObject_4;
	// UnityEngine.UI.Image CodeMonkey.HealthSystemCM.HealthBarUI::image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___image_5;
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.HealthBarUI::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_6;

public:
	inline static int32_t get_offset_of_getHealthSystemGameObject_4() { return static_cast<int32_t>(offsetof(HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4, ___getHealthSystemGameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_getHealthSystemGameObject_4() const { return ___getHealthSystemGameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_getHealthSystemGameObject_4() { return &___getHealthSystemGameObject_4; }
	inline void set_getHealthSystemGameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___getHealthSystemGameObject_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getHealthSystemGameObject_4), (void*)value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4, ___image_5)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_image_5() const { return ___image_5; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___image_5), (void*)value);
	}

	inline static int32_t get_offset_of_healthSystem_6() { return static_cast<int32_t>(offsetof(HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4, ___healthSystem_6)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_6() const { return ___healthSystem_6; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_6() { return &___healthSystem_6; }
	inline void set_healthSystem_6(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_6), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.HealthSystemComponent
struct  HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single CodeMonkey.HealthSystemCM.HealthSystemComponent::healthAmountMax
	float ___healthAmountMax_4;
	// System.Single CodeMonkey.HealthSystemCM.HealthSystemComponent::startingHealthAmount
	float ___startingHealthAmount_5;
	// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.HealthSystemComponent::healthSystem
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem_6;

public:
	inline static int32_t get_offset_of_healthAmountMax_4() { return static_cast<int32_t>(offsetof(HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6, ___healthAmountMax_4)); }
	inline float get_healthAmountMax_4() const { return ___healthAmountMax_4; }
	inline float* get_address_of_healthAmountMax_4() { return &___healthAmountMax_4; }
	inline void set_healthAmountMax_4(float value)
	{
		___healthAmountMax_4 = value;
	}

	inline static int32_t get_offset_of_startingHealthAmount_5() { return static_cast<int32_t>(offsetof(HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6, ___startingHealthAmount_5)); }
	inline float get_startingHealthAmount_5() const { return ___startingHealthAmount_5; }
	inline float* get_address_of_startingHealthAmount_5() { return &___startingHealthAmount_5; }
	inline void set_startingHealthAmount_5(float value)
	{
		___startingHealthAmount_5 = value;
	}

	inline static int32_t get_offset_of_healthSystem_6() { return static_cast<int32_t>(offsetof(HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6, ___healthSystem_6)); }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * get_healthSystem_6() const { return ___healthSystem_6; }
	inline HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** get_address_of_healthSystem_6() { return &___healthSystem_6; }
	inline void set_healthSystem_6(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * value)
	{
		___healthSystem_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___healthSystem_6), (void*)value);
	}
};


// CodeMonkey.HealthSystemCM.LookAtCamera
struct  LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean CodeMonkey.HealthSystemCM.LookAtCamera::invert
	bool ___invert_4;
	// UnityEngine.Transform CodeMonkey.HealthSystemCM.LookAtCamera::mainCameraTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mainCameraTransform_5;

public:
	inline static int32_t get_offset_of_invert_4() { return static_cast<int32_t>(offsetof(LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0, ___invert_4)); }
	inline bool get_invert_4() const { return ___invert_4; }
	inline bool* get_address_of_invert_4() { return &___invert_4; }
	inline void set_invert_4(bool value)
	{
		___invert_4 = value;
	}

	inline static int32_t get_offset_of_mainCameraTransform_5() { return static_cast<int32_t>(offsetof(LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0, ___mainCameraTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mainCameraTransform_5() const { return ___mainCameraTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mainCameraTransform_5() { return &___mainCameraTransform_5; }
	inline void set_mainCameraTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mainCameraTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mainCameraTransform_5), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___m_RectTransform_11;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * ___m_CanvasRenderer_12;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___m_Canvas_13;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_14;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_15;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyLayoutCallback_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyVertsCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___m_OnDirtyMaterialCallback_18;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___m_CachedMesh_21;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___m_CachedUvs_22;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * ___m_ColorTweenRunner_23;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Material_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Color_7)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_11() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_RectTransform_11)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_m_RectTransform_11() const { return ___m_RectTransform_11; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_m_RectTransform_11() { return &___m_RectTransform_11; }
	inline void set_m_RectTransform_11(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___m_RectTransform_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_12() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CanvasRenderer_12)); }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * get_m_CanvasRenderer_12() const { return ___m_CanvasRenderer_12; }
	inline CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 ** get_address_of_m_CanvasRenderer_12() { return &___m_CanvasRenderer_12; }
	inline void set_m_CanvasRenderer_12(CanvasRenderer_tB4D9C9FE77FD5C9C4546FC022D6E956960BC2B72 * value)
	{
		___m_CanvasRenderer_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_13() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_Canvas_13)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_m_Canvas_13() const { return ___m_Canvas_13; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_m_Canvas_13() { return &___m_Canvas_13; }
	inline void set_m_Canvas_13(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___m_Canvas_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_14() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_VertsDirty_14)); }
	inline bool get_m_VertsDirty_14() const { return ___m_VertsDirty_14; }
	inline bool* get_address_of_m_VertsDirty_14() { return &___m_VertsDirty_14; }
	inline void set_m_VertsDirty_14(bool value)
	{
		___m_VertsDirty_14 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_MaterialDirty_15)); }
	inline bool get_m_MaterialDirty_15() const { return ___m_MaterialDirty_15; }
	inline bool* get_address_of_m_MaterialDirty_15() { return &___m_MaterialDirty_15; }
	inline void set_m_MaterialDirty_15(bool value)
	{
		___m_MaterialDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_16() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyLayoutCallback_16)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyLayoutCallback_16() const { return ___m_OnDirtyLayoutCallback_16; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyLayoutCallback_16() { return &___m_OnDirtyLayoutCallback_16; }
	inline void set_m_OnDirtyLayoutCallback_16(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyLayoutCallback_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyVertsCallback_17)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyVertsCallback_17() const { return ___m_OnDirtyVertsCallback_17; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyVertsCallback_17() { return &___m_OnDirtyVertsCallback_17; }
	inline void set_m_OnDirtyVertsCallback_17(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyVertsCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_OnDirtyMaterialCallback_18)); }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * get_m_OnDirtyMaterialCallback_18() const { return ___m_OnDirtyMaterialCallback_18; }
	inline UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 ** get_address_of_m_OnDirtyMaterialCallback_18() { return &___m_OnDirtyMaterialCallback_18; }
	inline void set_m_OnDirtyMaterialCallback_18(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * value)
	{
		___m_OnDirtyMaterialCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_21() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedMesh_21)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_m_CachedMesh_21() const { return ___m_CachedMesh_21; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_m_CachedMesh_21() { return &___m_CachedMesh_21; }
	inline void set_m_CachedMesh_21(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___m_CachedMesh_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_22() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_CachedUvs_22)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_m_CachedUvs_22() const { return ___m_CachedUvs_22; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_m_CachedUvs_22() { return &___m_CachedUvs_22; }
	inline void set_m_CachedUvs_22(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___m_CachedUvs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_23() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___m_ColorTweenRunner_23)); }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * get_m_ColorTweenRunner_23() const { return ___m_ColorTweenRunner_23; }
	inline TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 ** get_address_of_m_ColorTweenRunner_23() { return &___m_ColorTweenRunner_23; }
	inline void set_m_ColorTweenRunner_23(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * value)
	{
		___m_ColorTweenRunner_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_24() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_24; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_24(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_24 = value;
	}
};

struct Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * ___s_Mesh_19;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * ___s_VertexHelper_20;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_DefaultUI_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_19() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_Mesh_19)); }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * get_s_Mesh_19() const { return ___s_Mesh_19; }
	inline Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C ** get_address_of_s_Mesh_19() { return &___s_Mesh_19; }
	inline void set_s_Mesh_19(Mesh_t6106B8D8E4C691321581AB0445552EC78B947B8C * value)
	{
		___s_Mesh_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_19), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_20() { return static_cast<int32_t>(offsetof(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8_StaticFields, ___s_VertexHelper_20)); }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * get_s_VertexHelper_20() const { return ___s_VertexHelper_20; }
	inline VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F ** get_address_of_s_VertexHelper_20() { return &___s_VertexHelper_20; }
	inline void set_s_VertexHelper_20(VertexHelper_t27373EA2CF0F5810EC8CF873D0A6D6C0B23DAC3F * value)
	{
		___s_VertexHelper_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_20), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A  : public UIBehaviour_t3C3C339CD5677BA7FC27C352FED8B78052A3FE70
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  ___m_Navigation_7;
	// UnityEngine.UI.Selectable_Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Navigation_7)); }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07 * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t761250C05C09773B75F5E0D52DDCBBFE60288A07  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_4), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Colors_9)); }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t93B54DF6E8D65D24CEA9726CA745E48C53E3B1EA  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_SpriteState_10)); }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t58B9DD66A79CD69AB4CFC3AD0C41E45DC2192C0A  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_t164EF8B310E294B7D0F6BF1A87376731EBD06DC5 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_TargetGraphic_13)); }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A, ___m_CanvasGroupCache_19)); }
	inline List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_tE7746C234F913BA0579DAC892E7288A1C7664A0A * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_t98F7C5A863B20CD5DBE49CE288038BA954C83F02* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B  : public Selectable_tAA9065030FE0468018DEC880302F95FEA9C0133A
{
public:
	// UnityEngine.UI.Button_ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B, ___m_OnClick_20)); }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F  : public Graphic_tBA2C3EF11D3DAEBB57F6879AB0BB4F8BD40D00D8
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_25;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___m_MaskMaterial_26;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * ___m_ParentMask_27;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_30;
	// UnityEngine.UI.MaskableGraphic_CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * ___m_OnCullStateChanged_31;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_32;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_33;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___m_Corners_34;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculateStencil_25)); }
	inline bool get_m_ShouldRecalculateStencil_25() const { return ___m_ShouldRecalculateStencil_25; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_25() { return &___m_ShouldRecalculateStencil_25; }
	inline void set_m_ShouldRecalculateStencil_25(bool value)
	{
		___m_ShouldRecalculateStencil_25 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_MaskMaterial_26)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_m_MaskMaterial_26() const { return ___m_MaskMaterial_26; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_m_MaskMaterial_26() { return &___m_MaskMaterial_26; }
	inline void set_m_MaskMaterial_26(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___m_MaskMaterial_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ParentMask_27)); }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * get_m_ParentMask_27() const { return ___m_ParentMask_27; }
	inline RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B ** get_address_of_m_ParentMask_27() { return &___m_ParentMask_27; }
	inline void set_m_ParentMask_27(RectMask2D_tF2CF19F2A4FE2D2FFC7E6F7809374757CA2F377B * value)
	{
		___m_ParentMask_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Maskable_28)); }
	inline bool get_m_Maskable_28() const { return ___m_Maskable_28; }
	inline bool* get_address_of_m_Maskable_28() { return &___m_Maskable_28; }
	inline void set_m_Maskable_28(bool value)
	{
		___m_Maskable_28 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IsMaskingGraphic_29)); }
	inline bool get_m_IsMaskingGraphic_29() const { return ___m_IsMaskingGraphic_29; }
	inline bool* get_address_of_m_IsMaskingGraphic_29() { return &___m_IsMaskingGraphic_29; }
	inline void set_m_IsMaskingGraphic_29(bool value)
	{
		___m_IsMaskingGraphic_29 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_IncludeForMasking_30)); }
	inline bool get_m_IncludeForMasking_30() const { return ___m_IncludeForMasking_30; }
	inline bool* get_address_of_m_IncludeForMasking_30() { return &___m_IncludeForMasking_30; }
	inline void set_m_IncludeForMasking_30(bool value)
	{
		___m_IncludeForMasking_30 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_OnCullStateChanged_31)); }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * get_m_OnCullStateChanged_31() const { return ___m_OnCullStateChanged_31; }
	inline CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 ** get_address_of_m_OnCullStateChanged_31() { return &___m_OnCullStateChanged_31; }
	inline void set_m_OnCullStateChanged_31(CullStateChangedEvent_t6BC3E87DBC04B585798460D55F56B86C23B62FE4 * value)
	{
		___m_OnCullStateChanged_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_ShouldRecalculate_32)); }
	inline bool get_m_ShouldRecalculate_32() const { return ___m_ShouldRecalculate_32; }
	inline bool* get_address_of_m_ShouldRecalculate_32() { return &___m_ShouldRecalculate_32; }
	inline void set_m_ShouldRecalculate_32(bool value)
	{
		___m_ShouldRecalculate_32 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_StencilValue_33)); }
	inline int32_t get_m_StencilValue_33() const { return ___m_StencilValue_33; }
	inline int32_t* get_address_of_m_StencilValue_33() { return &___m_StencilValue_33; }
	inline void set_m_StencilValue_33(int32_t value)
	{
		___m_StencilValue_33 = value;
	}

	inline static int32_t get_offset_of_m_Corners_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F, ___m_Corners_34)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_m_Corners_34() const { return ___m_Corners_34; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_m_Corners_34() { return &___m_Corners_34; }
	inline void set_m_Corners_34(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___m_Corners_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_34), (void*)value);
	}
};


// UnityEngine.UI.Image
struct  Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E  : public MaskableGraphic_tDA46A5925C6A2101217C9F52C855B5C1A36A7A0F
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_Sprite_36;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * ___m_OverrideSprite_37;
	// UnityEngine.UI.Image_Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_38;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_39;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_40;
	// UnityEngine.UI.Image_FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_41;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_42;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_43;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_44;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_45;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_46;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_47;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_48;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_49;

public:
	inline static int32_t get_offset_of_m_Sprite_36() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Sprite_36)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_Sprite_36() const { return ___m_Sprite_36; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_Sprite_36() { return &___m_Sprite_36; }
	inline void set_m_Sprite_36(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_Sprite_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_37() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_OverrideSprite_37)); }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * get_m_OverrideSprite_37() const { return ___m_OverrideSprite_37; }
	inline Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 ** get_address_of_m_OverrideSprite_37() { return &___m_OverrideSprite_37; }
	inline void set_m_OverrideSprite_37(Sprite_tCA09498D612D08DE668653AF1E9C12BF53434198 * value)
	{
		___m_OverrideSprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_38() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Type_38)); }
	inline int32_t get_m_Type_38() const { return ___m_Type_38; }
	inline int32_t* get_address_of_m_Type_38() { return &___m_Type_38; }
	inline void set_m_Type_38(int32_t value)
	{
		___m_Type_38 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_39() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PreserveAspect_39)); }
	inline bool get_m_PreserveAspect_39() const { return ___m_PreserveAspect_39; }
	inline bool* get_address_of_m_PreserveAspect_39() { return &___m_PreserveAspect_39; }
	inline void set_m_PreserveAspect_39(bool value)
	{
		___m_PreserveAspect_39 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_40() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillCenter_40)); }
	inline bool get_m_FillCenter_40() const { return ___m_FillCenter_40; }
	inline bool* get_address_of_m_FillCenter_40() { return &___m_FillCenter_40; }
	inline void set_m_FillCenter_40(bool value)
	{
		___m_FillCenter_40 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_41() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillMethod_41)); }
	inline int32_t get_m_FillMethod_41() const { return ___m_FillMethod_41; }
	inline int32_t* get_address_of_m_FillMethod_41() { return &___m_FillMethod_41; }
	inline void set_m_FillMethod_41(int32_t value)
	{
		___m_FillMethod_41 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_42() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillAmount_42)); }
	inline float get_m_FillAmount_42() const { return ___m_FillAmount_42; }
	inline float* get_address_of_m_FillAmount_42() { return &___m_FillAmount_42; }
	inline void set_m_FillAmount_42(float value)
	{
		___m_FillAmount_42 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_43() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillClockwise_43)); }
	inline bool get_m_FillClockwise_43() const { return ___m_FillClockwise_43; }
	inline bool* get_address_of_m_FillClockwise_43() { return &___m_FillClockwise_43; }
	inline void set_m_FillClockwise_43(bool value)
	{
		___m_FillClockwise_43 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_44() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_FillOrigin_44)); }
	inline int32_t get_m_FillOrigin_44() const { return ___m_FillOrigin_44; }
	inline int32_t* get_address_of_m_FillOrigin_44() { return &___m_FillOrigin_44; }
	inline void set_m_FillOrigin_44(int32_t value)
	{
		___m_FillOrigin_44 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_45() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_AlphaHitTestMinimumThreshold_45)); }
	inline float get_m_AlphaHitTestMinimumThreshold_45() const { return ___m_AlphaHitTestMinimumThreshold_45; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_45() { return &___m_AlphaHitTestMinimumThreshold_45; }
	inline void set_m_AlphaHitTestMinimumThreshold_45(float value)
	{
		___m_AlphaHitTestMinimumThreshold_45 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_46() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_Tracked_46)); }
	inline bool get_m_Tracked_46() const { return ___m_Tracked_46; }
	inline bool* get_address_of_m_Tracked_46() { return &___m_Tracked_46; }
	inline void set_m_Tracked_46(bool value)
	{
		___m_Tracked_46 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_47() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_UseSpriteMesh_47)); }
	inline bool get_m_UseSpriteMesh_47() const { return ___m_UseSpriteMesh_47; }
	inline bool* get_address_of_m_UseSpriteMesh_47() { return &___m_UseSpriteMesh_47; }
	inline void set_m_UseSpriteMesh_47(bool value)
	{
		___m_UseSpriteMesh_47 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_48() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_PixelsPerUnitMultiplier_48)); }
	inline float get_m_PixelsPerUnitMultiplier_48() const { return ___m_PixelsPerUnitMultiplier_48; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_48() { return &___m_PixelsPerUnitMultiplier_48; }
	inline void set_m_PixelsPerUnitMultiplier_48(float value)
	{
		___m_PixelsPerUnitMultiplier_48 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_49() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E, ___m_CachedReferencePixelsPerUnit_49)); }
	inline float get_m_CachedReferencePixelsPerUnit_49() const { return ___m_CachedReferencePixelsPerUnit_49; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_49() { return &___m_CachedReferencePixelsPerUnit_49; }
	inline void set_m_CachedReferencePixelsPerUnit_49(float value)
	{
		___m_CachedReferencePixelsPerUnit_49 = value;
	}
};

struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___s_ETC1DefaultUI_35;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_VertScratch_50;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ___s_UVScratch_51;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Xy_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ___s_Uv_53;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 * ___m_TrackedTexturelessImages_54;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_55;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_35() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_ETC1DefaultUI_35)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_s_ETC1DefaultUI_35() const { return ___s_ETC1DefaultUI_35; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_s_ETC1DefaultUI_35() { return &___s_ETC1DefaultUI_35; }
	inline void set_s_ETC1DefaultUI_35(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___s_ETC1DefaultUI_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_35), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_50() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_VertScratch_50)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_VertScratch_50() const { return ___s_VertScratch_50; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_VertScratch_50() { return &___s_VertScratch_50; }
	inline void set_s_VertScratch_50(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_VertScratch_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_50), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_51() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_UVScratch_51)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get_s_UVScratch_51() const { return ___s_UVScratch_51; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of_s_UVScratch_51() { return &___s_UVScratch_51; }
	inline void set_s_UVScratch_51(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		___s_UVScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_52() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Xy_52)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Xy_52() const { return ___s_Xy_52; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Xy_52() { return &___s_Xy_52; }
	inline void set_s_Xy_52(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Xy_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_53() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Uv_53)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get_s_Uv_53() const { return ___s_Uv_53; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of_s_Uv_53() { return &___s_Uv_53; }
	inline void set_s_Uv_53(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		___s_Uv_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_53), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_54() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___m_TrackedTexturelessImages_54)); }
	inline List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 * get_m_TrackedTexturelessImages_54() const { return ___m_TrackedTexturelessImages_54; }
	inline List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 ** get_address_of_m_TrackedTexturelessImages_54() { return &___m_TrackedTexturelessImages_54; }
	inline void set_m_TrackedTexturelessImages_54(List_1_tA9C10612DACE8F188F3B35F6173354C7225A0883 * value)
	{
		___m_TrackedTexturelessImages_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_54), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_55() { return static_cast<int32_t>(offsetof(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E_StaticFields, ___s_Initialized_55)); }
	inline bool get_s_Initialized_55() const { return ___s_Initialized_55; }
	inline bool* get_address_of_s_Initialized_55() { return &___s_Initialized_55; }
	inline void set_s_Initialized_55(bool value)
	{
		___s_Initialized_55 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Insert_m327E513FB78F72441BBF2756AFCC788F89A4FA52_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD  List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m233A1E6EF90A3BA46CD83BFC568F4E4DB4D93CC9_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::TryGetComponent<System.Object>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_TryGetComponent_TisRuntimeObject_m52AE59A1695186BE8CFD9E6C6EC4C7AD725B3724_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, RuntimeObject ** ___component0, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mFE9C42D5336D4F9EFF8CD96E2A26962EFF523947_gshared (RuntimeObject * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::TryGetComponent<System.Object>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_TryGetComponent_TisRuntimeObject_mFAD77C8D796CD0799D5DBC998E0FF32B5AE8EA86_gshared (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, RuntimeObject ** ___component0, const RuntimeMethod* method);

// System.Void UnityEngine.TextMesh::set_text(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220 (TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.Demo::AddLog(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, String_t* ___logString0, const RuntimeMethod* method);
// System.Boolean CodeMonkey.HealthSystemCM.HealthSystem::TryGetHealthSystem(UnityEngine.GameObject,CodeMonkey.HealthSystemCM.HealthSystem&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___getHealthSystemGameObject0, HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** ___healthSystem1, bool ___logErrors2, const RuntimeMethod* method);
// System.Void System.EventHandler::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealthChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnDamaged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnDead(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealthMaxChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Insert(System.Int32,!0)
inline void List_1_Insert_mBF5D461C74C96AC635EAF8ADDF7E18AE64E815A1 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, int32_t ___index0, String_t* ___item1, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, int32_t, String_t*, const RuntimeMethod*))List_1_Insert_m327E513FB78F72441BBF2756AFCC788F89A4FA52_gshared)(__this, ___index0, ___item1, method);
}
// System.Int32 System.Collections.Generic.List`1<System.String>::get_Count()
inline int32_t List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_inline (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::RemoveAt(System.Int32)
inline void List_1_RemoveAt_mD17877118EA5CCF90E0D36CF420287270492A0F2 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, int32_t, const RuntimeMethod*))List_1_RemoveAt_m1EC5117AD814B97460F8F95D49A428032FE37CBF_gshared)(__this, ___index0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1_GetEnumerator_m52CC760E475D226A2B75048D70C4E22692F9F68D_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_inline (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *, const RuntimeMethod*))Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline)(__this, method);
}
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *, const RuntimeMethod*))Enumerator_MoveNext_m38B1099DDAD7EEDE2F4CDAB11C095AC784AC2E34_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978 (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *, const RuntimeMethod*))Enumerator_Dispose_m94D0DAE031619503CDA6E53C5C3CC78AF3139472_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06 (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *, const RuntimeMethod*))List_1__ctor_mC832F1AC0F814BAEB19175F5D7972A7507508BC3_gshared)(__this, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m233A1E6EF90A3BA46CD83BFC568F4E4DB4D93CC9_gshared)(__this, method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_right_mC32CE648E98D3D4F62F897A2751EE567C7C0CFB0 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83 (Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * __this, Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::TryGetComponent<CodeMonkey.HealthSystemCM.Demo_Zombie>(!!0&)
inline bool Component_TryGetComponent_TisDemo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91_mA657A59968DC92CF336739AB31064F190688A393 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 ** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 **, const RuntimeMethod*))Component_TryGetComponent_TisRuntimeObject_m52AE59A1695186BE8CFD9E6C6EC4C7AD725B3724_gshared)(__this, ___component0, method);
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Damage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Zombie_Damage_m3345441E19B8E4D2C9FEC05BC9EFEC83E7026BEC (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, String_t* ___n0, const RuntimeMethod* method);
// UnityEngine.Vector3 CodeMonkey.HealthSystemCM.Demo_Cannon::GetMouseWorldPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Demo_Cannon_GetMouseWorldPosition_m491CA6AB5CA29173DE131343E03C88B2C62AA296 (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, const RuntimeMethod* method);
// System.Single CodeMonkey.HealthSystemCM.Demo_Cannon::GetAngleFromVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0 (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_m4B2B374C0B089A7ED0B522A3A4C56FA868992685 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m5AD76E22AA839706219AD86A4E0BE5276AF8E28A (int32_t ___button0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Transform>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___original0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position1, Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation2, const RuntimeMethod* method)
{
	return ((  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * (*) (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA *, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 , Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mFE9C42D5336D4F9EFF8CD96E2A26962EFF523947_gshared)(___original0, ___position1, ___rotation2, method);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Input_get_mousePosition_m1F6706785983B41FE8D5CBB81B5F15F68EBD9A53 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Camera::ScreenToWorldPoint(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5 (Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton/<>c__DisplayClass1_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0__ctor_mA7AC968334D5CAF0948B61266BD426905B25CF94 (U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * Component_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m95916328C175634DC742DD13D032F149F1FBF58C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m233A1E6EF90A3BA46CD83BFC568F4E4DB4D93CC9_gshared)(__this, method);
}
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * Button_get_onClick_m77E8CA6917881760CC7900930F4C789F3E2F8817_inline (Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC (UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m31973FDDC5BB0B2828AB6EF519EC4FD6563499C9 (UnityEvent_t5C6DDC2FCDF7F5C1808F1DDFBAD27A383F5FE65F * __this, UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * ___call0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::Damage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___amount0, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::SpawnZombie()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73 (Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780 (int32_t ___min0, int32_t ___max1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64 (const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.Demo_HealButton/<>c__DisplayClass1_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0__ctor_mDC1214DA7828992B6502E91C6F8FD01B01FEC47D (U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * __this, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::Heal(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___amount0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<CodeMonkey.HealthSystemCM.HealthSystemComponent>()
inline HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * Component_GetComponent_TisHealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6_m6926C7925E795638A562C47471C5FF35D8DDED23 (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m233A1E6EF90A3BA46CD83BFC568F4E4DB4D93CC9_gshared)(__this, method);
}
// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.HealthSystemComponent::GetHealthSystem()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * HealthSystemComponent_GetHealthSystem_mA7F8A5C583C2C0ACC401F49F83FDFDE6164AB832_inline (HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem::Play()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F (ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * __this, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___healthMax0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___a0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::SetHealthSystem(CodeMonkey.HealthSystemCM.HealthSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealthChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::UpdateHealthBar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_UpdateHealthBar_m0AD4E403485C4F870845738CA8F717DC137E59B5 (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, const RuntimeMethod* method);
// System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealthNormalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HealthSystem_GetHealthNormalized_mB1FEDFABE4083B167BCE7A3A637D41DB996DF9E8 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_fillAmount_m3939CACF382B30A0353F300E2AE2D3823E1D42F4 (Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * __this, float ___value0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void System.EventHandler::Invoke(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800 (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::Die()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::TryGetComponent<CodeMonkey.HealthSystemCM.IGetHealthSystem>(!!0&)
inline bool GameObject_TryGetComponent_TisIGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1_mF2CEAD5C764E76D26ECC9C4AC14F9B60C269E79A (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, RuntimeObject** ___component0, const RuntimeMethod* method)
{
	return ((  bool (*) (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *, RuntimeObject**, const RuntimeMethod*))GameObject_TryGetComponent_TisRuntimeObject_mFAD77C8D796CD0799D5DBC998E0FF32B5AE8EA86_gshared)(__this, ___component0, method);
}
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::SetHealth(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___health0, const RuntimeMethod* method);
// System.Void CodeMonkey.HealthSystemCM.LookAtCamera::LookAt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776 (LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_m3EC94482B2585FE03AEEDF90325A1F0B9A84960E (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___worldPosition0, const RuntimeMethod* method);
// System.Void UnityEngine.ScriptableObject::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B (ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Awake_m2884174F2B16323136857B6BBC9DB1C342610763 (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Awake_m2884174F2B16323136857B6BBC9DB1C342610763_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// textMesh.text = "";
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_0 = __this->get_textMesh_5();
		NullCheck(L_0);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_0, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		// AddLog("Log...");
		Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC(__this, _stringLiteral0CF9912C87CE662DB805471FE51F9E3902E24B05, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Start_m3989B457771469E3BBCC14F6551F69808E0E2341 (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Start_m3989B457771469E3BBCC14F6551F69808E0E2341_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * V_0 = NULL;
	{
		// HealthSystem.TryGetHealthSystem(getHealthSystemGameObject, out HealthSystem healthSystem, true);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_getHealthSystemGameObject_4();
		HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E(L_0, (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 **)(&V_0), (bool)1, /*hidden argument*/NULL);
		// healthSystem.OnHealthChanged += (object sender, System.EventArgs e) => {
		//     AddLog("OnHealthChanged");
		// };
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_2, __this, (intptr_t)((intptr_t)Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34(L_1, L_2, /*hidden argument*/NULL);
		// healthSystem.OnDamaged += (object sender, System.EventArgs e) => {
		//     AddLog("OnDamaged");
		// };
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_3 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_4, __this, (intptr_t)((intptr_t)Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089(L_3, L_4, /*hidden argument*/NULL);
		// healthSystem.OnDead += (object sender, System.EventArgs e) => {
		//     AddLog("OnDead");
		// };
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_5 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_6, __this, (intptr_t)((intptr_t)Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52(L_5, L_6, /*hidden argument*/NULL);
		// healthSystem.OnHealed += (object sender, System.EventArgs e) => {
		//     AddLog("OnHealed");
		// };
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_7 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_8, __this, (intptr_t)((intptr_t)Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10(L_7, L_8, /*hidden argument*/NULL);
		// healthSystem.OnHealthMaxChanged += (object sender, System.EventArgs e) => {
		//     AddLog("OnHealthMaxChanged");
		// };
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_10, __this, (intptr_t)((intptr_t)Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_9);
		HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B(L_9, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::AddLog(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, String_t* ___logString0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  V_1;
	memset((&V_1), 0, sizeof(V_1));
	String_t* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		// logList.Insert(0, logString);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = __this->get_logList_6();
		String_t* L_1 = ___logString0;
		NullCheck(L_0);
		List_1_Insert_mBF5D461C74C96AC635EAF8ADDF7E18AE64E815A1(L_0, 0, L_1, /*hidden argument*/List_1_Insert_mBF5D461C74C96AC635EAF8ADDF7E18AE64E815A1_RuntimeMethod_var);
		// if (logList.Count > 10) {
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_2 = __this->get_logList_6();
		NullCheck(L_2);
		int32_t L_3 = List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_inline(L_2, /*hidden argument*/List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_RuntimeMethod_var);
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)10))))
		{
			goto IL_0034;
		}
	}
	{
		// logList.RemoveAt(logList.Count-1);
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_4 = __this->get_logList_6();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_5 = __this->get_logList_6();
		NullCheck(L_5);
		int32_t L_6 = List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_inline(L_5, /*hidden argument*/List_1_get_Count_m4151A68BD4CB1D737213E7595F574987F8C812B4_RuntimeMethod_var);
		NullCheck(L_4);
		List_1_RemoveAt_mD17877118EA5CCF90E0D36CF420287270492A0F2(L_4, ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1)), /*hidden argument*/List_1_RemoveAt_mD17877118EA5CCF90E0D36CF420287270492A0F2_RuntimeMethod_var);
	}

IL_0034:
	{
		// string text = "";
		V_0 = _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
		// foreach (string str in logList) {
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_7 = __this->get_logList_6();
		NullCheck(L_7);
		Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813  L_8 = List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E(L_7, /*hidden argument*/List_1_GetEnumerator_mDFFBEE5A0B86EF1F068C4ED0ABC0F39B7CA7677E_RuntimeMethod_var);
		V_1 = L_8;
	}

IL_0046:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005d;
		}

IL_0048:
		{
			// foreach (string str in logList) {
			String_t* L_9 = Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_inline((Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *)(&V_1), /*hidden argument*/Enumerator_get_Current_m894E7226842A0AB920967095678A311EFF7C5737_RuntimeMethod_var);
			V_2 = L_9;
			// text += str + "\n";
			String_t* L_10 = V_0;
			String_t* L_11 = V_2;
			String_t* L_12 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923(L_10, L_11, _stringLiteralADC83B19E793491B1C6EA0FD8B46CD9F32E592FC, /*hidden argument*/NULL);
			V_0 = L_12;
		}

IL_005d:
		{
			// foreach (string str in logList) {
			bool L_13 = Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A((Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *)(&V_1), /*hidden argument*/Enumerator_MoveNext_m129741E497FB617DC9845CFEE4CB27B84C86301A_RuntimeMethod_var);
			if (L_13)
			{
				goto IL_0048;
			}
		}

IL_0066:
		{
			IL2CPP_LEAVE(0x76, FINALLY_0068);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0068;
	}

FINALLY_0068:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978((Enumerator_tBBAAE521602D26DCD42E467CF939632DC01EF813 *)(&V_1), /*hidden argument*/Enumerator_Dispose_mD9B1DB257A9F9A3CEA69542101B953689A4AD978_RuntimeMethod_var);
		IL2CPP_END_FINALLY(104)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(104)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x76, IL_0076)
	}

IL_0076:
	{
		// textMesh.text = text;
		TextMesh_t327D0DAFEF431170D8C2882083D442AF4D4A0E4A * L_14 = __this->get_textMesh_5();
		String_t* L_15 = V_0;
		NullCheck(L_14);
		TextMesh_set_text_m64242AB987CF285F432E7AED38F24FF855E9B220(L_14, L_15, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo__ctor_m8F4D851855681644B749EE73051E1BE06DEBAA1A (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo__ctor_m8F4D851855681644B749EE73051E1BE06DEBAA1A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private List<string> logList = new List<string>();
		List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * L_0 = (List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 *)il2cpp_codegen_object_new(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3_il2cpp_TypeInfo_var);
		List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06(L_0, /*hidden argument*/List_1__ctor_mDA22758D73530683C950C5CCF39BDB4E7E1F3F06_RuntimeMethod_var);
		__this->set_logList_6(L_0);
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_0(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4 (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddLog("OnHealthChanged");
		Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC(__this, _stringLiteral0EF80FE05747C3B8066F46572FA67164D400EB4C, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_1(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddLog("OnDamaged");
		Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC(__this, _stringLiteral4BD0DA8D8466F0DDA99BD8C7274F1A00E6D9E572, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_2(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54 (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddLog("OnDead");
		Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC(__this, _stringLiteral39BFCE4FC3E16D17E8BAECB76A0542032AE6460A, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_3(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2 (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddLog("OnHealed");
		Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC(__this, _stringLiteralEDCEFD93A3A438FA1701DBC9E39E86745E138B68, /*hidden argument*/NULL);
		// };
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_4(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA (Demo_t58FE0DDCC38E5C5031AF19F4FC72F2BBFB6EF0B6 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddLog("OnHealthMaxChanged");
		Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC(__this, _stringLiteral494C9D7BE9DFA92675F2FBB6C4B38C7F70C38F62, /*hidden argument*/NULL);
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_Bullet::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Bullet_Start_m45FDDAFBE031D873A015A127BEC073D35835C7EF (Demo_Bullet_t0CC46797722BD7E2889978D6B16C4B47FB3686F8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Bullet_Start_m45FDDAFBE031D873A015A127BEC073D35835C7EF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float bulletSpeed = 200f;
		V_0 = (200.0f);
		// GetComponent<Rigidbody2D>().velocity = transform.right * bulletSpeed;
		Rigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE * L_0 = Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tBDC6900A76D3C47E291446FF008D02B817C81CDE_mBF198078E908267FB6DA59F6242FC8F36FC06625_RuntimeMethod_var);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_right_mC32CE648E98D3D4F62F897A2751EE567C7C0CFB0(L_1, /*hidden argument*/NULL);
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_2, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_il2cpp_TypeInfo_var);
		Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  L_5 = Vector2_op_Implicit_mEA1F75961E3D368418BA8CEB9C40E55C25BA3C28(L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83(L_0, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Bullet::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Bullet_OnTriggerEnter2D_mB2A859E2D7299AC5A27C99BB607562B42E47996D (Demo_Bullet_t0CC46797722BD7E2889978D6B16C4B47FB3686F8 * __this, Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * ___collider2D0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Bullet_OnTriggerEnter2D_mB2A859E2D7299AC5A27C99BB607562B42E47996D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * V_0 = NULL;
	{
		// if (collider2D.TryGetComponent(out Demo_Zombie zombie)) {
		Collider2D_tD64BE58E48B95D89D349FEAB54D0FE2EEBF83379 * L_0 = ___collider2D0;
		NullCheck(L_0);
		bool L_1 = Component_TryGetComponent_TisDemo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91_mA657A59968DC92CF336739AB31064F190688A393(L_0, (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 **)(&V_0), /*hidden argument*/Component_TryGetComponent_TisDemo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91_mA657A59968DC92CF336739AB31064F190688A393_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		// zombie.Damage();
		Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * L_2 = V_0;
		NullCheck(L_2);
		Demo_Zombie_Damage_m3345441E19B8E4D2C9FEC05BC9EFEC83E7026BEC(L_2, /*hidden argument*/NULL);
		// Destroy(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_3, /*hidden argument*/NULL);
	}

IL_001b:
	{
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Bullet::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Bullet__ctor_m7EF3DF410C198246633CF2AF9F40817366B16C7F (Demo_Bullet_t0CC46797722BD7E2889978D6B16C4B47FB3686F8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_Cannon::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Cannon_Awake_mE9E6BBDE4A029D331FC9456B7ADAE31E75D0E3EB (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Cannon_Awake_mE9E6BBDE4A029D331FC9456B7ADAE31E75D0E3EB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// aimTransform = transform.Find("Aim");
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Transform_Find_m673797B6329C2669A543904532ABA1680DA4EAD1(L_0, _stringLiteralCF4F1FE5D0BA6E121C00FC814B0C56AE954A65BA, /*hidden argument*/NULL);
		__this->set_aimTransform_5(L_1);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Cannon::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Cannon_Update_mB0F52105ADCFE487F8E4EC304298B38F124A90A1 (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Cannon_Update_mB0F52105ADCFE487F8E4EC304298B38F124A90A1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// Vector3 aimDir = (GetMouseWorldPosition() - transform.position).normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Demo_Cannon_GetMouseWorldPosition_m491CA6AB5CA29173DE131343E03C88B2C62AA296(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_0, L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_4;
		// float angle = GetAngleFromVector(aimDir);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = V_0;
		float L_6 = Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0(__this, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		// aimTransform.eulerAngles = new Vector3(0, 0, angle);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = __this->get_aimTransform_5();
		float L_8 = V_1;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_9), (0.0f), (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_eulerAngles_m4B2B374C0B089A7ED0B522A3A4C56FA868992685(L_7, L_9, /*hidden argument*/NULL);
		// if (Input.GetMouseButtonDown(0)) {
		bool L_10 = Input_GetMouseButtonDown_m5AD76E22AA839706219AD86A4E0BE5276AF8E28A(0, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0071;
		}
	}
	{
		// Instantiate(pfBullet, transform.position, Quaternion.Euler(0, 0, angle));
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_11 = __this->get_pfBullet_4();
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_13 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_12, /*hidden argument*/NULL);
		float L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_15 = Quaternion_Euler_m537DD6CEAE0AD4274D8A84414C24C30730427D05((0.0f), (0.0f), L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0(L_11, L_13, L_15, /*hidden argument*/Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0_RuntimeMethod_var);
	}

IL_0071:
	{
		// }
		return;
	}
}
// UnityEngine.Vector3 CodeMonkey.HealthSystemCM.Demo_Cannon::GetMouseWorldPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Demo_Cannon_GetMouseWorldPosition_m491CA6AB5CA29173DE131343E03C88B2C62AA296 (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, const RuntimeMethod* method)
{
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 worldPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = Input_get_mousePosition_m1F6706785983B41FE8D5CBB81B5F15F68EBD9A53(/*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Camera_ScreenToWorldPoint_m179BB999DC97A251D0892B39C98F3FACDF0617C5(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		// worldPosition.z = 0f;
		(&V_0)->set_z_4((0.0f));
		// return worldPosition;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = V_0;
		return L_3;
	}
}
// System.Single CodeMonkey.HealthSystemCM.Demo_Cannon::GetAngleFromVector(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0 (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___dir0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// dir = dir.normalized;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_0 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&___dir0), /*hidden argument*/NULL);
		___dir0 = L_0;
		// float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1 = ___dir0;
		float L_2 = L_1.get_y_3();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = ___dir0;
		float L_4 = L_3.get_x_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_5 = atan2f(L_2, L_4);
		V_0 = ((float)il2cpp_codegen_multiply((float)L_5, (float)(57.29578f)));
		// if (n < 0) n += 360;
		float L_6 = V_0;
		if ((!(((float)L_6) < ((float)(0.0f)))))
		{
			goto IL_0031;
		}
	}
	{
		// if (n < 0) n += 360;
		float L_7 = V_0;
		V_0 = ((float)il2cpp_codegen_add((float)L_7, (float)(360.0f)));
	}

IL_0031:
	{
		// return n;
		float L_8 = V_0;
		return L_8;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Cannon::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Cannon__ctor_m545D77871238AB3C1408028FD122924492BE9F6F (Demo_Cannon_t456090E165C54F555F95058559544CD25714162A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_DamageButton_Start_m47D53331A12DCF11CB75D0FDBCAE47D3F7B7A6E3 (Demo_DamageButton_tA3803267184A803054306D09C2293BFC17FD719E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_DamageButton_Start_m47D53331A12DCF11CB75D0FDBCAE47D3F7B7A6E3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * L_0 = (U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass1_0__ctor_mA7AC968334D5CAF0948B61266BD426905B25CF94(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// HealthSystem.TryGetHealthSystem(getHealthSystemGameObject, out HealthSystem healthSystem, true);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = __this->get_getHealthSystemGameObject_4();
		U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * L_2 = V_0;
		NullCheck(L_2);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** L_3 = L_2->get_address_of_healthSystem_0();
		HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E(L_1, (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 **)L_3, (bool)1, /*hidden argument*/NULL);
		// GetComponent<Button>().onClick.AddListener(() => {
		//     healthSystem.Damage(10);
		// });
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_4 = Component_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m95916328C175634DC742DD13D032F149F1FBF58C(__this, /*hidden argument*/Component_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m95916328C175634DC742DD13D032F149F1FBF58C_RuntimeMethod_var);
		NullCheck(L_4);
		ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * L_5 = Button_get_onClick_m77E8CA6917881760CC7900930F4C789F3E2F8817_inline(L_4, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * L_6 = V_0;
		UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * L_7 = (UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 *)il2cpp_codegen_object_new(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4_il2cpp_TypeInfo_var);
		UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m731FBE36F3F0B2464BB7BDBC2584990068765100_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_AddListener_m31973FDDC5BB0B2828AB6EF519EC4FD6563499C9(L_5, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_DamageButton__ctor_m212BFD4B30DA6F1800CA9EABF6ACC0296897AB85 (Demo_DamageButton_tA3803267184A803054306D09C2293BFC17FD719E * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton_<>c__DisplayClass1_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0__ctor_mA7AC968334D5CAF0948B61266BD426905B25CF94 (U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton_<>c__DisplayClass1_0::<Start>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m731FBE36F3F0B2464BB7BDBC2584990068765100 (U3CU3Ec__DisplayClass1_0_t98C45A94424793ED1BCFF70306755328D287FFA6 * __this, const RuntimeMethod* method)
{
	{
		// healthSystem.Damage(10);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_0();
		NullCheck(L_0);
		HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765(L_0, (10.0f), /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_DefenseGame_Update_mBC671DA491634F848613D29079426AE6EC65C856 (Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B * __this, const RuntimeMethod* method)
{
	{
		// spawnTimer -= Time.deltaTime;
		float L_0 = __this->get_spawnTimer_5();
		float L_1 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		__this->set_spawnTimer_5(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// if (spawnTimer <= 0f) {
		float L_2 = __this->get_spawnTimer_5();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_0038;
		}
	}
	{
		// spawnTimer += spawnTimerMax;
		float L_3 = __this->get_spawnTimer_5();
		float L_4 = __this->get_spawnTimerMax_6();
		__this->set_spawnTimer_5(((float)il2cpp_codegen_add((float)L_3, (float)L_4)));
		// SpawnZombie();
		Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73(__this, /*hidden argument*/NULL);
	}

IL_0038:
	{
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::SpawnZombie()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73 (Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 spawnPosition = new Vector3(100, Random.Range(-25, +25), 0);
		int32_t L_0 = Random_Range_mD0C8F37FF3CAB1D87AAA6C45130BD59626BD6780(((int32_t)-25), ((int32_t)25), /*hidden argument*/NULL);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), (100.0f), (((float)((float)L_0))), (0.0f), /*hidden argument*/NULL);
		// Instantiate(pfZombie, spawnPosition, Quaternion.identity);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = __this->get_pfZombie_4();
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_il2cpp_TypeInfo_var);
		Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  L_3 = Quaternion_get_identity_m548B37D80F2DEE60E41D1F09BF6889B557BE1A64(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0(L_1, L_2, L_3, /*hidden argument*/Object_Instantiate_TisTransform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA_m8BCC51A95F42E99B9033022D8BF0EABA04FED8F0_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_DefenseGame__ctor_m784FCB1D862455D109914F4C1B316AB158575335 (Demo_DefenseGame_tF80702BBC47554419390D32ED2735470058B724B * __this, const RuntimeMethod* method)
{
	{
		// private float spawnTimerMax = 2.5f;
		__this->set_spawnTimerMax_6((2.5f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_HealButton::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_HealButton_Start_m728E0182CC8C7207A890A5568F24E583C8171DCF (Demo_HealButton_tFA151641791C07388AA4AAEAC99DAA88D127D779 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_HealButton_Start_m728E0182CC8C7207A890A5568F24E583C8171DCF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * L_0 = (U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass1_0__ctor_mDC1214DA7828992B6502E91C6F8FD01B01FEC47D(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// HealthSystem.TryGetHealthSystem(getHealthSystemGameObject, out HealthSystem healthSystem, true);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = __this->get_getHealthSystemGameObject_4();
		U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * L_2 = V_0;
		NullCheck(L_2);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** L_3 = L_2->get_address_of_healthSystem_0();
		HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E(L_1, (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 **)L_3, (bool)1, /*hidden argument*/NULL);
		// GetComponent<Button>().onClick.AddListener(() => {
		//     healthSystem.Heal(10);
		// });
		Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * L_4 = Component_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m95916328C175634DC742DD13D032F149F1FBF58C(__this, /*hidden argument*/Component_GetComponent_TisButton_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B_m95916328C175634DC742DD13D032F149F1FBF58C_RuntimeMethod_var);
		NullCheck(L_4);
		ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * L_5 = Button_get_onClick_m77E8CA6917881760CC7900930F4C789F3E2F8817_inline(L_4, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * L_6 = V_0;
		UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 * L_7 = (UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4 *)il2cpp_codegen_object_new(UnityAction_tD19B26F1B2C048E38FD5801A33573BE01064CAF4_il2cpp_TypeInfo_var);
		UnityAction__ctor_mEFC4B92529CE83DF72501F92E07EC5598C54BDAC(L_7, L_6, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m3B3616F62C011EB9A03B855AF972712288A6CEF2_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		UnityEvent_AddListener_m31973FDDC5BB0B2828AB6EF519EC4FD6563499C9(L_5, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_HealButton::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_HealButton__ctor_m791CBDB7BD6D3957564098E070DAB73A3BE13767 (Demo_HealButton_tFA151641791C07388AA4AAEAC99DAA88D127D779 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_HealButton_<>c__DisplayClass1_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0__ctor_mDC1214DA7828992B6502E91C6F8FD01B01FEC47D (U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_HealButton_<>c__DisplayClass1_0::<Start>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m3B3616F62C011EB9A03B855AF972712288A6CEF2 (U3CU3Ec__DisplayClass1_0_tDD5B92A039A637849C04D1F694E962D20390EA7D * __this, const RuntimeMethod* method)
{
	{
		// healthSystem.Heal(10);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_0();
		NullCheck(L_0);
		HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC(L_0, (10.0f), /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_Unit::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Unit_Start_m2AA94BF459E5ED88B019B66C3AA97EE9940E9E2C (Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Unit_Start_m2AA94BF459E5ED88B019B66C3AA97EE9940E9E2C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthSystem = GetComponent<HealthSystemComponent>().GetHealthSystem();
		HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * L_0 = Component_GetComponent_TisHealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6_m6926C7925E795638A562C47471C5FF35D8DDED23(__this, /*hidden argument*/Component_GetComponent_TisHealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6_m6926C7925E795638A562C47471C5FF35D8DDED23_RuntimeMethod_var);
		NullCheck(L_0);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = HealthSystemComponent_GetHealthSystem_mA7F8A5C583C2C0ACC401F49F83FDFDE6164AB832_inline(L_0, /*hidden argument*/NULL);
		__this->set_healthSystem_6(L_1);
		// healthSystem.OnDead += HealthSystem_OnDead;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_2 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_3, __this, (intptr_t)((intptr_t)Demo_Unit_HealthSystem_OnDead_mCAF5A68EF417E026DF0CE10006661A22AE35CF27_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_2);
		HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52(L_2, L_3, /*hidden argument*/NULL);
		// healthSystem.OnDamaged += HealthSystem_OnDamaged;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_4 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_5, __this, (intptr_t)((intptr_t)Demo_Unit_HealthSystem_OnDamaged_mDFE694CC2C14414A79608E6455C85604F87DE4AF_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089(L_4, L_5, /*hidden argument*/NULL);
		// healthSystem.OnHealed += HealthSystem_OnHealed;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_6 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_7, __this, (intptr_t)((intptr_t)Demo_Unit_HealthSystem_OnHealed_mB19EBDE076B2174AE17EEE736D2B271E7A1EA67A_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_6);
		HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10(L_6, L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Unit::HealthSystem_OnHealed(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Unit_HealthSystem_OnHealed_mB19EBDE076B2174AE17EEE736D2B271E7A1EA67A (Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// healParticleSystem.Play();
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = __this->get_healParticleSystem_5();
		NullCheck(L_0);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Unit::HealthSystem_OnDamaged(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Unit_HealthSystem_OnDamaged_mDFE694CC2C14414A79608E6455C85604F87DE4AF (Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// damageParticleSystem.Play();
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = __this->get_damageParticleSystem_4();
		NullCheck(L_0);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Unit::HealthSystem_OnDead(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Unit_HealthSystem_OnDead_mCAF5A68EF417E026DF0CE10006661A22AE35CF27 (Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// transform.eulerAngles = new Vector3(0, 0, -90);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_1), (0.0f), (0.0f), (-90.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_eulerAngles_m4B2B374C0B089A7ED0B522A3A4C56FA868992685(L_0, L_1, /*hidden argument*/NULL);
		// damageParticleSystem.Play();
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_2 = __this->get_damageParticleSystem_4();
		NullCheck(L_2);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Unit::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Unit_OnMouseDown_mA11A003B4AC502FDA27ACB0D3DF461C20B5AAC33 (Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02 * __this, const RuntimeMethod* method)
{
	{
		// healthSystem.Damage(10);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		NullCheck(L_0);
		HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765(L_0, (10.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Unit::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Unit__ctor_m3AAC26B169EB43C33659F5B13124F05FADF72E43 (Demo_Unit_t45A652E1A6B98A70ED25E3159398300C3F1F8B02 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_UnitCSharp_Awake_m38AEE7D1F5B4E9BC4678A54C0DFAF9C68F8F7546 (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_UnitCSharp_Awake_m38AEE7D1F5B4E9BC4678A54C0DFAF9C68F8F7546_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthSystem = new HealthSystem(100);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 *)il2cpp_codegen_object_new(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3_il2cpp_TypeInfo_var);
		HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7(L_0, (100.0f), /*hidden argument*/NULL);
		__this->set_healthSystem_6(L_0);
		// healthSystem.OnDead += HealthSystem_OnDead;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_2, __this, (intptr_t)((intptr_t)Demo_UnitCSharp_HealthSystem_OnDead_mB8FCC204EB6B40020CC7478703A6FD8020558958_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52(L_1, L_2, /*hidden argument*/NULL);
		// healthSystem.OnDamaged += HealthSystem_OnDamaged;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_3 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_4, __this, (intptr_t)((intptr_t)Demo_UnitCSharp_HealthSystem_OnDamaged_mA11ED92EC84BF522A87597E476EAB03CE414250D_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_3);
		HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089(L_3, L_4, /*hidden argument*/NULL);
		// healthSystem.OnHealed += HealthSystem_OnHealed;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_5 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_6, __this, (intptr_t)((intptr_t)Demo_UnitCSharp_HealthSystem_OnHealed_mC11861945ACBBF6535571F527070A432DB84AC12_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10(L_5, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::HealthSystem_OnHealed(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_UnitCSharp_HealthSystem_OnHealed_mC11861945ACBBF6535571F527070A432DB84AC12 (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// healParticleSystem.Play();
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = __this->get_healParticleSystem_5();
		NullCheck(L_0);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::HealthSystem_OnDamaged(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_UnitCSharp_HealthSystem_OnDamaged_mA11ED92EC84BF522A87597E476EAB03CE414250D (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// damageParticleSystem.Play();
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_0 = __this->get_damageParticleSystem_4();
		NullCheck(L_0);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::HealthSystem_OnDead(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_UnitCSharp_HealthSystem_OnDead_mB8FCC204EB6B40020CC7478703A6FD8020558958 (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// transform.eulerAngles = new Vector3(0, 0, -90);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_1), (0.0f), (0.0f), (-90.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_eulerAngles_m4B2B374C0B089A7ED0B522A3A4C56FA868992685(L_0, L_1, /*hidden argument*/NULL);
		// damageParticleSystem.Play();
		ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * L_2 = __this->get_damageParticleSystem_4();
		NullCheck(L_2);
		ParticleSystem_Play_m5BC5E6B56FCF639CAD5DF41B51DC05A0B444212F(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::OnMouseDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_UnitCSharp_OnMouseDown_m45885A7922A6D1D08FB9392F37F2DF5E28875680 (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, const RuntimeMethod* method)
{
	{
		// healthSystem.Damage(10);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		NullCheck(L_0);
		HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765(L_0, (10.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_UnitCSharp::GetHealthSystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * Demo_UnitCSharp_GetHealthSystem_mBA0D8FA00B80F29EC137D45574FDBFE9F614140B (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, const RuntimeMethod* method)
{
	{
		// return healthSystem;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		return L_0;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_UnitCSharp__ctor_m23E23867AD0B2C5DE5FF866AA6994695FA3190CC (Demo_UnitCSharp_t3FC1CE084A95ACE6CC697F93BE0D6E5EE1DB8D4C * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Zombie_Awake_m948FA4A81F17E21AAC1C1AD509BD3A089671A03F (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Zombie_Awake_m948FA4A81F17E21AAC1C1AD509BD3A089671A03F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthSystem = new HealthSystem(100);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 *)il2cpp_codegen_object_new(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3_il2cpp_TypeInfo_var);
		HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7(L_0, (100.0f), /*hidden argument*/NULL);
		__this->set_healthSystem_4(L_0);
		// healthSystem.OnDead += HealthSystem_OnDead;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = __this->get_healthSystem_4();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_2, __this, (intptr_t)((intptr_t)Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Zombie_Update_m382273970FD8253126D44E8A35EFE0CA2D5E4D7E (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Zombie_Update_m382273970FD8253126D44E8A35EFE0CA2D5E4D7E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		// Vector3 moveDir = new Vector3(-1, 0, 0);
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_0), (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		// float speed = 10f;
		V_1 = (10.0f);
		// transform.position += moveDir * speed * Time.deltaTime;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_0 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = L_0;
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_3 = V_0;
		float L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_3, L_4, /*hidden argument*/NULL);
		float L_6 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_7 = Vector3_op_Multiply_m1C5F07723615156ACF035D88A1280A9E8F35A04E(L_5, L_6, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_8 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_2, L_7, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_mDA89E4893F14ECA5CBEEE7FB80A5BF7C1B8EA6DC(L_1, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::HealthSystem_OnDead(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353 (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Destroy(gameObject);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Damage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Zombie_Damage_m3345441E19B8E4D2C9FEC05BC9EFEC83E7026BEC (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, const RuntimeMethod* method)
{
	{
		// healthSystem.Damage(60);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_4();
		NullCheck(L_0);
		HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765(L_0, (60.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_Zombie::GetHealthSystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * Demo_Zombie_GetHealthSystem_m0DFB5D123D48C9C579CFEF47F4DC8712F0657C21 (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, const RuntimeMethod* method)
{
	{
		// return healthSystem;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_4();
		return L_0;
	}
}
// System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Demo_Zombie__ctor_mE25DB1066650792908019F9C1B06CADB55C9335C (Demo_Zombie_t582D5E7E8F37D5E847FBA79255D06F04130B1C91 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_Start_m513114536DBB0AD94D9AB686C586C4E39BFF70EA (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, const RuntimeMethod* method)
{
	HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * V_0 = NULL;
	{
		// if (HealthSystem.TryGetHealthSystem(getHealthSystemGameObject, out HealthSystem healthSystem)) {
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_getHealthSystemGameObject_4();
		bool L_1 = HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E(L_0, (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 **)(&V_0), (bool)0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		// SetHealthSystem(healthSystem);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_2 = V_0;
		HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC(__this, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::SetHealthSystem(CodeMonkey.HealthSystemCM.HealthSystem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * ___healthSystem0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (this.healthSystem != null) {
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		// this.healthSystem.OnHealthChanged -= HealthSystem_OnHealthChanged;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_2, __this, (intptr_t)((intptr_t)HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_1);
		HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF(L_1, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		// this.healthSystem = healthSystem;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_3 = ___healthSystem0;
		__this->set_healthSystem_6(L_3);
		// UpdateHealthBar();
		HealthBarUI_UpdateHealthBar_m0AD4E403485C4F870845738CA8F717DC137E59B5(__this, /*hidden argument*/NULL);
		// healthSystem.OnHealthChanged += HealthSystem_OnHealthChanged;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_4 = ___healthSystem0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_5, __this, (intptr_t)((intptr_t)HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_4);
		HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34(L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::HealthSystem_OnHealthChanged(System.Object,System.EventArgs)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, RuntimeObject * ___sender0, EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___e1, const RuntimeMethod* method)
{
	{
		// UpdateHealthBar();
		HealthBarUI_UpdateHealthBar_m0AD4E403485C4F870845738CA8F717DC137E59B5(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::UpdateHealthBar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_UpdateHealthBar_m0AD4E403485C4F870845738CA8F717DC137E59B5 (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, const RuntimeMethod* method)
{
	{
		// image.fillAmount = healthSystem.GetHealthNormalized();
		Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * L_0 = __this->get_image_5();
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = __this->get_healthSystem_6();
		NullCheck(L_1);
		float L_2 = HealthSystem_GetHealthNormalized_mB1FEDFABE4083B167BCE7A3A637D41DB996DF9E8(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Image_set_fillAmount_m3939CACF382B30A0353F300E2AE2D3823E1D42F4(L_0, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI_OnDestroy_m780AFFA696943D26015DE5582FC0E17E192DDBE9 (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthBarUI_OnDestroy_m780AFFA696943D26015DE5582FC0E17E192DDBE9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthSystem.OnHealthChanged -= HealthSystem_OnHealthChanged;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = (EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)il2cpp_codegen_object_new(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var);
		EventHandler__ctor_m497A2BCD46DB769EAFFDE919303FCAE226906B6F(L_1, __this, (intptr_t)((intptr_t)HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthBarUI::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBarUI__ctor_m42C03777A895847C16018B870A84E142C6361A0F (HealthBarUI_t50F7F9C28E26793C5F83DE512CC409AE74684DB4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealthChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnHealthChanged_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealthChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnHealthChanged_0();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealthMaxChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnHealthMaxChanged_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnHealthMaxChanged_1();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealthMaxChanged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_remove_OnHealthMaxChanged_mBF597E0E3EF51715FCD35D117B18E9895C473C89 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_remove_OnHealthMaxChanged_mBF597E0E3EF51715FCD35D117B18E9895C473C89_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnHealthMaxChanged_1();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnHealthMaxChanged_1();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnDamaged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnDamaged_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnDamaged_2();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnDamaged(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_remove_OnDamaged_m3B7CC3942A6A596520B226EB032F3841737A7674 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_remove_OnDamaged_m3B7CC3942A6A596520B226EB032F3841737A7674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnDamaged_2();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnDamaged_2();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnHealed_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnHealed_3();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealed(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_remove_OnHealed_m5FC7901576A801F1C61EA47C70778D1993DC24D5 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_remove_OnHealed_m5FC7901576A801F1C61EA47C70778D1993DC24D5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnHealed_3();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnHealed_3();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnDead(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnDead_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnDead_4();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnDead(System.EventHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_remove_OnDead_m28D623D97CEF97A8F418ECB1EE8C67032553D7B7 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_remove_OnDead_m28D623D97CEF97A8F418ECB1EE8C67032553D7B7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_1 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * V_2 = NULL;
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnDead_4();
		V_0 = L_0;
	}

IL_0007:
	{
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = V_0;
		V_1 = L_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = ___value0;
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)CastclassSealed((RuntimeObject*)L_4, EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C_il2cpp_TypeInfo_var));
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C ** L_5 = __this->get_address_of_OnDead_4();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = V_2;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = V_1;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = InterlockedCompareExchangeImpl<EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *>((EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C **)L_5, L_6, L_7);
		V_0 = L_8;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = V_0;
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_10 = V_1;
		if ((!(((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_9) == ((RuntimeObject*)(EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C *)L_10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___healthMax0, const RuntimeMethod* method)
{
	{
		// public HealthSystem(float healthMax) {
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		// this.healthMax = healthMax;
		float L_0 = ___healthMax0;
		__this->set_healthMax_5(L_0);
		// health = healthMax;
		float L_1 = ___healthMax0;
		__this->set_health_6(L_1);
		// }
		return;
	}
}
// System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HealthSystem_GetHealth_mC367CA7B427DC204C7A5D3CD2B9DFCFE642E3A3A (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method)
{
	{
		// return health;
		float L_0 = __this->get_health_6();
		return L_0;
	}
}
// System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealthMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HealthSystem_GetHealthMax_m6B963D43FF565740A2AC9DE2A55D44806E22CC31 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method)
{
	{
		// return healthMax;
		float L_0 = __this->get_healthMax_5();
		return L_0;
	}
}
// System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealthNormalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float HealthSystem_GetHealthNormalized_mB1FEDFABE4083B167BCE7A3A637D41DB996DF9E8 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method)
{
	{
		// return health / healthMax;
		float L_0 = __this->get_health_6();
		float L_1 = __this->get_healthMax_5();
		return ((float)((float)L_0/(float)L_1));
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::Damage(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___amount0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B4_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B3_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B7_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B6_0 = NULL;
	{
		// health -= amount;
		float L_0 = __this->get_health_6();
		float L_1 = ___amount0;
		__this->set_health_6(((float)il2cpp_codegen_subtract((float)L_0, (float)L_1)));
		// if (health < 0) {
		float L_2 = __this->get_health_6();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0026;
		}
	}
	{
		// health = 0;
		__this->set_health_6((0.0f));
	}

IL_0026:
	{
		// OnHealthChanged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = __this->get_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_0032;
		}
	}
	{
		goto IL_003d;
	}

IL_0032:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_5 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B4_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B4_0, __this, L_5, /*hidden argument*/NULL);
	}

IL_003d:
	{
		// OnDamaged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = __this->get_OnDamaged_2();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = L_6;
		G_B6_0 = L_7;
		if (L_7)
		{
			G_B7_0 = L_7;
			goto IL_0049;
		}
	}
	{
		goto IL_0054;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_8 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B7_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B7_0, __this, L_8, /*hidden argument*/NULL);
	}

IL_0054:
	{
		// if (health <= 0) {
		float L_9 = __this->get_health_6();
		if ((!(((float)L_9) <= ((float)(0.0f)))))
		{
			goto IL_0067;
		}
	}
	{
		// Die();
		HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509(__this, /*hidden argument*/NULL);
	}

IL_0067:
	{
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::Die()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B2_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B1_0 = NULL;
	{
		// OnDead?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_0 = __this->get_OnDead_4();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_2 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B2_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B2_0, __this, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean CodeMonkey.HealthSystemCM.HealthSystem::IsDead()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HealthSystem_IsDead_m5B147E5D54C2F4604E5F1E3171EE862B3E916EC3 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method)
{
	{
		// return health <= 0;
		float L_0 = __this->get_health_6();
		return (bool)((((int32_t)((!(((float)L_0) <= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::Heal(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___amount0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B4_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B3_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B7_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B6_0 = NULL;
	{
		// health += amount;
		float L_0 = __this->get_health_6();
		float L_1 = ___amount0;
		__this->set_health_6(((float)il2cpp_codegen_add((float)L_0, (float)L_1)));
		// if (health > healthMax) {
		float L_2 = __this->get_health_6();
		float L_3 = __this->get_healthMax_5();
		if ((!(((float)L_2) > ((float)L_3))))
		{
			goto IL_0028;
		}
	}
	{
		// health = healthMax;
		float L_4 = __this->get_healthMax_5();
		__this->set_health_6(L_4);
	}

IL_0028:
	{
		// OnHealthChanged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = __this->get_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = L_5;
		G_B3_0 = L_6;
		if (L_6)
		{
			G_B4_0 = L_6;
			goto IL_0034;
		}
	}
	{
		goto IL_003f;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_7 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B4_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B4_0, __this, L_7, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// OnHealed?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_8 = __this->get_OnHealed_3();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_9 = L_8;
		G_B6_0 = L_9;
		if (L_9)
		{
			G_B7_0 = L_9;
			goto IL_004a;
		}
	}
	{
		return;
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_10 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B7_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B7_0, __this, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::HealComplete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_HealComplete_m98E89B04DD881539AA4EEBB44399A7F06AE58958 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_HealComplete_m98E89B04DD881539AA4EEBB44399A7F06AE58958_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B2_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B1_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B5_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B4_0 = NULL;
	{
		// health = healthMax;
		float L_0 = __this->get_healthMax_5();
		__this->set_health_6(L_0);
		// OnHealthChanged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_1 = __this->get_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0018;
		}
	}
	{
		goto IL_0023;
	}

IL_0018:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_3 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B2_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B2_0, __this, L_3, /*hidden argument*/NULL);
	}

IL_0023:
	{
		// OnHealed?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = __this->get_OnHealed_3();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = L_4;
		G_B4_0 = L_5;
		if (L_5)
		{
			G_B5_0 = L_5;
			goto IL_002e;
		}
	}
	{
		return;
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_6 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B5_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B5_0, __this, L_6, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::SetHealthMax(System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_SetHealthMax_m34E7B7BC73D4367C0055A61AD1B21BA56FF5565B (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___healthMax0, bool ___fullHealth1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_SetHealthMax_m34E7B7BC73D4367C0055A61AD1B21BA56FF5565B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B4_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B3_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B7_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B6_0 = NULL;
	{
		// this.healthMax = healthMax;
		float L_0 = ___healthMax0;
		__this->set_healthMax_5(L_0);
		// if (fullHealth) health = healthMax;
		bool L_1 = ___fullHealth1;
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		// if (fullHealth) health = healthMax;
		float L_2 = ___healthMax0;
		__this->set_health_6(L_2);
	}

IL_0011:
	{
		// OnHealthMaxChanged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_3 = __this->get_OnHealthMaxChanged_1();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_001d;
		}
	}
	{
		goto IL_0028;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_5 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B4_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B4_0, __this, L_5, /*hidden argument*/NULL);
	}

IL_0028:
	{
		// OnHealthChanged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = __this->get_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_7 = L_6;
		G_B6_0 = L_7;
		if (L_7)
		{
			G_B7_0 = L_7;
			goto IL_0033;
		}
	}
	{
		return;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_8 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B7_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B7_0, __this, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystem::SetHealth(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809 (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * __this, float ___health0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B6_0 = NULL;
	EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * G_B5_0 = NULL;
	{
		// if (health > healthMax) {
		float L_0 = ___health0;
		float L_1 = __this->get_healthMax_5();
		if ((!(((float)L_0) > ((float)L_1))))
		{
			goto IL_0011;
		}
	}
	{
		// health = healthMax;
		float L_2 = __this->get_healthMax_5();
		___health0 = L_2;
	}

IL_0011:
	{
		// if (health < 0) {
		float L_3 = ___health0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0020;
		}
	}
	{
		// health = 0;
		___health0 = (0.0f);
	}

IL_0020:
	{
		// this.health = health;
		float L_4 = ___health0;
		__this->set_health_6(L_4);
		// OnHealthChanged?.Invoke(this, EventArgs.Empty);
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_5 = __this->get_OnHealthChanged_0();
		EventHandler_t2B84E745E28BA26C49C4E99A387FC3B534D1110C * L_6 = L_5;
		G_B5_0 = L_6;
		if (L_6)
		{
			G_B6_0 = L_6;
			goto IL_0033;
		}
	}
	{
		goto IL_003e;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var);
		EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * L_7 = ((EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields*)il2cpp_codegen_static_fields_for(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_il2cpp_TypeInfo_var))->get_Empty_0();
		NullCheck(G_B6_0);
		EventHandler_Invoke_mD23D5EFEA562A05C5EACDD3E91EEDD2BF6C22800(G_B6_0, __this, L_7, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// if (health <= 0) {
		float L_8 = ___health0;
		if ((!(((float)L_8) <= ((float)(0.0f)))))
		{
			goto IL_004c;
		}
	}
	{
		// Die();
		HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509(__this, /*hidden argument*/NULL);
	}

IL_004c:
	{
		// }
		return;
	}
}
// System.Boolean CodeMonkey.HealthSystemCM.HealthSystem::TryGetHealthSystem(UnityEngine.GameObject,CodeMonkey.HealthSystemCM.HealthSystem&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___getHealthSystemGameObject0, HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** ___healthSystem1, bool ___logErrors2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		// healthSystem = null;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** L_0 = ___healthSystem1;
		*((RuntimeObject **)L_0) = (RuntimeObject *)NULL;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_0, (void*)(RuntimeObject *)NULL);
		// if (getHealthSystemGameObject != null) {
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = ___getHealthSystemGameObject0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_1, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0048;
		}
	}
	{
		// if (getHealthSystemGameObject.TryGetComponent(out IGetHealthSystem getHealthSystem)) {
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = ___getHealthSystemGameObject0;
		NullCheck(L_3);
		bool L_4 = GameObject_TryGetComponent_TisIGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1_mF2CEAD5C764E76D26ECC9C4AC14F9B60C269E79A(L_3, (RuntimeObject**)(&V_0), /*hidden argument*/GameObject_TryGetComponent_TisIGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1_mF2CEAD5C764E76D26ECC9C4AC14F9B60C269E79A_RuntimeMethod_var);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		// healthSystem = getHealthSystem.GetHealthSystem();
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** L_5 = ___healthSystem1;
		RuntimeObject* L_6 = V_0;
		NullCheck(L_6);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_7 = InterfaceFuncInvoker0< HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * >::Invoke(0 /* CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.IGetHealthSystem::GetHealthSystem() */, IGetHealthSystem_t1560282EFE58F716FC7EBE90F03C269923528AB1_il2cpp_TypeInfo_var, L_6);
		*((RuntimeObject **)L_5) = (RuntimeObject *)L_7;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_5, (void*)(RuntimeObject *)L_7);
		// if (healthSystem != null) {
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 ** L_8 = ___healthSystem1;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_9 = *((HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 **)L_8);
		if (!L_9)
		{
			goto IL_0024;
		}
	}
	{
		// return true;
		return (bool)1;
	}

IL_0024:
	{
		// if (logErrors) {
		bool L_10 = ___logErrors2;
		if (!L_10)
		{
			goto IL_0031;
		}
	}
	{
		// Debug.LogError($"Got HealthSystem from object but healthSystem is null! Should it have been created? Maybe you have an issue with the order of operations.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral4293DE8BAA84BEFF979E1C0FF1D9F6E939AF64F3, /*hidden argument*/NULL);
	}

IL_0031:
	{
		// return false;
		return (bool)0;
	}

IL_0033:
	{
		// if (logErrors) {
		bool L_11 = ___logErrors2;
		if (!L_11)
		{
			goto IL_0046;
		}
	}
	{
		// Debug.LogError($"Referenced Game Object '{getHealthSystemGameObject}' does not have a script that implements IGetHealthSystem!");
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_12 = ___getHealthSystemGameObject0;
		String_t* L_13 = String_Format_m0ACDD8B34764E4040AED0B3EEB753567E4576BFA(_stringLiteral829807EF52C7B8846BD67A635F78561AF68316A9, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(L_13, /*hidden argument*/NULL);
	}

IL_0046:
	{
		// return false;
		return (bool)0;
	}

IL_0048:
	{
		// if (logErrors) {
		bool L_14 = ___logErrors2;
		if (!L_14)
		{
			goto IL_0055;
		}
	}
	{
		// Debug.LogError($"You need to assign the field 'getHealthSystemGameObject'!");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteralA1D28A508954C4E8248BBBDF385E3FB855AB6A7A, /*hidden argument*/NULL);
	}

IL_0055:
	{
		// return false;
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.HealthSystemComponent::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystemComponent_Awake_m70FC15C7F1FB8E777D58EBE37E3E6C688E8BA0FB (HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (HealthSystemComponent_Awake_m70FC15C7F1FB8E777D58EBE37E3E6C688E8BA0FB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// healthSystem = new HealthSystem(healthAmountMax);
		float L_0 = __this->get_healthAmountMax_4();
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_1 = (HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 *)il2cpp_codegen_object_new(HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3_il2cpp_TypeInfo_var);
		HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7(L_1, L_0, /*hidden argument*/NULL);
		__this->set_healthSystem_6(L_1);
		// if (startingHealthAmount != 0) {
		float L_2 = __this->get_startingHealthAmount_5();
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_002f;
		}
	}
	{
		// healthSystem.SetHealth(startingHealthAmount);
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_3 = __this->get_healthSystem_6();
		float L_4 = __this->get_startingHealthAmount_5();
		NullCheck(L_3);
		HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809(L_3, L_4, /*hidden argument*/NULL);
	}

IL_002f:
	{
		// }
		return;
	}
}
// CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.HealthSystemComponent::GetHealthSystem()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * HealthSystemComponent_GetHealthSystem_mA7F8A5C583C2C0ACC401F49F83FDFDE6164AB832 (HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * __this, const RuntimeMethod* method)
{
	{
		// return healthSystem;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		return L_0;
	}
}
// System.Void CodeMonkey.HealthSystemCM.HealthSystemComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthSystemComponent__ctor_m8FF62368C9DB0711E1AFD7BF1B7EA4725F5EB045 (HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * __this, const RuntimeMethod* method)
{
	{
		// [SerializeField] private float healthAmountMax = 100f;
		__this->set_healthAmountMax_4((100.0f));
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.LookAtCamera::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LookAtCamera_Awake_m33AF9BDAF449B93FFF46E011B03C7C5A20E0240A (LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0 * __this, const RuntimeMethod* method)
{
	{
		// mainCameraTransform = Camera.main.transform;
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_0 = Camera_get_main_m9256A9F84F92D7ED73F3E6C4E2694030AD8B61FA(/*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(L_0, /*hidden argument*/NULL);
		__this->set_mainCameraTransform_5(L_1);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.LookAtCamera::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LookAtCamera_Update_m7886FFAC880BF8580109F144D59DC0A13DAC1656 (LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0 * __this, const RuntimeMethod* method)
{
	{
		// LookAt();
		LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.LookAtCamera::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LookAtCamera_OnEnable_m59043E0F4A861408046D7C32588C1D6EC75E0172 (LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0 * __this, const RuntimeMethod* method)
{
	{
		// LookAt();
		LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.LookAtCamera::LookAt()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776 (LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// if (invert) {
		bool L_0 = __this->get_invert_4();
		if (!L_0)
		{
			goto IL_0049;
		}
	}
	{
		// Vector3 dir = (transform.position - mainCameraTransform.position).normalized;
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_1 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_2 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_1, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = __this->get_mainCameraTransform_5();
		NullCheck(L_3);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_5 = Vector3_op_Subtraction_mF9846B723A5034F8B9F5F5DCB78E3D67649143D3(L_2, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_6 = Vector3_get_normalized_mE20796F1D2D36244FACD4D14DADB245BE579849B((Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 *)(&V_1), /*hidden argument*/NULL);
		V_0 = L_6;
		// transform.LookAt(transform.position + dir);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_7 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_8 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_9 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_8, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_10 = V_0;
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_11 = Vector3_op_Addition_m929F9C17E5D11B94D50B4AFF1D730B70CB59B50E(L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_LookAt_m3EC94482B2585FE03AEEDF90325A1F0B9A84960E(L_7, L_11, /*hidden argument*/NULL);
		// } else {
		return;
	}

IL_0049:
	{
		// transform.LookAt(mainCameraTransform.position);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_12 = Component_get_transform_m00F05BD782F920C301A7EBA480F3B7A904C07EC9(__this, /*hidden argument*/NULL);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_13 = __this->get_mainCameraTransform_5();
		NullCheck(L_13);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_14 = Transform_get_position_mF54C3A064F7C8E24F1C56EE128728B2E4485E294(L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_LookAt_m3EC94482B2585FE03AEEDF90325A1F0B9A84960E(L_12, L_14, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CodeMonkey.HealthSystemCM.LookAtCamera::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LookAtCamera__ctor_mE0028225B1C36A59A91F14B945FEBC8A4BE2C1CF (LookAtCamera_t6CF5422F38AD9C901DCA539A73C5E52576594FF0 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Readme_HealthSystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Readme_HealthSystem__ctor_mEA835B58F53ED1F09A87EAB7638A9BFAA2748654 (Readme_HealthSystem_t804424AD260C195064D734E5F33C7124EB8E612C * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m6E2B3821A4A361556FC12E9B1C71E1D5DC002C5B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Section__ctor_mBA25DBE826C89953A5AD944CD379A5E9888B3E87 (Section_t82A1B7132E0C8C9A3450E84B3C34075733E0AB89 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * Button_get_onClick_m77E8CA6917881760CC7900930F4C789F3E2F8817_inline (Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * __this, const RuntimeMethod* method)
{
	{
		// get { return m_OnClick; }
		ButtonClickedEvent_t975D9C903BC4880557ADD7D3ACFB01CB2B3D6DDB * L_0 = __this->get_m_OnClick_20();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * HealthSystemComponent_GetHealthSystem_mA7F8A5C583C2C0ACC401F49F83FDFDE6164AB832_inline (HealthSystemComponent_t72CC14230115D93EA2F071B83C3383F2D20D68C6 * __this, const RuntimeMethod* method)
{
	{
		// return healthSystem;
		HealthSystem_t809E97221C82C33366829FDC916916B36119E2F3 * L_0 = __this->get_healthSystem_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared_inline (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_mD7829C7E8CFBEDD463B15A951CDE9B90A12CC55C_gshared_inline (Enumerator_tE0C99528D3DCE5863566CE37BD94162A4C0431CD * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
