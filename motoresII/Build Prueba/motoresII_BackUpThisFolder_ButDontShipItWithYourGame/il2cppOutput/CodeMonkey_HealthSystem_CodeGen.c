﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void CodeMonkey.HealthSystemCM.Demo::Awake()
extern void Demo_Awake_m2884174F2B16323136857B6BBC9DB1C342610763 (void);
// 0x00000002 System.Void CodeMonkey.HealthSystemCM.Demo::Start()
extern void Demo_Start_m3989B457771469E3BBCC14F6551F69808E0E2341 (void);
// 0x00000003 System.Void CodeMonkey.HealthSystemCM.Demo::AddLog(System.String)
extern void Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC (void);
// 0x00000004 System.Void CodeMonkey.HealthSystemCM.Demo::.ctor()
extern void Demo__ctor_m8F4D851855681644B749EE73051E1BE06DEBAA1A (void);
// 0x00000005 System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_0(System.Object,System.EventArgs)
extern void Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4 (void);
// 0x00000006 System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_1(System.Object,System.EventArgs)
extern void Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B (void);
// 0x00000007 System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_2(System.Object,System.EventArgs)
extern void Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54 (void);
// 0x00000008 System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_3(System.Object,System.EventArgs)
extern void Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2 (void);
// 0x00000009 System.Void CodeMonkey.HealthSystemCM.Demo::<Start>b__4_4(System.Object,System.EventArgs)
extern void Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA (void);
// 0x0000000A System.Void CodeMonkey.HealthSystemCM.Demo_Bullet::Start()
extern void Demo_Bullet_Start_m45FDDAFBE031D873A015A127BEC073D35835C7EF (void);
// 0x0000000B System.Void CodeMonkey.HealthSystemCM.Demo_Bullet::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Demo_Bullet_OnTriggerEnter2D_mB2A859E2D7299AC5A27C99BB607562B42E47996D (void);
// 0x0000000C System.Void CodeMonkey.HealthSystemCM.Demo_Bullet::.ctor()
extern void Demo_Bullet__ctor_m7EF3DF410C198246633CF2AF9F40817366B16C7F (void);
// 0x0000000D System.Void CodeMonkey.HealthSystemCM.Demo_Cannon::Awake()
extern void Demo_Cannon_Awake_mE9E6BBDE4A029D331FC9456B7ADAE31E75D0E3EB (void);
// 0x0000000E System.Void CodeMonkey.HealthSystemCM.Demo_Cannon::Update()
extern void Demo_Cannon_Update_mB0F52105ADCFE487F8E4EC304298B38F124A90A1 (void);
// 0x0000000F UnityEngine.Vector3 CodeMonkey.HealthSystemCM.Demo_Cannon::GetMouseWorldPosition()
extern void Demo_Cannon_GetMouseWorldPosition_m491CA6AB5CA29173DE131343E03C88B2C62AA296 (void);
// 0x00000010 System.Single CodeMonkey.HealthSystemCM.Demo_Cannon::GetAngleFromVector(UnityEngine.Vector3)
extern void Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0 (void);
// 0x00000011 System.Void CodeMonkey.HealthSystemCM.Demo_Cannon::.ctor()
extern void Demo_Cannon__ctor_m545D77871238AB3C1408028FD122924492BE9F6F (void);
// 0x00000012 System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton::Start()
extern void Demo_DamageButton_Start_m47D53331A12DCF11CB75D0FDBCAE47D3F7B7A6E3 (void);
// 0x00000013 System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton::.ctor()
extern void Demo_DamageButton__ctor_m212BFD4B30DA6F1800CA9EABF6ACC0296897AB85 (void);
// 0x00000014 System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::Update()
extern void Demo_DefenseGame_Update_mBC671DA491634F848613D29079426AE6EC65C856 (void);
// 0x00000015 System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::SpawnZombie()
extern void Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73 (void);
// 0x00000016 System.Void CodeMonkey.HealthSystemCM.Demo_DefenseGame::.ctor()
extern void Demo_DefenseGame__ctor_m784FCB1D862455D109914F4C1B316AB158575335 (void);
// 0x00000017 System.Void CodeMonkey.HealthSystemCM.Demo_HealButton::Start()
extern void Demo_HealButton_Start_m728E0182CC8C7207A890A5568F24E583C8171DCF (void);
// 0x00000018 System.Void CodeMonkey.HealthSystemCM.Demo_HealButton::.ctor()
extern void Demo_HealButton__ctor_m791CBDB7BD6D3957564098E070DAB73A3BE13767 (void);
// 0x00000019 System.Void CodeMonkey.HealthSystemCM.Demo_Unit::Start()
extern void Demo_Unit_Start_m2AA94BF459E5ED88B019B66C3AA97EE9940E9E2C (void);
// 0x0000001A System.Void CodeMonkey.HealthSystemCM.Demo_Unit::HealthSystem_OnHealed(System.Object,System.EventArgs)
extern void Demo_Unit_HealthSystem_OnHealed_mB19EBDE076B2174AE17EEE736D2B271E7A1EA67A (void);
// 0x0000001B System.Void CodeMonkey.HealthSystemCM.Demo_Unit::HealthSystem_OnDamaged(System.Object,System.EventArgs)
extern void Demo_Unit_HealthSystem_OnDamaged_mDFE694CC2C14414A79608E6455C85604F87DE4AF (void);
// 0x0000001C System.Void CodeMonkey.HealthSystemCM.Demo_Unit::HealthSystem_OnDead(System.Object,System.EventArgs)
extern void Demo_Unit_HealthSystem_OnDead_mCAF5A68EF417E026DF0CE10006661A22AE35CF27 (void);
// 0x0000001D System.Void CodeMonkey.HealthSystemCM.Demo_Unit::OnMouseDown()
extern void Demo_Unit_OnMouseDown_mA11A003B4AC502FDA27ACB0D3DF461C20B5AAC33 (void);
// 0x0000001E System.Void CodeMonkey.HealthSystemCM.Demo_Unit::.ctor()
extern void Demo_Unit__ctor_m3AAC26B169EB43C33659F5B13124F05FADF72E43 (void);
// 0x0000001F System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::Awake()
extern void Demo_UnitCSharp_Awake_m38AEE7D1F5B4E9BC4678A54C0DFAF9C68F8F7546 (void);
// 0x00000020 System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::HealthSystem_OnHealed(System.Object,System.EventArgs)
extern void Demo_UnitCSharp_HealthSystem_OnHealed_mC11861945ACBBF6535571F527070A432DB84AC12 (void);
// 0x00000021 System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::HealthSystem_OnDamaged(System.Object,System.EventArgs)
extern void Demo_UnitCSharp_HealthSystem_OnDamaged_mA11ED92EC84BF522A87597E476EAB03CE414250D (void);
// 0x00000022 System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::HealthSystem_OnDead(System.Object,System.EventArgs)
extern void Demo_UnitCSharp_HealthSystem_OnDead_mB8FCC204EB6B40020CC7478703A6FD8020558958 (void);
// 0x00000023 System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::OnMouseDown()
extern void Demo_UnitCSharp_OnMouseDown_m45885A7922A6D1D08FB9392F37F2DF5E28875680 (void);
// 0x00000024 CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_UnitCSharp::GetHealthSystem()
extern void Demo_UnitCSharp_GetHealthSystem_mBA0D8FA00B80F29EC137D45574FDBFE9F614140B (void);
// 0x00000025 System.Void CodeMonkey.HealthSystemCM.Demo_UnitCSharp::.ctor()
extern void Demo_UnitCSharp__ctor_m23E23867AD0B2C5DE5FF866AA6994695FA3190CC (void);
// 0x00000026 System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Awake()
extern void Demo_Zombie_Awake_m948FA4A81F17E21AAC1C1AD509BD3A089671A03F (void);
// 0x00000027 System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Update()
extern void Demo_Zombie_Update_m382273970FD8253126D44E8A35EFE0CA2D5E4D7E (void);
// 0x00000028 System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::HealthSystem_OnDead(System.Object,System.EventArgs)
extern void Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353 (void);
// 0x00000029 System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::Damage()
extern void Demo_Zombie_Damage_m3345441E19B8E4D2C9FEC05BC9EFEC83E7026BEC (void);
// 0x0000002A CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.Demo_Zombie::GetHealthSystem()
extern void Demo_Zombie_GetHealthSystem_m0DFB5D123D48C9C579CFEF47F4DC8712F0657C21 (void);
// 0x0000002B System.Void CodeMonkey.HealthSystemCM.Demo_Zombie::.ctor()
extern void Demo_Zombie__ctor_mE25DB1066650792908019F9C1B06CADB55C9335C (void);
// 0x0000002C System.Void CodeMonkey.HealthSystemCM.HealthBarUI::Start()
extern void HealthBarUI_Start_m513114536DBB0AD94D9AB686C586C4E39BFF70EA (void);
// 0x0000002D System.Void CodeMonkey.HealthSystemCM.HealthBarUI::SetHealthSystem(CodeMonkey.HealthSystemCM.HealthSystem)
extern void HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC (void);
// 0x0000002E System.Void CodeMonkey.HealthSystemCM.HealthBarUI::HealthSystem_OnHealthChanged(System.Object,System.EventArgs)
extern void HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA (void);
// 0x0000002F System.Void CodeMonkey.HealthSystemCM.HealthBarUI::UpdateHealthBar()
extern void HealthBarUI_UpdateHealthBar_m0AD4E403485C4F870845738CA8F717DC137E59B5 (void);
// 0x00000030 System.Void CodeMonkey.HealthSystemCM.HealthBarUI::OnDestroy()
extern void HealthBarUI_OnDestroy_m780AFFA696943D26015DE5582FC0E17E192DDBE9 (void);
// 0x00000031 System.Void CodeMonkey.HealthSystemCM.HealthBarUI::.ctor()
extern void HealthBarUI__ctor_m42C03777A895847C16018B870A84E142C6361A0F (void);
// 0x00000032 System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealthChanged(System.EventHandler)
extern void HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34 (void);
// 0x00000033 System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealthChanged(System.EventHandler)
extern void HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF (void);
// 0x00000034 System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealthMaxChanged(System.EventHandler)
extern void HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B (void);
// 0x00000035 System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealthMaxChanged(System.EventHandler)
extern void HealthSystem_remove_OnHealthMaxChanged_mBF597E0E3EF51715FCD35D117B18E9895C473C89 (void);
// 0x00000036 System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnDamaged(System.EventHandler)
extern void HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089 (void);
// 0x00000037 System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnDamaged(System.EventHandler)
extern void HealthSystem_remove_OnDamaged_m3B7CC3942A6A596520B226EB032F3841737A7674 (void);
// 0x00000038 System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnHealed(System.EventHandler)
extern void HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10 (void);
// 0x00000039 System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnHealed(System.EventHandler)
extern void HealthSystem_remove_OnHealed_m5FC7901576A801F1C61EA47C70778D1993DC24D5 (void);
// 0x0000003A System.Void CodeMonkey.HealthSystemCM.HealthSystem::add_OnDead(System.EventHandler)
extern void HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52 (void);
// 0x0000003B System.Void CodeMonkey.HealthSystemCM.HealthSystem::remove_OnDead(System.EventHandler)
extern void HealthSystem_remove_OnDead_m28D623D97CEF97A8F418ECB1EE8C67032553D7B7 (void);
// 0x0000003C System.Void CodeMonkey.HealthSystemCM.HealthSystem::.ctor(System.Single)
extern void HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7 (void);
// 0x0000003D System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealth()
extern void HealthSystem_GetHealth_mC367CA7B427DC204C7A5D3CD2B9DFCFE642E3A3A (void);
// 0x0000003E System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealthMax()
extern void HealthSystem_GetHealthMax_m6B963D43FF565740A2AC9DE2A55D44806E22CC31 (void);
// 0x0000003F System.Single CodeMonkey.HealthSystemCM.HealthSystem::GetHealthNormalized()
extern void HealthSystem_GetHealthNormalized_mB1FEDFABE4083B167BCE7A3A637D41DB996DF9E8 (void);
// 0x00000040 System.Void CodeMonkey.HealthSystemCM.HealthSystem::Damage(System.Single)
extern void HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765 (void);
// 0x00000041 System.Void CodeMonkey.HealthSystemCM.HealthSystem::Die()
extern void HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509 (void);
// 0x00000042 System.Boolean CodeMonkey.HealthSystemCM.HealthSystem::IsDead()
extern void HealthSystem_IsDead_m5B147E5D54C2F4604E5F1E3171EE862B3E916EC3 (void);
// 0x00000043 System.Void CodeMonkey.HealthSystemCM.HealthSystem::Heal(System.Single)
extern void HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC (void);
// 0x00000044 System.Void CodeMonkey.HealthSystemCM.HealthSystem::HealComplete()
extern void HealthSystem_HealComplete_m98E89B04DD881539AA4EEBB44399A7F06AE58958 (void);
// 0x00000045 System.Void CodeMonkey.HealthSystemCM.HealthSystem::SetHealthMax(System.Single,System.Boolean)
extern void HealthSystem_SetHealthMax_m34E7B7BC73D4367C0055A61AD1B21BA56FF5565B (void);
// 0x00000046 System.Void CodeMonkey.HealthSystemCM.HealthSystem::SetHealth(System.Single)
extern void HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809 (void);
// 0x00000047 System.Boolean CodeMonkey.HealthSystemCM.HealthSystem::TryGetHealthSystem(UnityEngine.GameObject,CodeMonkey.HealthSystemCM.HealthSystem&,System.Boolean)
extern void HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E (void);
// 0x00000048 System.Void CodeMonkey.HealthSystemCM.HealthSystemComponent::Awake()
extern void HealthSystemComponent_Awake_m70FC15C7F1FB8E777D58EBE37E3E6C688E8BA0FB (void);
// 0x00000049 CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.HealthSystemComponent::GetHealthSystem()
extern void HealthSystemComponent_GetHealthSystem_mA7F8A5C583C2C0ACC401F49F83FDFDE6164AB832 (void);
// 0x0000004A System.Void CodeMonkey.HealthSystemCM.HealthSystemComponent::.ctor()
extern void HealthSystemComponent__ctor_m8FF62368C9DB0711E1AFD7BF1B7EA4725F5EB045 (void);
// 0x0000004B CodeMonkey.HealthSystemCM.HealthSystem CodeMonkey.HealthSystemCM.IGetHealthSystem::GetHealthSystem()
// 0x0000004C System.Void CodeMonkey.HealthSystemCM.LookAtCamera::Awake()
extern void LookAtCamera_Awake_m33AF9BDAF449B93FFF46E011B03C7C5A20E0240A (void);
// 0x0000004D System.Void CodeMonkey.HealthSystemCM.LookAtCamera::Update()
extern void LookAtCamera_Update_m7886FFAC880BF8580109F144D59DC0A13DAC1656 (void);
// 0x0000004E System.Void CodeMonkey.HealthSystemCM.LookAtCamera::OnEnable()
extern void LookAtCamera_OnEnable_m59043E0F4A861408046D7C32588C1D6EC75E0172 (void);
// 0x0000004F System.Void CodeMonkey.HealthSystemCM.LookAtCamera::LookAt()
extern void LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776 (void);
// 0x00000050 System.Void CodeMonkey.HealthSystemCM.LookAtCamera::.ctor()
extern void LookAtCamera__ctor_mE0028225B1C36A59A91F14B945FEBC8A4BE2C1CF (void);
// 0x00000051 System.Void CodeMonkey.HealthSystemCM.Readme_HealthSystem::.ctor()
extern void Readme_HealthSystem__ctor_mEA835B58F53ED1F09A87EAB7638A9BFAA2748654 (void);
// 0x00000052 System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mA7AC968334D5CAF0948B61266BD426905B25CF94 (void);
// 0x00000053 System.Void CodeMonkey.HealthSystemCM.Demo_DamageButton_<>c__DisplayClass1_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m731FBE36F3F0B2464BB7BDBC2584990068765100 (void);
// 0x00000054 System.Void CodeMonkey.HealthSystemCM.Demo_HealButton_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mDC1214DA7828992B6502E91C6F8FD01B01FEC47D (void);
// 0x00000055 System.Void CodeMonkey.HealthSystemCM.Demo_HealButton_<>c__DisplayClass1_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m3B3616F62C011EB9A03B855AF972712288A6CEF2 (void);
// 0x00000056 System.Void CodeMonkey.HealthSystemCM.Readme_HealthSystem_Section::.ctor()
extern void Section__ctor_mBA25DBE826C89953A5AD944CD379A5E9888B3E87 (void);
static Il2CppMethodPointer s_methodPointers[86] = 
{
	Demo_Awake_m2884174F2B16323136857B6BBC9DB1C342610763,
	Demo_Start_m3989B457771469E3BBCC14F6551F69808E0E2341,
	Demo_AddLog_m42EDEE906A788027A5525ECF51C88A56BF3187CC,
	Demo__ctor_m8F4D851855681644B749EE73051E1BE06DEBAA1A,
	Demo_U3CStartU3Eb__4_0_mBA3828BBA3D754B17A354DF950FFA8553BE6B2D4,
	Demo_U3CStartU3Eb__4_1_m47B649E965975DB1F27BA1C59F07E8F4EBC0CA5B,
	Demo_U3CStartU3Eb__4_2_mB821C5E6C8C475C23FA47E3974C33AD020FE7F54,
	Demo_U3CStartU3Eb__4_3_mE0214865B0BE423E88B47CEF94DD8398B560A2A2,
	Demo_U3CStartU3Eb__4_4_mFC2B478410ADD38FC10A18C556DE9D76043E03FA,
	Demo_Bullet_Start_m45FDDAFBE031D873A015A127BEC073D35835C7EF,
	Demo_Bullet_OnTriggerEnter2D_mB2A859E2D7299AC5A27C99BB607562B42E47996D,
	Demo_Bullet__ctor_m7EF3DF410C198246633CF2AF9F40817366B16C7F,
	Demo_Cannon_Awake_mE9E6BBDE4A029D331FC9456B7ADAE31E75D0E3EB,
	Demo_Cannon_Update_mB0F52105ADCFE487F8E4EC304298B38F124A90A1,
	Demo_Cannon_GetMouseWorldPosition_m491CA6AB5CA29173DE131343E03C88B2C62AA296,
	Demo_Cannon_GetAngleFromVector_m9E6AC4CC419576DD4BC263E1950AD1902A1E8FE0,
	Demo_Cannon__ctor_m545D77871238AB3C1408028FD122924492BE9F6F,
	Demo_DamageButton_Start_m47D53331A12DCF11CB75D0FDBCAE47D3F7B7A6E3,
	Demo_DamageButton__ctor_m212BFD4B30DA6F1800CA9EABF6ACC0296897AB85,
	Demo_DefenseGame_Update_mBC671DA491634F848613D29079426AE6EC65C856,
	Demo_DefenseGame_SpawnZombie_mDB1690A0A12187C9907E258715E53364B443FC73,
	Demo_DefenseGame__ctor_m784FCB1D862455D109914F4C1B316AB158575335,
	Demo_HealButton_Start_m728E0182CC8C7207A890A5568F24E583C8171DCF,
	Demo_HealButton__ctor_m791CBDB7BD6D3957564098E070DAB73A3BE13767,
	Demo_Unit_Start_m2AA94BF459E5ED88B019B66C3AA97EE9940E9E2C,
	Demo_Unit_HealthSystem_OnHealed_mB19EBDE076B2174AE17EEE736D2B271E7A1EA67A,
	Demo_Unit_HealthSystem_OnDamaged_mDFE694CC2C14414A79608E6455C85604F87DE4AF,
	Demo_Unit_HealthSystem_OnDead_mCAF5A68EF417E026DF0CE10006661A22AE35CF27,
	Demo_Unit_OnMouseDown_mA11A003B4AC502FDA27ACB0D3DF461C20B5AAC33,
	Demo_Unit__ctor_m3AAC26B169EB43C33659F5B13124F05FADF72E43,
	Demo_UnitCSharp_Awake_m38AEE7D1F5B4E9BC4678A54C0DFAF9C68F8F7546,
	Demo_UnitCSharp_HealthSystem_OnHealed_mC11861945ACBBF6535571F527070A432DB84AC12,
	Demo_UnitCSharp_HealthSystem_OnDamaged_mA11ED92EC84BF522A87597E476EAB03CE414250D,
	Demo_UnitCSharp_HealthSystem_OnDead_mB8FCC204EB6B40020CC7478703A6FD8020558958,
	Demo_UnitCSharp_OnMouseDown_m45885A7922A6D1D08FB9392F37F2DF5E28875680,
	Demo_UnitCSharp_GetHealthSystem_mBA0D8FA00B80F29EC137D45574FDBFE9F614140B,
	Demo_UnitCSharp__ctor_m23E23867AD0B2C5DE5FF866AA6994695FA3190CC,
	Demo_Zombie_Awake_m948FA4A81F17E21AAC1C1AD509BD3A089671A03F,
	Demo_Zombie_Update_m382273970FD8253126D44E8A35EFE0CA2D5E4D7E,
	Demo_Zombie_HealthSystem_OnDead_mA6B2AC26A2736B26186252629E823C4F0D12B353,
	Demo_Zombie_Damage_m3345441E19B8E4D2C9FEC05BC9EFEC83E7026BEC,
	Demo_Zombie_GetHealthSystem_m0DFB5D123D48C9C579CFEF47F4DC8712F0657C21,
	Demo_Zombie__ctor_mE25DB1066650792908019F9C1B06CADB55C9335C,
	HealthBarUI_Start_m513114536DBB0AD94D9AB686C586C4E39BFF70EA,
	HealthBarUI_SetHealthSystem_m0C065C7B2074E11641FFF11E7C533AF526A20FEC,
	HealthBarUI_HealthSystem_OnHealthChanged_mC32DBD4169F74EFC65CDC9C8A576260AE065B4DA,
	HealthBarUI_UpdateHealthBar_m0AD4E403485C4F870845738CA8F717DC137E59B5,
	HealthBarUI_OnDestroy_m780AFFA696943D26015DE5582FC0E17E192DDBE9,
	HealthBarUI__ctor_m42C03777A895847C16018B870A84E142C6361A0F,
	HealthSystem_add_OnHealthChanged_mD09155478B56D153442D617E44559F860B8F9B34,
	HealthSystem_remove_OnHealthChanged_mEF45A57281B10211054C592FABB6A21A382D9CFF,
	HealthSystem_add_OnHealthMaxChanged_m6F6546E5F192699C3DEA43AB5C99B20198FBB35B,
	HealthSystem_remove_OnHealthMaxChanged_mBF597E0E3EF51715FCD35D117B18E9895C473C89,
	HealthSystem_add_OnDamaged_mF13B8DBA08C66061C401695CD9D4C422371D5089,
	HealthSystem_remove_OnDamaged_m3B7CC3942A6A596520B226EB032F3841737A7674,
	HealthSystem_add_OnHealed_mBDB25F011D66D48752BE38A2A47F483EA7EE7C10,
	HealthSystem_remove_OnHealed_m5FC7901576A801F1C61EA47C70778D1993DC24D5,
	HealthSystem_add_OnDead_mBE3B34E64CA49893B822F070572D093F48E48F52,
	HealthSystem_remove_OnDead_m28D623D97CEF97A8F418ECB1EE8C67032553D7B7,
	HealthSystem__ctor_m3BF9F0097CBF9ACD5A8E7BA03F22F33789BD6FB7,
	HealthSystem_GetHealth_mC367CA7B427DC204C7A5D3CD2B9DFCFE642E3A3A,
	HealthSystem_GetHealthMax_m6B963D43FF565740A2AC9DE2A55D44806E22CC31,
	HealthSystem_GetHealthNormalized_mB1FEDFABE4083B167BCE7A3A637D41DB996DF9E8,
	HealthSystem_Damage_m6DD66AE205D2E53C9940EEAA0C002EA84C08C765,
	HealthSystem_Die_mFA134C8954F0EF28DA9FA6613934AA304CC3A509,
	HealthSystem_IsDead_m5B147E5D54C2F4604E5F1E3171EE862B3E916EC3,
	HealthSystem_Heal_mEE2158592582A159ACC38068CA59DD93E11F7CDC,
	HealthSystem_HealComplete_m98E89B04DD881539AA4EEBB44399A7F06AE58958,
	HealthSystem_SetHealthMax_m34E7B7BC73D4367C0055A61AD1B21BA56FF5565B,
	HealthSystem_SetHealth_m10851911239F975D6209A2A50EBCC8E35B1DE809,
	HealthSystem_TryGetHealthSystem_m6E72A099593C2F864AF8BBE8ECCE83E60615691E,
	HealthSystemComponent_Awake_m70FC15C7F1FB8E777D58EBE37E3E6C688E8BA0FB,
	HealthSystemComponent_GetHealthSystem_mA7F8A5C583C2C0ACC401F49F83FDFDE6164AB832,
	HealthSystemComponent__ctor_m8FF62368C9DB0711E1AFD7BF1B7EA4725F5EB045,
	NULL,
	LookAtCamera_Awake_m33AF9BDAF449B93FFF46E011B03C7C5A20E0240A,
	LookAtCamera_Update_m7886FFAC880BF8580109F144D59DC0A13DAC1656,
	LookAtCamera_OnEnable_m59043E0F4A861408046D7C32588C1D6EC75E0172,
	LookAtCamera_LookAt_m28F4C1C4EABF12A5FAAB9D173F46F5C61FBE7776,
	LookAtCamera__ctor_mE0028225B1C36A59A91F14B945FEBC8A4BE2C1CF,
	Readme_HealthSystem__ctor_mEA835B58F53ED1F09A87EAB7638A9BFAA2748654,
	U3CU3Ec__DisplayClass1_0__ctor_mA7AC968334D5CAF0948B61266BD426905B25CF94,
	U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m731FBE36F3F0B2464BB7BDBC2584990068765100,
	U3CU3Ec__DisplayClass1_0__ctor_mDC1214DA7828992B6502E91C6F8FD01B01FEC47D,
	U3CU3Ec__DisplayClass1_0_U3CStartU3Eb__0_m3B3616F62C011EB9A03B855AF972712288A6CEF2,
	Section__ctor_mBA25DBE826C89953A5AD944CD379A5E9888B3E87,
};
static const int32_t s_InvokerIndices[86] = 
{
	23,
	23,
	26,
	23,
	27,
	27,
	27,
	27,
	27,
	23,
	26,
	23,
	23,
	23,
	1135,
	2152,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	27,
	23,
	23,
	23,
	27,
	27,
	27,
	23,
	14,
	23,
	23,
	23,
	27,
	23,
	14,
	23,
	23,
	26,
	27,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	296,
	687,
	687,
	687,
	296,
	23,
	114,
	296,
	23,
	1712,
	296,
	2153,
	23,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_CodeMonkey_HealthSystemCodeGenModule;
const Il2CppCodeGenModule g_CodeMonkey_HealthSystemCodeGenModule = 
{
	"CodeMonkey_HealthSystem.dll",
	86,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
