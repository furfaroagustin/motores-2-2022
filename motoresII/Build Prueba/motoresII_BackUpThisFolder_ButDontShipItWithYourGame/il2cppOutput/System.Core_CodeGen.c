﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000000F TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000010 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000012 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000014 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000015 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000017 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000018 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000001B System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000001C TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x0000001E System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000001F System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000020 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000021 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000022 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000024 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000025 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000026 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000027 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000028 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000029 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000002A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002C System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000002D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x0000002E System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000002F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000030 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000031 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000033 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000037 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000038 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000042 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000043 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000046 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000047 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000048 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000049 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000004A System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000004B System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000004C System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x0000004D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000004E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000004F TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000050 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000051 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000052 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000053 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000054 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x00000055 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x00000056 System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x00000057 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x00000058 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000059 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x0000005A System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x0000005B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000005C System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005D System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000005E System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000005F System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000060 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000061 System.Void System.Linq.Set`1::Resize()
// 0x00000062 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000063 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000064 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000065 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000066 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000067 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000068 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000069 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000006A System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x0000006B TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000006C System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000006D System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x0000006E System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006F System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000070 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000071 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000072 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000073 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000074 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000075 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000076 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000077 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000078 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000079 TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000007A System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000007B System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000007C System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000007D System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000007E System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000007F System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000080 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000081 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000082 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000083 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000084 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000085 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000086 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000087 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000088 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000089 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000008A System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000008B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000008F System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000090 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000091 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000092 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x00000093 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x00000094 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x00000095 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000096 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[150] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[150] = 
{
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[49] = 
{
	{ 0x02000004, { 73, 4 } },
	{ 0x02000005, { 77, 9 } },
	{ 0x02000006, { 88, 7 } },
	{ 0x02000007, { 97, 10 } },
	{ 0x02000008, { 109, 11 } },
	{ 0x02000009, { 123, 9 } },
	{ 0x0200000A, { 135, 12 } },
	{ 0x0200000B, { 150, 1 } },
	{ 0x0200000C, { 151, 2 } },
	{ 0x0200000D, { 153, 12 } },
	{ 0x0200000E, { 165, 11 } },
	{ 0x02000010, { 176, 8 } },
	{ 0x02000012, { 184, 3 } },
	{ 0x02000013, { 189, 5 } },
	{ 0x02000014, { 194, 7 } },
	{ 0x02000015, { 201, 3 } },
	{ 0x02000016, { 204, 7 } },
	{ 0x02000017, { 211, 4 } },
	{ 0x02000018, { 215, 21 } },
	{ 0x0200001A, { 236, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 1 } },
	{ 0x0600000A, { 31, 2 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 1 } },
	{ 0x0600000D, { 36, 1 } },
	{ 0x0600000E, { 37, 2 } },
	{ 0x0600000F, { 39, 3 } },
	{ 0x06000010, { 42, 2 } },
	{ 0x06000011, { 44, 3 } },
	{ 0x06000012, { 47, 4 } },
	{ 0x06000013, { 51, 3 } },
	{ 0x06000014, { 54, 3 } },
	{ 0x06000015, { 57, 1 } },
	{ 0x06000016, { 58, 3 } },
	{ 0x06000017, { 61, 2 } },
	{ 0x06000018, { 63, 3 } },
	{ 0x06000019, { 66, 2 } },
	{ 0x0600001A, { 68, 5 } },
	{ 0x0600002A, { 86, 2 } },
	{ 0x0600002F, { 95, 2 } },
	{ 0x06000034, { 107, 2 } },
	{ 0x0600003A, { 120, 3 } },
	{ 0x0600003F, { 132, 3 } },
	{ 0x06000044, { 147, 3 } },
	{ 0x06000066, { 187, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[238] = 
{
	{ (Il2CppRGCTXDataType)2, 23795 },
	{ (Il2CppRGCTXDataType)3, 19135 },
	{ (Il2CppRGCTXDataType)2, 23796 },
	{ (Il2CppRGCTXDataType)2, 23797 },
	{ (Il2CppRGCTXDataType)3, 19136 },
	{ (Il2CppRGCTXDataType)2, 23798 },
	{ (Il2CppRGCTXDataType)2, 23799 },
	{ (Il2CppRGCTXDataType)3, 19137 },
	{ (Il2CppRGCTXDataType)2, 23800 },
	{ (Il2CppRGCTXDataType)3, 19138 },
	{ (Il2CppRGCTXDataType)2, 23801 },
	{ (Il2CppRGCTXDataType)3, 19139 },
	{ (Il2CppRGCTXDataType)2, 23802 },
	{ (Il2CppRGCTXDataType)2, 23803 },
	{ (Il2CppRGCTXDataType)3, 19140 },
	{ (Il2CppRGCTXDataType)2, 23804 },
	{ (Il2CppRGCTXDataType)2, 23805 },
	{ (Il2CppRGCTXDataType)3, 19141 },
	{ (Il2CppRGCTXDataType)2, 23806 },
	{ (Il2CppRGCTXDataType)3, 19142 },
	{ (Il2CppRGCTXDataType)2, 23807 },
	{ (Il2CppRGCTXDataType)3, 19143 },
	{ (Il2CppRGCTXDataType)3, 19144 },
	{ (Il2CppRGCTXDataType)2, 17427 },
	{ (Il2CppRGCTXDataType)3, 19145 },
	{ (Il2CppRGCTXDataType)2, 23808 },
	{ (Il2CppRGCTXDataType)3, 19146 },
	{ (Il2CppRGCTXDataType)3, 19147 },
	{ (Il2CppRGCTXDataType)2, 17434 },
	{ (Il2CppRGCTXDataType)3, 19148 },
	{ (Il2CppRGCTXDataType)3, 19149 },
	{ (Il2CppRGCTXDataType)2, 23809 },
	{ (Il2CppRGCTXDataType)3, 19150 },
	{ (Il2CppRGCTXDataType)2, 23810 },
	{ (Il2CppRGCTXDataType)3, 19151 },
	{ (Il2CppRGCTXDataType)3, 19152 },
	{ (Il2CppRGCTXDataType)3, 19153 },
	{ (Il2CppRGCTXDataType)2, 23811 },
	{ (Il2CppRGCTXDataType)3, 19154 },
	{ (Il2CppRGCTXDataType)2, 23812 },
	{ (Il2CppRGCTXDataType)3, 19155 },
	{ (Il2CppRGCTXDataType)3, 19156 },
	{ (Il2CppRGCTXDataType)2, 17464 },
	{ (Il2CppRGCTXDataType)3, 19157 },
	{ (Il2CppRGCTXDataType)2, 17465 },
	{ (Il2CppRGCTXDataType)2, 23813 },
	{ (Il2CppRGCTXDataType)3, 19158 },
	{ (Il2CppRGCTXDataType)2, 23814 },
	{ (Il2CppRGCTXDataType)2, 23815 },
	{ (Il2CppRGCTXDataType)2, 17468 },
	{ (Il2CppRGCTXDataType)2, 23816 },
	{ (Il2CppRGCTXDataType)2, 17470 },
	{ (Il2CppRGCTXDataType)2, 23817 },
	{ (Il2CppRGCTXDataType)3, 19159 },
	{ (Il2CppRGCTXDataType)2, 23818 },
	{ (Il2CppRGCTXDataType)2, 17473 },
	{ (Il2CppRGCTXDataType)2, 23819 },
	{ (Il2CppRGCTXDataType)2, 17475 },
	{ (Il2CppRGCTXDataType)2, 17477 },
	{ (Il2CppRGCTXDataType)2, 23820 },
	{ (Il2CppRGCTXDataType)3, 19160 },
	{ (Il2CppRGCTXDataType)2, 23821 },
	{ (Il2CppRGCTXDataType)2, 17480 },
	{ (Il2CppRGCTXDataType)2, 17482 },
	{ (Il2CppRGCTXDataType)2, 23822 },
	{ (Il2CppRGCTXDataType)3, 19161 },
	{ (Il2CppRGCTXDataType)2, 23823 },
	{ (Il2CppRGCTXDataType)3, 19162 },
	{ (Il2CppRGCTXDataType)3, 19163 },
	{ (Il2CppRGCTXDataType)2, 23824 },
	{ (Il2CppRGCTXDataType)2, 17487 },
	{ (Il2CppRGCTXDataType)2, 23825 },
	{ (Il2CppRGCTXDataType)2, 17489 },
	{ (Il2CppRGCTXDataType)3, 19164 },
	{ (Il2CppRGCTXDataType)3, 19165 },
	{ (Il2CppRGCTXDataType)2, 17492 },
	{ (Il2CppRGCTXDataType)3, 19166 },
	{ (Il2CppRGCTXDataType)3, 19167 },
	{ (Il2CppRGCTXDataType)2, 17504 },
	{ (Il2CppRGCTXDataType)2, 23826 },
	{ (Il2CppRGCTXDataType)3, 19168 },
	{ (Il2CppRGCTXDataType)3, 19169 },
	{ (Il2CppRGCTXDataType)2, 17506 },
	{ (Il2CppRGCTXDataType)2, 23657 },
	{ (Il2CppRGCTXDataType)3, 19170 },
	{ (Il2CppRGCTXDataType)3, 19171 },
	{ (Il2CppRGCTXDataType)2, 23827 },
	{ (Il2CppRGCTXDataType)3, 19172 },
	{ (Il2CppRGCTXDataType)3, 19173 },
	{ (Il2CppRGCTXDataType)2, 17516 },
	{ (Il2CppRGCTXDataType)2, 23828 },
	{ (Il2CppRGCTXDataType)3, 19174 },
	{ (Il2CppRGCTXDataType)3, 19175 },
	{ (Il2CppRGCTXDataType)3, 17901 },
	{ (Il2CppRGCTXDataType)3, 19176 },
	{ (Il2CppRGCTXDataType)2, 23829 },
	{ (Il2CppRGCTXDataType)3, 19177 },
	{ (Il2CppRGCTXDataType)3, 19178 },
	{ (Il2CppRGCTXDataType)2, 17528 },
	{ (Il2CppRGCTXDataType)2, 23830 },
	{ (Il2CppRGCTXDataType)3, 19179 },
	{ (Il2CppRGCTXDataType)3, 19180 },
	{ (Il2CppRGCTXDataType)3, 19181 },
	{ (Il2CppRGCTXDataType)3, 19182 },
	{ (Il2CppRGCTXDataType)3, 19183 },
	{ (Il2CppRGCTXDataType)3, 17907 },
	{ (Il2CppRGCTXDataType)3, 19184 },
	{ (Il2CppRGCTXDataType)2, 23831 },
	{ (Il2CppRGCTXDataType)3, 19185 },
	{ (Il2CppRGCTXDataType)3, 19186 },
	{ (Il2CppRGCTXDataType)2, 17541 },
	{ (Il2CppRGCTXDataType)2, 23832 },
	{ (Il2CppRGCTXDataType)3, 19187 },
	{ (Il2CppRGCTXDataType)3, 19188 },
	{ (Il2CppRGCTXDataType)2, 17543 },
	{ (Il2CppRGCTXDataType)2, 23833 },
	{ (Il2CppRGCTXDataType)3, 19189 },
	{ (Il2CppRGCTXDataType)3, 19190 },
	{ (Il2CppRGCTXDataType)2, 23834 },
	{ (Il2CppRGCTXDataType)3, 19191 },
	{ (Il2CppRGCTXDataType)3, 19192 },
	{ (Il2CppRGCTXDataType)2, 23835 },
	{ (Il2CppRGCTXDataType)3, 19193 },
	{ (Il2CppRGCTXDataType)3, 19194 },
	{ (Il2CppRGCTXDataType)2, 17558 },
	{ (Il2CppRGCTXDataType)2, 23836 },
	{ (Il2CppRGCTXDataType)3, 19195 },
	{ (Il2CppRGCTXDataType)3, 19196 },
	{ (Il2CppRGCTXDataType)3, 19197 },
	{ (Il2CppRGCTXDataType)3, 17918 },
	{ (Il2CppRGCTXDataType)2, 23837 },
	{ (Il2CppRGCTXDataType)3, 19198 },
	{ (Il2CppRGCTXDataType)3, 19199 },
	{ (Il2CppRGCTXDataType)2, 23838 },
	{ (Il2CppRGCTXDataType)3, 19200 },
	{ (Il2CppRGCTXDataType)3, 19201 },
	{ (Il2CppRGCTXDataType)2, 17574 },
	{ (Il2CppRGCTXDataType)2, 23839 },
	{ (Il2CppRGCTXDataType)3, 19202 },
	{ (Il2CppRGCTXDataType)3, 19203 },
	{ (Il2CppRGCTXDataType)3, 19204 },
	{ (Il2CppRGCTXDataType)3, 19205 },
	{ (Il2CppRGCTXDataType)3, 19206 },
	{ (Il2CppRGCTXDataType)3, 19207 },
	{ (Il2CppRGCTXDataType)3, 17924 },
	{ (Il2CppRGCTXDataType)2, 23840 },
	{ (Il2CppRGCTXDataType)3, 19208 },
	{ (Il2CppRGCTXDataType)3, 19209 },
	{ (Il2CppRGCTXDataType)2, 23841 },
	{ (Il2CppRGCTXDataType)3, 19210 },
	{ (Il2CppRGCTXDataType)3, 19211 },
	{ (Il2CppRGCTXDataType)3, 19212 },
	{ (Il2CppRGCTXDataType)3, 19213 },
	{ (Il2CppRGCTXDataType)3, 19214 },
	{ (Il2CppRGCTXDataType)3, 19215 },
	{ (Il2CppRGCTXDataType)2, 23842 },
	{ (Il2CppRGCTXDataType)2, 23843 },
	{ (Il2CppRGCTXDataType)3, 19216 },
	{ (Il2CppRGCTXDataType)2, 17609 },
	{ (Il2CppRGCTXDataType)2, 17603 },
	{ (Il2CppRGCTXDataType)3, 19217 },
	{ (Il2CppRGCTXDataType)2, 17602 },
	{ (Il2CppRGCTXDataType)2, 23844 },
	{ (Il2CppRGCTXDataType)3, 19218 },
	{ (Il2CppRGCTXDataType)3, 19219 },
	{ (Il2CppRGCTXDataType)3, 19220 },
	{ (Il2CppRGCTXDataType)2, 23845 },
	{ (Il2CppRGCTXDataType)3, 19221 },
	{ (Il2CppRGCTXDataType)2, 17625 },
	{ (Il2CppRGCTXDataType)2, 17617 },
	{ (Il2CppRGCTXDataType)3, 19222 },
	{ (Il2CppRGCTXDataType)3, 19223 },
	{ (Il2CppRGCTXDataType)2, 17616 },
	{ (Il2CppRGCTXDataType)2, 23846 },
	{ (Il2CppRGCTXDataType)3, 19224 },
	{ (Il2CppRGCTXDataType)3, 19225 },
	{ (Il2CppRGCTXDataType)3, 19226 },
	{ (Il2CppRGCTXDataType)2, 23847 },
	{ (Il2CppRGCTXDataType)2, 23848 },
	{ (Il2CppRGCTXDataType)3, 19227 },
	{ (Il2CppRGCTXDataType)3, 19228 },
	{ (Il2CppRGCTXDataType)2, 17637 },
	{ (Il2CppRGCTXDataType)3, 19229 },
	{ (Il2CppRGCTXDataType)2, 17638 },
	{ (Il2CppRGCTXDataType)2, 23849 },
	{ (Il2CppRGCTXDataType)3, 19230 },
	{ (Il2CppRGCTXDataType)3, 19231 },
	{ (Il2CppRGCTXDataType)2, 23850 },
	{ (Il2CppRGCTXDataType)3, 19232 },
	{ (Il2CppRGCTXDataType)2, 23851 },
	{ (Il2CppRGCTXDataType)3, 19233 },
	{ (Il2CppRGCTXDataType)3, 19234 },
	{ (Il2CppRGCTXDataType)3, 19235 },
	{ (Il2CppRGCTXDataType)2, 17657 },
	{ (Il2CppRGCTXDataType)3, 19236 },
	{ (Il2CppRGCTXDataType)2, 17665 },
	{ (Il2CppRGCTXDataType)3, 19237 },
	{ (Il2CppRGCTXDataType)2, 23852 },
	{ (Il2CppRGCTXDataType)2, 23853 },
	{ (Il2CppRGCTXDataType)3, 19238 },
	{ (Il2CppRGCTXDataType)3, 19239 },
	{ (Il2CppRGCTXDataType)3, 19240 },
	{ (Il2CppRGCTXDataType)3, 19241 },
	{ (Il2CppRGCTXDataType)3, 19242 },
	{ (Il2CppRGCTXDataType)3, 19243 },
	{ (Il2CppRGCTXDataType)2, 17681 },
	{ (Il2CppRGCTXDataType)2, 23854 },
	{ (Il2CppRGCTXDataType)3, 19244 },
	{ (Il2CppRGCTXDataType)3, 19245 },
	{ (Il2CppRGCTXDataType)2, 17685 },
	{ (Il2CppRGCTXDataType)3, 19246 },
	{ (Il2CppRGCTXDataType)2, 23855 },
	{ (Il2CppRGCTXDataType)2, 17695 },
	{ (Il2CppRGCTXDataType)2, 17693 },
	{ (Il2CppRGCTXDataType)2, 23856 },
	{ (Il2CppRGCTXDataType)3, 19247 },
	{ (Il2CppRGCTXDataType)2, 23857 },
	{ (Il2CppRGCTXDataType)3, 19248 },
	{ (Il2CppRGCTXDataType)3, 19249 },
	{ (Il2CppRGCTXDataType)3, 19250 },
	{ (Il2CppRGCTXDataType)2, 17699 },
	{ (Il2CppRGCTXDataType)3, 19251 },
	{ (Il2CppRGCTXDataType)3, 19252 },
	{ (Il2CppRGCTXDataType)2, 17702 },
	{ (Il2CppRGCTXDataType)3, 19253 },
	{ (Il2CppRGCTXDataType)1, 23858 },
	{ (Il2CppRGCTXDataType)2, 17701 },
	{ (Il2CppRGCTXDataType)3, 19254 },
	{ (Il2CppRGCTXDataType)1, 17701 },
	{ (Il2CppRGCTXDataType)1, 17699 },
	{ (Il2CppRGCTXDataType)2, 23859 },
	{ (Il2CppRGCTXDataType)2, 17701 },
	{ (Il2CppRGCTXDataType)3, 19255 },
	{ (Il2CppRGCTXDataType)3, 19256 },
	{ (Il2CppRGCTXDataType)3, 19257 },
	{ (Il2CppRGCTXDataType)2, 17700 },
	{ (Il2CppRGCTXDataType)3, 19258 },
	{ (Il2CppRGCTXDataType)2, 17713 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	150,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	49,
	s_rgctxIndices,
	238,
	s_rgctxValues,
	NULL,
};
