﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanzaLLamas : MonoBehaviour
{
    public GameObject lanzaLLamasBala;
    private void Update()
    {
        if (Input.GetKey("space"))
        {
            activarBala();
        }
    }
    void Disparo()
    {
        lanzaLLamasBala.SetActive(false);
    }
    void activarBala()
    {
        lanzaLLamasBala.SetActive(true);
        Invoke("Disparo", 6);

    }

}
