﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Die : MonoBehaviour
{
	//public GameObject panel;
	public int maxHealth = 100;
	public int currentHealth;
	public HealtBar healthBar;
	public GameObject sangreEfecto;
	public int contadorMuertes;
	public TextMeshProUGUI TextoContadorMuertes;
	public Transform spawnPoint;
	protected AudioSource audioSource;
	public AudioClip trampa;
	
	//public AudioClip disparo;
	void Start()
	{
	//	panel.SetActive(false);
		///audioSource = GetComponent<AudioSource>();
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}	
	void Update()
	{

	}
	public void OnCollisionEnter(Collision colision)
	{
		if (colision.gameObject.CompareTag("Bala"))
		{
			Debug.Log("hola");
			RestaBarra(10);
			Instantiate(sangreEfecto, transform.position, transform.rotation);
			//audioSource.PlayOneShot(trampa);
		}
		if (colision.gameObject.CompareTag("Llama"))
		{
			Debug.Log("hola");
			RestaBarra(50);
			Instantiate(sangreEfecto, transform.position, transform.rotation);
			//audioSource.PlayOneShot(trampa);
		}
		if (colision.gameObject.CompareTag("Proyectil"))
		{
			Debug.Log("hola");
			RestaBarra(40);
			Instantiate(sangreEfecto, transform.position, transform.rotation);
			//audioSource.PlayOneShot(trampa);
		}

		if (currentHealth == 0)
		{
			contadorMuertes++;
			TextoContadorMuertes.text= "Kills: " + contadorMuertes.ToString();

			//transform.position = spawnPoint.position;
			//Destruccion();
			Fin();
		}
		if (colision.gameObject.CompareTag("Life"))
		{
			Debug.Log("Se suman 10");
			SumarBarra(10);
			
			//audioSource.PlayOneShot(trampa);
		}


	}
    void RestaBarra(int damage)
	{
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}
	void SumarBarra(int damage)
	{
		currentHealth += damage;

		healthBar.setHealth(currentHealth);
	}
	
	private void Fin()
	{
		SceneManager.LoadScene("Nivel1");
		//SceneManager.LoadScene("InterludeMenu");
		//SceneManager.LoadScene("LevelOne");
		currentHealth = maxHealth;
	    healthBar.SetMaxHealth(maxHealth);
		transform.position = spawnPoint.position;
		if (contadorMuertes == 10)
		{
			SceneManager.LoadScene("WinnerOne");

		}




	}


}
