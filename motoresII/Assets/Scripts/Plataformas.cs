using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Plataformas : MonoBehaviour
{
    public float tiempoCreacion = 6f;
    public GameObject prefabPlataforma;
    private int count = 0;
    public BoxCollider boxcollider;
    public GameObject fuego;
    public float tiempoAparicion;
    public float timeToExplode = 10f;
    void Start()
    {
        InvokeRepeating("Creando",0, tiempoCreacion);
        InvokeRepeating("Prendiendo",0, tiempoAparicion);
    }
    private void Update()
    {
       
    }


    void Creando()
    {
        boxcollider.enabled = false;
        fuego.SetActive(true);

       
    }
    void Prendiendo()
    {
        boxcollider.enabled = true;
        fuego.SetActive(false);
    }


}



