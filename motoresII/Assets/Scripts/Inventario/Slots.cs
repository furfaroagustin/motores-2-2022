﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class Slots : MonoBehaviour, IPointerClickHandler
{
    public GameObject item;
    public int ID;
    public string type;
    public bool empty;
    public Sprite icon;
    public string description;

    public Transform slotIconGameObject;
    private void Start()
    {
        slotIconGameObject = transform.GetChild(0);
    }
    public void UpdateSlot()
    {
        this.GetComponent<Image>().sprite = icon;
    }
    public void UseItem()
    {
        item.GetComponent<Items>().itemsUsage();
    }
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        UseItem();
    }
}