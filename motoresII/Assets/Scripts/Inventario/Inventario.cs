﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventario : MonoBehaviour
{
    private bool inventarioEnabled;
    public GameObject inventario;
    private int allSlots;
    private int enabledSlot;
    private GameObject[] slot;
    public GameObject slotHolder;
    void Start()
    {
        allSlots = slotHolder.transform.childCount;
        slot = new GameObject[allSlots];//array
        for (int i=0;i<allSlots;i++)
        {
            slot[i] = slotHolder.transform.GetChild(i).gameObject;
            if (slot[i].GetComponent<Slots>().item==null)
            {
                slot[i].GetComponent<Slots>().empty = true;
            }
        }

    }

  
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            inventarioEnabled = !inventarioEnabled;
        }
        if(inventarioEnabled==true)
        {
            inventario.SetActive(true);
        }
        else
        {
            inventario.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Item")
        {
            GameObject itemPickedUp = other.gameObject;
            Items item = itemPickedUp.GetComponent<Items>();
            AddItem(itemPickedUp, item.ID, item.type, item.description, item.icon);

        }
    }
    public void AddItem(GameObject itemObject,int itemID,string itemType,string itemDescripcion,Sprite itemIcon)
    {
        for (int i = 0; i < allSlots; i++)
        {
            if (slot[i].GetComponent<Slots>().empty)
            {
                itemObject.GetComponent<Items>().pickedUp = true;
                slot[i].GetComponent<Slots>().item = itemObject;
                slot[i].GetComponent<Slots>().ID = itemID;
                slot[i].GetComponent<Slots>().type = itemType;
                slot[i].GetComponent<Slots>().description=itemDescripcion;
                slot[i].GetComponent<Slots>().icon = itemIcon;
                itemObject.transform.parent = slot[i].transform;
                itemObject.SetActive(false);

                slot[i].GetComponent<Slots>().UpdateSlot();

                slot[i].GetComponent<Slots>().empty = false;
                
            }
            return;
        }
    }
}
