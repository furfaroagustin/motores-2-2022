﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
    public int ID;
    public string type;
    public string description;
    public Sprite icon;
    [HideInInspector]
    public bool pickedUp;
    [HideInInspector]
    public bool equipped;
    [HideInInspector]
    public GameObject weaponManager;
    public GameObject weapon;
    public bool PlayerWeapon;

    private void Start()
    {
        weaponManager = GameObject.FindWithTag("WeaponManager");
        if(!PlayerWeapon)
        {
            int allweapons = weaponManager.transform.childCount;
            for(int i=0;i<allweapons;i++)
            {
                if(weaponManager.transform.GetChild(i).gameObject.GetComponent<Items>().ID==ID)
                {
                   weapon= weaponManager.transform.GetChild(i).gameObject;
                }
            }
        }
    }

    private void Update()
    {
        if(equipped)
        {
            if(Input.GetKeyDown(KeyCode.E))
            {
                equipped = true;
            }
            if (equipped ==false) 
            {
                gameObject.SetActive(false);
            }
        }
    }
    public void itemsUsage()
    {
        if(type=="Weapon")
        {
            weapon.SetActive(true);
            weapon.GetComponent<Items>().equipped = true;
            equipped = true;
        }
    }


}
