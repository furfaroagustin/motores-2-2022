﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Timer : MonoBehaviour
{
    public float timer = 0;
   // public float tiempoPartida = 0;
    bool Stop = false;
    public GameObject panelComienzo;
    public float t = 2;
    public TextMeshProUGUI textTimerPro;
    public TextMeshProUGUI textPartida;
    private void Update()
    {
        //  timer -= Time.deltaTime;


        if (!Stop)
        {
            textTimerPro.text = "" + timer.ToString("0");
            timer -= Time.deltaTime;
        }


        if (timer <= 0)
        {
            Stop = true;
            Explotar();

        }
    }

    void Explotar()
    {
        panelComienzo.SetActive(true);
        t -= Time.deltaTime;
        if (t <= 0)
        {
            Stop = true;
            textTimerPro.text = "";
            panelComienzo.SetActive(false);
            //TiempoPartida();
        }

    }
    /*void TiempoPartida()
    {
        tiempoPartida -= Time.deltaTime;
        textTimerPro.text = "" + tiempoPartida.ToString("0");
        if (tiempoPartida <= 0)
        {
            Stop = true;
        }
    }/*/
    

   
}
