﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciador : MonoBehaviour
{
    public float tiempoCreacion = 8f;
    public GameObject prefab;
    private int count = 0;
    void Start()
    {
        InvokeRepeating("Creando", 0.0F, tiempoCreacion);
    }



    void Creando()
    {
        GameObject cube = Instantiate(prefab, transform.position, transform.rotation) as GameObject;
        cube.name = "Arma" + count++;

        Destroy(cube, 5f);
    }
}
