﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoTorreta : MonoBehaviour
{
    [SerializeField]
    private GameObject _bullet;
    [SerializeField]
    private float _timer = 2f;
    [SerializeField]
    private int  _counter;
    private int _maxCounter=20;

    private void Start()
    {
        StartCoroutine(FireBullets_CR());


    }
    private void Update()
    {
        
    }

    IEnumerator FireBullets_CR()
    {
        for (int i = 0; i < _maxCounter;i++)
        {
            _counter++;
            Instantiate(_bullet, transform.position, transform.rotation);
            yield return new WaitForSeconds(_timer);
        }
    }





}
