﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TiempoPartida : MonoBehaviour
{
    public TextMeshProUGUI tiempoText;
    private float startTiempo;

    void Start()
    {
        startTiempo = Time.time;
    }


    void Update()
    {
        float t = Time.time - startTiempo;

        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("00");

        tiempoText.text = "" + minutes + ":" + seconds + "     ";
    }
}
