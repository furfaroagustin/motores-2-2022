﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmasDestruccion : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			Destroy(this.gameObject);
			Debug.Log("Se destruyo el PowerUp");

		}
		if (other.gameObject.CompareTag("PlayerTwo"))
		{
			Destroy(this.gameObject);
			Debug.Log("Se destruyo el PowerUp");

		}
	}
}
