﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoTwo : MonoBehaviour
{
    public GameObject bala;//bullet
    public Transform puntoSpawn;//spawnPoint

    public float fuerzaDisparo = 1500;//shotforce
    public float shotRate = 0.5f;//radio de disparo

    private float shotRateTime = 0;//tiempo entre disparo 

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (Time.time > shotRateTime)
            {
                GameObject nuevaBala;//NewBullet
                nuevaBala = Instantiate(bala, puntoSpawn.position, puntoSpawn.rotation);
                nuevaBala.GetComponent<Rigidbody>().AddForce(puntoSpawn.forward * fuerzaDisparo);
                shotRateTime = Time.time + shotRate;
                Destroy(nuevaBala, 2);
            }
        }
    }
}
