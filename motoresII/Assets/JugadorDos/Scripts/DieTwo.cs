﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;


public class DieTwo : MonoBehaviour
{
	//public GameObject panel;
	public int maxHealth = 100;
	public int currentHealth;
	public HealtBar healthBar;
	public GameObject sangre;
	public int contadorMuertes;
	public TextMeshProUGUI TextoContadorMuertes;
	public Transform spawnPoint;
	//protected AudioSource audioSource;
	//public AudioClip trampa;

	//public AudioClip risa;
	void Start()
	{
		//	panel.SetActive(false);
		///audioSource = GetComponent<AudioSource>();
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
	}
	void Update()
	{

	}
	public void OnCollisionEnter(Collision colision)
	{
		if (colision.gameObject.CompareTag("Bala"))
		{
			Debug.Log("hola");
			RestaBarra(10);
			//audioSource.PlayOneShot(trampa);
			Instantiate(sangre, transform.position, transform.rotation);
		}

		if (currentHealth == 0)
		{
			contadorMuertes++;
			TextoContadorMuertes.text = "Kills: " + contadorMuertes.ToString();
			//transform.position = spawnPoint.position;
			//Destruccion();
			Fin();
		}
		if (colision.gameObject.CompareTag("Life"))
		{
			Debug.Log("Se suman 10");
			SumarBarra(10);

			//audioSource.PlayOneShot(trampa);
		}
		if (colision.gameObject.CompareTag("Llama"))
		{
			Debug.Log("hola");
			RestaBarra(50);
			Instantiate(sangre, transform.position, transform.rotation);
			//audioSource.PlayOneShot(trampa);
		}
		if (colision.gameObject.CompareTag("Proyectil"))
		{
			Debug.Log("hola");
			RestaBarra(50);
			Instantiate(sangre, transform.position, transform.rotation);
			//audioSource.PlayOneShot(trampa);
		}

	}
	void RestaBarra(int damage)
	{
		currentHealth -= damage;

		healthBar.setHealth(currentHealth);
	}
	void SumarBarra(int damage)
	{
		currentHealth += damage;

		healthBar.setHealth(currentHealth);
	}
	/*
	public void Destruccion()
	{
		Destroy(this.gameObject);
		

		GestorDeAudio.instancia.ReproducirSonido("Muerte");
		SceneManager.LoadScene("GameOver");

		//Persistencia.instancia.GuardarDataPersistencia();

	}*/
	private void Fin()
	{
		SceneManager.LoadScene("Nivel1");
		currentHealth = maxHealth;
		healthBar.SetMaxHealth(maxHealth);
		transform.position = spawnPoint.position;
		if(contadorMuertes==10)
        {
			SceneManager.LoadScene("WinnerTwo");

		}


	}
}
