﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EfectoExplodsion : MonoBehaviour
{
    public GameObject explosionEfecto;

    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ActivarExplosion();
        }
        if (collision.gameObject.CompareTag("PlayerTwo"))
        {
            ActivarExplosion();
        }
    }






    void ActivarExplosion()
    {
        Instantiate(explosionEfecto, transform.position, transform.rotation);

    }

}
