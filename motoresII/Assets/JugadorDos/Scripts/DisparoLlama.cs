﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoLlama : MonoBehaviour
{
    public GameObject lanzaLLamasBala;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            activarBala();
        }
    }
    void Disparo()
    {
        lanzaLLamasBala.SetActive(false);
    }
    void activarBala()
    {
        lanzaLLamasBala.SetActive(true);
        Invoke("Disparo", 6);

    }
}
